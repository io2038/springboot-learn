package ink.tsg.schedulingtasks.task.service;

import ink.tsg.schedulingtasks.task.entity.SysJob;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 定时任务调度表 服务类
 * </p>
 *
 * @author GEO_TSG
 * @since 2021-04-10
 */
public interface ISysJobService extends IService<SysJob> {

}
