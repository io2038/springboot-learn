package ink.tsg.schedulingtasks.task.mapper;

import ink.tsg.schedulingtasks.task.entity.SysJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 定时任务调度表 Mapper 接口
 * </p>
 *
 * @author GEO_TSG
 * @since 2021-04-10
 */
@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {

}
