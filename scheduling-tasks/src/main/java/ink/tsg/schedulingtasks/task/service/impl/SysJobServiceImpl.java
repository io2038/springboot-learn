package ink.tsg.schedulingtasks.task.service.impl;

import ink.tsg.schedulingtasks.task.entity.SysJob;
import ink.tsg.schedulingtasks.task.mapper.SysJobMapper;
import ink.tsg.schedulingtasks.task.service.ISysJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 定时任务调度表 服务实现类
 * </p>
 *
 * @author GEO_TSG
 * @since 2021-04-10
 */
@Service
@Slf4j
@EnableScheduling //确保后台任务执行被创建,开启定时任务
@EnableAsync    //开启多线程
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements ISysJobService {



    @Scheduled(fixedRate = 3000)
    public void test(){
        List<SysJob> sysJobs = baseMapper.selectList(null);
        log.info("查询结果{}",sysJobs);
    }

    @Scheduled(fixedRate = 1000)
    public void test1(){
        log.info("1秒执行");
    }
}
