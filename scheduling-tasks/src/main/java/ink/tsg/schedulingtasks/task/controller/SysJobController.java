package ink.tsg.schedulingtasks.task.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 定时任务调度表 前端控制器
 * </p>
 *
 * @author GEO_TSG
 * @since 2021-04-10
 */
@RestController
@RequestMapping("/task/sys-job")
public class SysJobController {

}
