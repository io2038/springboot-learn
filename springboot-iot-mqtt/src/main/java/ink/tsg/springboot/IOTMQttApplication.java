package ink.tsg.springboot;

import ink.tsg.springboot.subsribe.MQTTSubsribe;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class IOTMQttApplication {

    public static void main(String[] args) {
        SpringApplication.run(IOTMQttApplication.class,args);
    }
    @Autowired
    private MQTTSubsribe mqttSubsribe;

    /**
     * 接受订阅的接口和消息,mqtt消费端
     */
    @PostConstruct
    public void consumeMqttClient() throws MqttException {
        mqttSubsribe.init();
    }

}
