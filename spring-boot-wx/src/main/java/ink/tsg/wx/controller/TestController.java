package ink.tsg.wx.controller;

import ink.tsg.wx.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    private ITestService testService;

    @GetMapping("/hellowx")
    public void hellowx(HttpServletRequest request, HttpServletResponse response){
        testService.hellowx(request,response);
    }

    @PostMapping("/hellowx")
    public void userMSG(HttpServletRequest request, HttpServletResponse response){
        Map<String,String> map = testService.parseuserMSG(request, response);
        // testService.hellowx(request,response);
    }


}
