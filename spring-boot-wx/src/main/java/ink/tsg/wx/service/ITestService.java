package ink.tsg.wx.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface ITestService {
    void hellowx(HttpServletRequest request, HttpServletResponse response);

    Map<String, String> parseuserMSG(HttpServletRequest request,HttpServletResponse response);
}
