package ink.tsg.javabase.enumTest;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Color.java
 * @Description
 * @createTime 2021-05-27
 */
public enum Color {
    RED("1","aaa"), GREEN("2","bbb"), BLUE("3","cccc");

    private final String pare;

    Color(String code,String pare) {
        this.pare = pare;
    }
    public String getCode()
    {
        return pare;
    }

}
