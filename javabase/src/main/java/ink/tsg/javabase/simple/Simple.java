package ink.tsg.javabase.simple;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Simple.java
 * @Description
 * @createTime 2022-01-13
 */
public class Simple {


    private static volatile Simple simple ;

    private Simple() {
    }

    public static Simple getSimple(){
            if(simple==null){
                synchronized (Simple.class){
                    if(simple==null){
                        simple = new Simple();
                    }
                }
            }
        return simple;
    }
}
