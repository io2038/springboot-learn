package ink.tsg.javabase.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayToArrayList {
    public static void main(String[] args) {

        ArrayList<String> strings = new ArrayList<>();
        strings.add("aaaa");
        strings.add("dddd");
        strings.add("cccc");
        strings.add("qqqq");
        System.out.println(strings);

    }

}
