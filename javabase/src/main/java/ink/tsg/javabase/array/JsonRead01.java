package ink.tsg.javabase.array;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import io.netty.util.CharsetUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName JsonRead01.java
 * @Description
 * @createTime 2021-07-01
 */
public class JsonRead01 {

    public static void main(String[] args) throws IOException {

        String srcPath = "C:\\Users\\Administrator\\Desktop\\fifth_json.json";
        JSON json = JSONUtil.readJSONArray(new File(srcPath), CharsetUtil.UTF_8);
        List list = json.toBean(List.class);
        double minx = Double.MAX_VALUE;
        double miny = Double.MAX_VALUE;
        for (Object o : list) {
            List item = (List) o;
            BigDecimal lonBig = (BigDecimal) item.get(0);
            minx = Math.min(lonBig.doubleValue(),minx);
            BigDecimal latBig = (BigDecimal) item.get(1);
            miny = Math.min(latBig.doubleValue(),miny);
        }
        System.out.println(minx);
        System.out.println(miny);
        ArrayList<Object> objects = new ArrayList<>();
        for (Object o : list) {
            ArrayList<Object> item3d = new ArrayList<>();
            List item = (List) o;
            BigDecimal lonBig = (BigDecimal) item.get(0);
            item3d.add( (int)((lonBig.doubleValue()%minx)*1000));
            BigDecimal latBig = (BigDecimal) item.get(1);
            item3d.add((int)((latBig.doubleValue()%miny)*1000));
            item3d.add(Double.parseDouble((String) item.get(2)));
            item3d.add(Double.parseDouble((String) item.get(3)));
            objects.add(item3d);
        }
        System.out.println(objects);
    }
}
