package ink.tsg.javabase.thread;

import java.util.*;

/**
 * 题目：
 * 有一个list，元素类型为整形（设定元素值不超过100），现有一个整数A，
 * 若是list中有3个元素相加的和为A，输出这三个元素在list中的index，输出一组即可，若是没有，输出null

 * 要求：
 * 1.使用java完成，体现面向对象的设计
 * 2.体现代码的稳定性、可靠性
 * 3.提供完整的单元测试
 * */
public class ThreadPoolExecutorTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(3);
        list.add(1);
        list.add(6);
        list.add(8);
        // System.out.println(list);
        EleSumTarget eleSumTarget = new EleSumTarget(list,8,3);
        List<Integer> calcul = eleSumTarget.calcul();
    }

}
