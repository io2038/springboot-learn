**离散点最小（凸）包围边界查找**

参考：https://blog.csdn.net/manerfan/article/details/46610683

算法的实现思路，简要如下

1. 找到离散点中，保证y坐标最大的情况下，x坐标最小的点，记做A点
2. 以A点为原点，x轴正反向射线（Ax−→Ax→）顺时针扫描，找到旋转角最小时扫描到的点，记做B点
3. 以B点为原点，AB方向射线（AB−→−AB→）顺时针扫描，找到旋转角最小时扫描到的点，记做C点
4. 以C点为原点，BC方向射线（BC−→−BC→）顺时针扫描，找到旋转角最小时扫描到的点，记做D点
5. 以此类推，直到找到起始点A

![1](https://img-blog.csdn.net/20150623225525008)

![2](https://img-blog.csdn.net/20150623225921049)

![3](https://img-blog.csdn.net/20150623230149494)

![n](https://img-blog.csdn.net/20150623230500257)

![效果图](https://img-blog.csdn.net/20150623231209803)

