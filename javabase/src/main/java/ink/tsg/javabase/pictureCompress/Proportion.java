package ink.tsg.javabase.pictureCompress;

import cn.hutool.core.io.FileUtil;
import net.coobird.thumbnailator.Thumbnails;

import java.io.IOException;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Proportion.java
 * @Description 按比例压缩图片
 * @createTime 2021-06-07
 */
public class Proportion {
    public static final String[] STATUS_CODE = { "1", "2"};
    public static void main(String[] args) throws IOException {
        Thumbnails.of("E:\\工作文档\\blue.jpg").scale(0.25f).toFile(FileUtil.touch("D:\\geomative\\uploadPath\\abnormal\\20210529\\be815dc4d1774e08a5ae8c5c28353357\\1129A1_007_01\\profileImg\\compress\\pro12.bmp"));
    }

}
