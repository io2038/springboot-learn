package ink.tsg.javabase.interview.enumeration;

public class TestEnum {
    public static void main(String[] args) {
        System.out.println(PizzaStatus.ORDERED.name());//ORDERED
        System.out.println(PizzaStatus.ORDERED);//ORDERED
        System.out.println(PizzaStatus.ORDERED.name().getClass());//class java.lang.String
        System.out.println(PizzaStatus.ORDERED.getClass());//class shuang.kou.enumdemo.enumtest.PizzaStatus
        System.out.println(PizzaStatus.ORDERED.ordinal());
        System.out.println(PizzaStatus.valueOf("ORDERED"));
    }
}
