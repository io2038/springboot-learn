package ink.tsg.javabase.interview.enumeration;

public enum  PizzaStatus {

    ORDERED,
    READY,
    DELIVERED;

}
