package ink.tsg.javabase.finalTest;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Test.java
 * @Description
 * @createTime 2022-03-02
 */
public class Test extends FinalMethod {


    public static void main(String[] args) {
//        FinalClassTest finalClassTest = new FinalClassTest();
//        System.out.println(finalClassTest.add());
//        System.out.println(romanToInt("LVIII"));
        System.out.println(isValid("{[]}"));

    }

    public static boolean isValid(String s) {
        HashMap<Character, Character> map = new HashMap<>();
        map.put('(', ')');
        map.put('{', '}');
        map.put('[', ']');
        Stack<Character> stack = new Stack<>();
        boolean flag = true;
        for (int i = 0; i < s.length(); i++) {
            char c = s.toCharArray()[i];
            if (map.containsKey(c)) {
                stack.push(map.get(c));
            } else {
                if (stack.isEmpty() || !stack.pop().equals(c)) {
                    flag = false;
                    break;
                }
            }
        }
        if (stack.size() == s.length() || stack.size() != 0) {
            flag = false;
        }

        return flag;
    }

    public static String longestCommonPrefix(String[] strs) {
        String result = strs[0];
        for (int j = 1; j < strs.length; j++) {
            result = longestCommonPrefix1(result, strs[j]);
            if (result.length() == 0) {
                break;
            }
        }
        return result;
    }

    private static String longestCommonPrefix1(String result, String str) {
        int length = Math.min(result.length(), str.length());
        int index = 0;
        while (index < length && result.charAt(index) == str.charAt(index)) {
            index++;
        }
        return result.substring(0, index);
    }


    public static int romanToInt(String s) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {

            int j = i + 1 == s.length() ? i : i + 1;

            if (map.get(s.charAt(i)) < map.get(s.charAt(j))) {
                sum = sum + map.get(s.charAt(i + 1)) - map.get(s.charAt(i));
                i++;
            } else {
                sum += map.get(s.charAt(i));
            }
        }
        return sum;
    }

}
