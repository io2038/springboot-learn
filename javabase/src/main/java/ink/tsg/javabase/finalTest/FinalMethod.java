package ink.tsg.javabase.finalTest;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName FinalMethod.java
 * @Description
 * @createTime 2022-03-02
 */
public class FinalMethod {
    int a = 0;
    final public int add(){
        return ++a;
    }
}
