package ink.tsg.javabase.finalTest;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName FinalClassTest.java
 * @Description
 * @createTime 2022-03-02
 */
public class FinalClassTest {
    int a = 0;
    public int add(){
        return ++a;
    }
}
