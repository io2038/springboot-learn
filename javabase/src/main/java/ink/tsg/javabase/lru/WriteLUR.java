package ink.tsg.javabase.lru;

import java.util.HashMap;
import java.util.Map;

public class WriteLUR {

    // 构造一个Node节点作为数据载体
    class Node<K,V>{
        K key;
        V value;
        Node<K,V> pre;
        Node<K,V> next;
        public Node(){
            this.pre = this.next = null;
        }
        public Node(K key,V value){
            this.key = key;
            this.value = value;
            this.pre = this.next = null;
        }
    }
    // 创建一个双向队列,存放Node
    class DoubleLinkedList<K,V>{
        Node<K,V> head;
        Node<K,V> tail;
        public DoubleLinkedList(){
            head = new Node<>();
            tail = new Node<>();
            head.next = tail;
            tail.pre = head;
        }
        // 添加到头，以头为基准
        public void addHead(Node<K,V> node){
            node.next = head.next;
            node.pre = head;
            head.next.pre = node;
            head.next = node;
        }
        // 删除节点
        public void removeNode(Node<K,V> node){
            node.next.pre = node.pre;
            node.pre.next = node.next;
            node.pre = null;
            node.next = null;
        }
        // 获取尾节点
        public Node getLast(){
            return tail.pre;
        }


    }

    private int cacheSize;
    Map<Integer,Node<Integer,Integer>> map;
    DoubleLinkedList<Integer,Integer> doubleLinkedList;

    //构造方法
    public WriteLUR(int cacheSize){
        this.cacheSize = cacheSize;
        map = new HashMap<>(); //查找
        doubleLinkedList = new DoubleLinkedList<>();
    }
    // 得到
    public int get(int key){
        if(!map.containsKey(key)){
            return -1;
        }
        Node<Integer, Integer> node = map.get(key);
        doubleLinkedList.removeNode(node);
        doubleLinkedList.addHead(node);
        return node.value;
    }
    public void put(int key,int value){
        if(map.containsKey(key)){ // 更新
            Node<Integer, Integer> node = map.get(key);
            node.value = value;
            map.put(key,node);
            doubleLinkedList.removeNode(node);
            doubleLinkedList.addHead(node);
        }else{  // 新增
            if(map.size() == cacheSize){    // 内存满了
                Node<Integer,Integer> lastNode = doubleLinkedList.getLast();
                map.remove(lastNode.key);
                doubleLinkedList.removeNode(lastNode);
            }
            Node<Integer, Integer> node = new Node<>(key,value);
            map.put(key,node);
            doubleLinkedList.addHead(node);
        }
    }
    public static void main(String[] args) {
        BaseLinkHashMap<Integer, String> lruMap = new BaseLinkHashMap<>(3);
        lruMap.put(1,"a");
        lruMap.put(2,"b");
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(4,"d");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(5,"q");
        System.out.println(lruMap.keySet());
    }
}
