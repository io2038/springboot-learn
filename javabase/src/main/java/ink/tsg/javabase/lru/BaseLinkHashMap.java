package ink.tsg.javabase.lru;


import java.util.LinkedHashMap;
import java.util.Map;

public class BaseLinkHashMap<K,V> extends LinkedHashMap<K,V> {
    private int capacity;   //缓存大小

    public BaseLinkHashMap(int capacity) {
        /*
         * accessOrder:
         *  the ordering mode - <tt>true</tt> for
         *  access-order, <tt>false</tt> for insertion-order
         **/
        super(capacity,0.75F,false);
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return super.size()>capacity;
    }

    public static void main(String[] args) {
        BaseLinkHashMap<Integer, String> lruMap = new BaseLinkHashMap<>(3);
        lruMap.put(1,"a");
        lruMap.put(2,"b");
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(4,"d");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(3,"c");
        System.out.println(lruMap.keySet());
        lruMap.put(5,"q");
        System.out.println(lruMap.keySet());
    }
}
