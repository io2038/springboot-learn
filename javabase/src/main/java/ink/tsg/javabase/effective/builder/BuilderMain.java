package ink.tsg.javabase.effective.builder;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName BuilderMain.java
 * @Description 测试主入口
 * @createTime 2021-05-18
 */
public class BuilderMain {

    public static void main(String[] args) {
        NutritionFacts cocaCola = new NutritionFacts.Builder(240, 8)
                .calories(100).sodium(35).carbohydrate(27).build();
    }

}
