package ink.tsg.javabase.graphics;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Demo01.java
 * @Description
 * @createTime 2021-06-30
 */
public class Demo02 {

    public static void main(String[] args) throws IOException {
        BufferedImage image = (BufferedImage) ImgUtil.rotate(ImageIO.read(FileUtil.file("D:\\test1.png")), 180);
        ImgUtil.write(image, FileUtil.file("D:\\ert1.png"));
        System.out.println("aaa");
    }

}
