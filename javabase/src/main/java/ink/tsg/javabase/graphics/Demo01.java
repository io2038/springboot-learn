package ink.tsg.javabase.graphics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Demo01.java
 * @Description
 * @createTime 2021-06-30
 */
public class Demo01 {

    public static void main(String[] args) throws IOException {
        //获取图片缓冲区
        BufferedImage newimage = new BufferedImage(4000, 4000,
                BufferedImage.TYPE_INT_RGB);
        //获取图片的画布
        Graphics2D graphics = newimage.createGraphics();
        graphics.setColor(Color.white);
        graphics.setBackground(Color.white);
        graphics.fillRect(0, 0, 4000, 4000);
        for(int j=0;j<4000;j++){
            for (int i = 0;i<4000;i++){
















                //BufferedImage bi = new BufferedImage(1,1,BufferedImage.TYPE_INT_RGB);

                //得到绘制坏境(这张图片的笔)

                //Graphics gh= (Graphics)bi.getGraphics();

                if(10<=i&&i<11&&10<=j&&j<11){
                    graphics.setColor(Color.yellow);//设置颜色
                    graphics.fillRect(i,j,1,1);
                }else if(20<i&&i<=30&&30<=j&&j<40){
                    graphics.setColor(Color.orange);//设置颜色
                    graphics.fillRect(i,j,1,1);
                }else if(30<i&&i<60&&60<=j&&j<80){
                    graphics.setColor(Color.RED);//设置颜色
                    graphics.fillRect(i,j,1,1);
                }else{
                    graphics.setColor(Color.blue);//设置颜色
                    graphics.fillRect(i,j,1,1);
                }
//                gh.fillRect(0, 0, 1, 1);//填充整张图片(其实就是设置背景色)

                //gh.setColor(Color.lightGray);

                //gh.drawRoundRect(0, 0, 130, 70, 130, 70);//绘制一个圆形边框

                //gh.drawRect(0, 0, 40-1, 40-1);//绘制一个四边形边框

                // gh.setFont(new Font("Bernard MT",Font.BOLD,18));//字体样式 字体格式 字体大小

                //gh.setColor(Color.BLUE);//设置字体颜色

                //gh.drawString("aaaa", 22, 25);//向图片上写随机字符串
//                g.drawImage(bi,i*40,j*40,0,0,null);
            }
        }


        ImageIO.write(newimage, "PNG", new FileOutputStream("D:/test.png"));//把图片输出到指定路径
    }

}
