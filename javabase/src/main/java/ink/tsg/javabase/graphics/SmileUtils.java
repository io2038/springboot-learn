package ink.tsg.javabase.graphics;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;
import smile.interpolation.BicubicInterpolation;
import smile.interpolation.CubicSplineInterpolation2D;
import smile.interpolation.KrigingInterpolation1D;
import smile.interpolation.KrigingInterpolation2D;
import smile.plot.swing.*;
import smile.plot.swing.Canvas;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SmileUtils.java
 * @Description
 * @createTime 2021-08-11
 */
public class SmileUtils {


    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
//        kriging1D();
        kriging2D();
//        splineInterpolator();

    }

    private static void splineInterpolator() {

        double[] x = {0.0, 1.0, 2.0};
        double[] y = {1.0, -1.0, 2.0};
        UnivariateInterpolator interpolator = new SplineInterpolator();
        UnivariateFunction function = interpolator.interpolate(x, y);
        double interpolationX = 0.5;
        double interpolatedY = function.value(interpolationX);
        System.out.println("f(" + interpolationX + ") = " + interpolatedY);
    }

    private static void kriging2D() throws InvocationTargetException, InterruptedException {
        double[] x= {0.0, 5.2,3.1,6.4,9.8,4.6,7.5,8.4,9.4,1.5};
        double[] y = {0.0, 7.5,1.5,5.2,3.1,6.4,8.4,9.4,9.8,4.6};
        double[] z = {0.0, 120.0,138.0,122.0,145.0,163.0,185.2,168.6,178.6,196.5};

        KrigingInterpolation2D instance = new KrigingInterpolation2D(x, y, z, 1.1);
        double[][] data = new double[x.length][x.length];
        for (int i = 0; i < x.length; i++) {
            for(int j = 0; j < y.length; j++){
                data[i][j] = instance.interpolate(x[i], y[j]);
            }
        }
        System.out.println(data);
        Heatmap heatmap = new Heatmap(x, y, data, new Color[]{Color.CYAN,Color.blue,Color.MAGENTA,Color.GRAY,Color.RED,Color.ORANGE,Color.DARK_GRAY,Color.YELLOW,Color.PINK,Color.blue});
        Canvas canvas = heatmap.canvas();
//        LinePlot plot = LinePlot.of(data, Color.CYAN);
//        Canvas canvas = plot.canvas();
        canvas.window();

    }

    private static void kriging1D() throws InvocationTargetException, InterruptedException {
        double[] x = {0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
        double[] y = {0.0, 0.8415, 0.9093, 0.1411, -0.7568, -0.9589, -0.2794};
        KrigingInterpolation1D kriging = new KrigingInterpolation1D(x, y);

        double[][] data = new double[61][2];
        for (int i = 0; i < data.length; i++) {
            data[i][0] = i * 0.1;
            data[i][1] = kriging.interpolate(data[i][0]);
        }
        System.out.println(Arrays.toString(data));
//        for (int i = 0; i < data.length; i++) {
//            System.out.println(Arrays.toString(data[i]));
//        }
        LinePlot plot = LinePlot.of(data, Color.CYAN);
        Canvas canvas = plot.canvas();
//        canvas.line("Kriging", data, Color.CYAN)
        canvas.window();
    }
}
