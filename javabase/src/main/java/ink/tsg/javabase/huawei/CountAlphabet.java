package ink.tsg.javabase.huawei;

import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;

import static java.lang.Character.toUpperCase;

/**
 * @Author dzsg
 * @Description写出一个程序，接受一个由字母、数字和空格组成的字符串，
 *  和一个字母，然后输出输入字符串中该字母的出现次数。不区分大小写。
 *  第一行输入一个由字母和数字以及空格组成的字符串，第二行输入一个字母。
 * @Date 2021/2/1 9:54
 **/
public class CountAlphabet {

    public static void main(String[] args) {
        // Scanner in = new Scanner(System.in);
//        String line = in.nextLine();
//        Scanner in2 = new Scanner(line);
        //定义一个十六进制值
        // String strHex3 = "00001322";
        //将十六进制转化成十进制
        // int valueTen2 = Integer.parseInt(strHex3,16);
        // System.out.println(valueTen2);
        Scanner sc = new Scanner(System.in);
        float f = sc.nextFloat();
        if(f%Math.floor(f)>0.4){
            System.out.println((int)Math.floor(f)+1);
        }else{
            System.out.println((int)Math.floor(f));
        }
//        while(in2.hasNextInt()){
//            System.out.println(in2.nextLine());
//        }
//        do{
//            String s = sc.nextLine();
//            if("".equals(s)){
//                for (Integer i :integers ) {
//                    System.out.println(i);
//                }
//                break;
//            }else{
//                integers.add(Integer.parseInt(s));
//            }
//        }while (true);
    }

    private static int countAlphabet(String str, String str2) {
        int result=0;
        char[] chars = str.toCharArray();
        for (Character c : chars){
            if(toUpperCase(c)==toUpperCase(str2.toCharArray()[0])){
                result++;
            }
        }
        return result;
    }

}
