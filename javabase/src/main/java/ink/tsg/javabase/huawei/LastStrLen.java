package ink.tsg.javabase.huawei;
/**
 * @Author dzsg
 * @Description 计算字符串最后一个单词的长度，单词以空格隔开。
 * @Date 2021/2/1 9:31
 **/
public class LastStrLen {

    public static void main(String[] args) {
        String str = "XSUWHQ";
        int n = getLastLen(str);
        System.out.println(n);
    }

    private static int getLastLen(String str) {
        if(str.length()==0){
            return 0;
        }
        String trim = str.trim();
        int i = trim.lastIndexOf(" ")+1;
        return trim.length()-i;

    }

}
