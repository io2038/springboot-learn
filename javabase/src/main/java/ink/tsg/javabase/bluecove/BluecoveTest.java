package ink.tsg.javabase.bluecove;

import javax.bluetooth.RemoteDevice;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName BluecoveTest.java
 * @Description
 * @createTime 2022-03-10
 */
public class BluecoveTest {

    public static void main(String[] args) throws IOException, InterruptedException {
        //附近所有的蓝牙设备，必须先执行 runDiscovery
        Set<RemoteDevice> devicesDiscovered = RemoteDeviceDiscovery.getDevices();
        Iterator<RemoteDevice> itr = devicesDiscovered.iterator();
        System.out.println(itr);
    }
}
