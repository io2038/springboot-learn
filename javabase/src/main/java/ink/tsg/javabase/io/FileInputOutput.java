package ink.tsg.javabase.io;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;

import java.io.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName FileInputOutput.java
 * @Description 关于文件的输入流和输出流练习
 * @createTime 2021-05-11
 */
public class FileInputOutput {

    public static void main(String[] args) throws Exception {
//        file4InputStream();
        FileUtil.copy(new File("E:\\工作文档\\项目规划.png"), new File("E:\\工作文档\\tstst\\a.png"), true);
    }

    private static void file4InputStream() throws IOException {

        File file = new File("E:\\工作文档\\address.json");
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[(int)file.length()];
        stream.read(bytes);

        File file1 = new File("E:\\工作文档\\aaa.txt");
        BufferedOutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(file1));
        outputStream1.write(bytes);

        outputStream1.close();
        stream.close();
    }


}
