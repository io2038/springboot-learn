package ink.tsg.javabase.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @Author dzsg
 * @Description
 * 给定一个字符串s，找到最长子字符串的长度而不重复字符。
 * 范例1：
 * 输入： s =“ abcabcbb”
 * 输出： 3
 * 说明：答案为“ abc”，长度为3。
 * @Date 2020/11/9 10:52
 **/
public class LengthOfLongestSubstring {
    public static void main(String[] args) {
        System.out.println("请输入：");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int length = lengthOfLongestSubstring(s);
        System.out.println(length);

    }
    public static int  lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        char[] chars = s.toCharArray();
        if(s.length()==0){
            return 0;
        }
        int left=0,right=0,len=0;
        for(int i=0;i<chars.length;i++){
            if(map.containsKey(chars[i])){
                left = map.get(chars[i]);
            }else{
                map.put(chars[i],i);
                right++;
            }
            len = Math.max(len,right-left);
        }
        return len;
    }
}
