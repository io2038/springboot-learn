package ink.tsg.javabase.leetcode;

import java.util.Scanner;

/**
 * @Author dzsg
 * @Description
 * 给定一个整数数组 A，如果它是有效的山脉数组就返回 true，否则返回 false。
 * 如果 A 满足下述条件，那么它是一个山脉数组：
 * A.length >= 3
 * 在 0 < i < A.length - 1 条件下，存在 i 使得：
 * A[0] < A[1] < ... A[i-1] < A[i]
 * A[i] > A[i+1] > ... > A[A.length - 1]
 * @Date 2020/11/3 9:38
 **/
public class Mountains {

    public static void main(String[] args) {
        int[] a = {1,2,1};
        boolean b = validMountainArray(a);
        System.out.println(b);
    }
    public static boolean validMountainArray(int[] A) {
        if(A.length<3){
            return false;
        }else{
            int maxIndexLeft = 0, maxIndexRight = 0;
            for (int i = 0;i< A.length-1;i++){
                if(A[i]>A[i+1]){
                    maxIndexLeft = i;
                    break;
                }
            }
            for (int j = A.length-1;j>0;j--){
                if(A[j]>A[j-1]){
                    maxIndexRight = j;
                    break;
                }
            }
            if(maxIndexLeft == maxIndexRight && maxIndexLeft != 0){
                return true;
            }else{
                return false;
            }
        }
    }
}
