package ink.tsg.javabase.leetcode;

import java.util.Scanner;

/**
 * @Author dzsg
 * @Description 在一个 n * m 的二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个高效的函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 * <p>
 * 示例:
 * 现有矩阵 matrix 如下：
 * [
 * [1,   4,  7, 11, 15],
 * [2,   5,  8, 12, 19],
 * [3,   6,  9, 16, 22],
 * [10, 13, 14, 17, 24],
 * [18, 21, 23, 26, 30]
 * ]
 * 给定 target = 5，返回 true。
 * 给定 target = 20，返回 false。
 * @Date 2020/11/6 9:05
 **/
public class FindNumberIn2DArray {

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 4, 7, 11, 15},
                {2, 5, 8, 12, 19},
                {3, 6, 9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}
        };
        System.out.println("请输入目标值：");
        Scanner scanner = new Scanner(System.in);
        int target = scanner.nextInt();
//        boolean numberIn2DArray = findNumberIn2DArray(matrix, target);
        boolean numberIn2DArray = findNumberIn2DArray1(matrix, target);
        System.out.println(numberIn2DArray);
    }

    private static boolean findNumberIn2DArray1(int[][] matrix, int target) {

        return false;
    }
    /**
     * @Author dzsg
     * @Description
     * 暴力破解
     * @Date 2020/11/6 9:48
     * @Param [matrix, target]
     * @return boolean
     **/
    public static boolean findNumberIn2DArray(int[][] matrix, int target) {
        for(int i = 0;i<matrix.length;i++){
            for(int j=0;j<matrix[i].length;j++){
                if(matrix[i][j]==target){
                    return true;
                }
            }
        }
        return false;
    }

}
