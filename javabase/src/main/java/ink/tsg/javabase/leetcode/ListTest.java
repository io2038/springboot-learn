package ink.tsg.javabase.leetcode;


import java.util.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ListTest.java
 * @Description
 * @createTime 2022-04-01
 */
public class ListTest {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(1);
        list.add(5);
        list.add(8);
        list.add(-1);
        list.add(-3);
        System.out.println(list);
        Collections.rotate(list,3);
        System.out.println(list);

        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        list2.add(1);
        list2.add(5);
        list2.add(8);
        list2.add(-1);
        System.out.println(list2);
        Collections.rotate(list2,-1);
        System.out.println(list2);
    }
}
