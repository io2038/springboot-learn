package ink.tsg.javabase.leetcode;
import java.util.Scanner;

public class Dichotomy {
    public static void main(String[] args) {
        int[] array = {1,5,9,11,16,19,25};
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        Integer value = Integer.valueOf(s);
        int low = 0;
        int hight = array.length-1;
        while (low<hight){
            if(value==array[(low+hight)/2]){
                System.out.println("找到了");
                break;
            }
            if(value<array[(low+hight)/2]){
                hight = (low+hight)/2-1;
                continue;
            }
            if(value>array[(low+hight)/2]){
                low = (low+hight)/2+1;
                continue;
            }
        }
        System.out.println("不存在");

    }

}
