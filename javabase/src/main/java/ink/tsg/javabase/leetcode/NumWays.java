package ink.tsg.javabase.leetcode;

import java.util.Scanner;

/**
 * @Author dzsg
 * @Description
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级台阶。求该青蛙跳上一个 n 级的台阶总共有多少种跳法。
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 * @Date 2020/11/5 10:52
 * @Param
 * @return
 **/
public class NumWays {
    public static void main(String[] args) {
        System.out.println("请输入台阶数：");
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();

        int ways = numWays(i);
        System.out.println(ways);
    }
    public static int numWays(int n) {
        if(n==0){
            return 1;
        }
        if(n<3){
            return n;
        }else{
            return numWays(n-1)+numWays(n-2);
        }

    }
}
