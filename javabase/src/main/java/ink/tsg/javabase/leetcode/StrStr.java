package ink.tsg.javabase.leetcode;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName StrStr.java
 * @Description 给你两个字符串haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串出现的第一个位置（下标从 0 开始）。如果不存在，则返回-1 。
 * @createTime 2022-03-21
 */
public class StrStr {

    public static void main(String[] args) {
        System.out.println(strStr("mississippi", "issi"));
    }

    public static int strStr(String haystack, String needle) {
        return haystack.indexOf(needle);
    }

}
