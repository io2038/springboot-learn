package ink.tsg.javabase.leetcode;

import java.util.Stack;

/**
 * @Author dzsg
 * @Description
 * 输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
 * 示例 1：
 * 输入：head = [1,3,2]
 * 输出：[2,3,1]
 * @Date 2020/11/4 9:46
 **/
public class ReversePrint {
    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        int[] ints = reversePrint(listNode);
        System.out.println(ints);
    }

    public static int[] reversePrint(ListNode head) {

        Stack<Integer> st = new Stack<Integer>();
        int size=0;
        while (head!=null){
           st.push(head.val);
           head = head.next;
           size++;
        }
        int[] ints = new int[size];
        for (int i =0;i<size;i++){
            ints[i] = st.pop();
        }
        return ints;
    }

}

//class ListNode {
//    int val;
//    ListNode next;
//    ListNode(int x) { val = x; }
//
//
//}