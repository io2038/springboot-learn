package ink.tsg.javabase.leetcode;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.yaml.snakeyaml.nodes.ScalarNode;

import java.util.Scanner;

/**
 * @Author dzsg
 * @Description
 * 给定只含 "I"（增大）或 "D"（减小）的字符串 S ，令 N = S.length。
 * 返回 [0, 1, ..., N] 的任意排列 A 使得对于所有 i = 0, ..., N-1，都有：
 * 如果 S[i] == "I"，那么 A[i] < A[i+1]
 * 如果 S[i] == "D"，那么 A[i] > A[i+1]
 * 示例 1：
 * 输入："IDID"
 * 输出：[0,4,1,3,2]
 *
 * @Date 2020/11/3 10:44
 **/
public class ChangesStr {

    public static void main(String[] args) {
        System.out.println("请输入字符串：");
        Scanner sc  = new Scanner(System.in);
        String str = sc.nextLine();
        int[] ints = diStringMatch(str);
        System.out.println(ints);

    }
    public static int[] diStringMatch(String S) {
        int N = S.length();
        int lo = 0, hi = N;
        int[] ans = new int[N + 1];
        for (int i = 0; i < N; ++i) {
            if (S.charAt(i) == 'I')
                ans[i] = lo++;
            else
                ans[i] = hi--;
        }

        ans[N] = lo;
        return ans;
    }
}
