package ink.tsg.javabase.leetcode;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName IsPalindrome.java
 * @Description 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 *              说明：本题中，我们将空字符串定义为有效的回文串。
 * @createTime 2022-04-11
 */
public class IsPalindrome {

    public static void main(String[] args) {

        System.out.println(isPalindrome(" A man, a plan, a canal: Panama"));
    }

    public static boolean isPalindrome(String s) {
        return false;
    }
}
