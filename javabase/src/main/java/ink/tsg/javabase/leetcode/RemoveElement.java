package ink.tsg.javabase.leetcode;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName RemoveElement.java
 * @Description
 * @createTime 2022-03-11
 */
public class RemoveElement {

    public static void main(String[] args) {
        System.out.println(removeElement(new int[]{1,1}, 2));
    }

    public static int removeElement(int[] nums, int val) {
        if(nums.length == 1&&nums[0] != val){
            return nums.length;
        }
        int p1 = 0,p2=1;
        while(p2<nums.length){
            if(nums[p1]!=val){
                p1++;
            }
            if(nums[p1] == val&&nums[p2]!=val){
                nums[p1] = nums[p2];
                nums[p2] = val;
                p1++;
            }
            p2++;
        }
        return p1;
    }
}
