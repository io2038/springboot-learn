package ink.tsg.javabase.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SearchMatrix.java
 * @Description
 * 编写一个高效的算法来搜索 mxn 矩阵 matrix 中的一个目标值 target 。该矩阵具有以下特性：
 *
 * 每行的元素从左到右升序排列。
 * 每列的元素从上到下升序排列。
 *输入：matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
 * 输出：true
 * @createTime 2021-10-25
 */
public class SearchMatrix {

    public static void main(String[] args) {
        int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        int target = 5;

//        searchMatrix();
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("ddd");
        list = list.stream().filter(userName -> !userName.
                equals("ccc")).collect(Collectors.toList());
        System.out.println(list);
    }

    public static boolean searchMatrix(int[][] matrix, int target) {
        return false;
    }
}
