package ink.tsg.javabase.leetcode;

/***
 * @Author dzsg
 * @Description
 * 给定整数数组 A，每次 move 操作将会选择任意 A[i]，并将其递增 1。
 * 返回使 A 中的每个值都是唯一的最少操作次数。
 * 输入：[3,2,1,2,1,7]
 * 输出：6
 * 解释：经过 6 次 move 操作，数组将变为 [3, 4, 1, 2, 5, 7]。
 * 可以看出 5 次或 5 次以下的 move 操作是不能让数组的每个值唯一的。
 *
 * @Date 2020/11/3 14:42
 **/
public class MinIncrementForUnique {

    public static void main(String[] args) {
        int[] a = {3,2,1,2,1,7};
        int i = minIncrementForUnique(a);
        System.out.println(i);
    }
    public static int minIncrementForUnique(int[] A) {

        for(int i = 0;i<A.length-1;i++){



        }


        return 0;
    }
}
