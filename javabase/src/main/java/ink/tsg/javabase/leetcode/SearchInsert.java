package ink.tsg.javabase.leetcode;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SearchInsert.java
 * @Description
 * @createTime 2022-03-22
 */
public class SearchInsert {

    public static void main(String[] args) {
        System.out.println(searchInsert(new int[]{2,3,5,6,9},7));
    }


    public static int searchInsert(int[] nums, int target) {
        int n = nums.length;
        int left=0,right=n-1,result = n;
        while (left>=right){
            int mid = (left+right)/2+left;
            if(target<nums[mid]){
                result = mid;
                right = mid-1;
            }else{
                left = mid+1;
            }

        }
        return result;
    }
}
