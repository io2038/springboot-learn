package ink.tsg.javabase.leetcode;

import java.util.Arrays;

/**
 * @Author dzsg
 * @Description 给定一个按非递减顺序排序的整数数组 A，返回每个数字的平方数组，也按排序的非递减顺序返回。
 * Example 1:
 * Input: [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 * Example 2:
 * Input: [-7,-3,2,3,11]
 * Output: [4,9,9,49,121]
 * @Date 2020/11/10 10:33
 **/
public class SortedSquares {

    public static void main(String[] args) {
        int[] nums = {-7,-3,1,2,11};
        int[] ints = sortedSquares(nums);
        System.out.println(ints);
    }

    public static int[] sortedSquares(int[] A) {
        for(int i =0;i<A.length;i++){
            A[i] = (int) Math.pow( A[i],2);
        }
        Arrays.sort(A);
        return A;
    }
}
