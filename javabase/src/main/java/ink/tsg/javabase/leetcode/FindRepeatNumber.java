package ink.tsg.javabase.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author dzsg
 * @Description
 * 找出数组中重复的数字。
 * 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。
 * 数组中某些数字是重复的，但不知道有几个数字重复了，
 * 也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。
 * 示例 1：
 * 输入：
 * [2, 3, 1, 0, 2, 5, 3]
 * 输出：2 或 3
 * @Date 2020/11/3 16:56
 **/
public class FindRepeatNumber {
    public static void main(String[] args) {
        int[] nums = {7,2,2};
        //int number = findRepeatNumber(nums);
        //int number2 = findRepeatNumber2(nums);
        int number3 = officialMethod(nums);
        System.out.println(number3);
    }
    /**
     * @Author dzsg
     * @Description
     * 由于只需要找出数组中任意一个重复的数字，因此遍历数组，遇到重复的数字即返回。
     * 为了判断一个数字是否重复遇到，使用集合存储已经遇到的数字，如果遇到的一个数字已经在集合中，则当前的数字是重复数字。
     * 初始化集合为空集合，重复的数字 repeat = -1
     * 遍历数组中的每个元素：
     * 将该元素加入集合中，判断是否添加成功
     * 如果添加失败，说明该元素已经在集合中，因此该元素是重复元素，将该元素的值赋给 repeat，并结束遍历
     * 返回 repeat
     * @Date 2020/11/4 9:26
     * @Param [nums]
     * @return int
     **/
    private static int officialMethod(int[] nums){
        Set<Integer> set = new HashSet<Integer>();
        int repeat = -1;
        for (int num : nums) {
            // 如果set内不包含要加入的元素则返回为true
            if (!set.add(num)) {
                repeat = num;
                break;
            }
        }
        return repeat;
    }

    /**
     * @Author dzsg
     * @Description
     * 如果没有重复数字，那么正常排序后，数字i应该在下标为i的位置，所以思路是重头扫描数组，遇到下标为i的数字如果不是i的话，
     * （假设为m),那么我们就拿与下标m的数字交换。在交换过程中，如果有重复的数字发生，那么终止返回ture
     * 有bug：{7,2,2}
     * @Date 2020/11/4 9:15
     * @Param [nums]
     * @return int
     **/
    private static int findRepeatNumber2(int[] nums) {
        int temp;
        for(int i=0;i<nums.length;i++){
            while (nums[i]!=i){
                if(nums[i]==nums[nums[i]]){
                    return nums[i];
                }
                temp=nums[i];
                nums[i]=nums[temp];
                nums[temp]=temp;
            }
        }
        return -1;
    }
    /***
     * @Author dzsg
     * @Description
     * 时间复杂度太高
     * @Date 2020/11/4 9:16
     * @Param [nums]
     * @return int
     **/
    public static int findRepeatNumber(int[] nums) {
        int result = 0;
        for(int i=0;i<nums.length;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[i]==nums[j]){
                    result = nums[i];
                    break;
                }

            }
            if(result!=0){
                break;
            }
        }
        return result;
    }
}
