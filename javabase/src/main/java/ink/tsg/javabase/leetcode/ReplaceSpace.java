package ink.tsg.javabase.leetcode;

import java.util.Scanner;

/**
 * @Author dzsg
 * @Description
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 * 示例 1：
 * 输入：s = "We are happy."
 * 输出："We%20are%20happy."
 * @Date 2020/11/4 9:32
 **/
public class ReplaceSpace {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String  replaceSpace= replaceSpace(s);
        System.out.println(replaceSpace);

    }

    private static String replaceSpace(String s) {

        String replace = s.replace(" ", "%20");


        return replace;
    }


}
