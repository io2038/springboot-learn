package ink.tsg.javabase.leetcode;

import java.util.Arrays;
import java.util.Spliterator;

/**
 * @Author dzsg
 * @Description
 * 给定一个已排序的数组nums，就地删除重复项，以使每个元素仅出现一次并返回新的长度。
 * 不要为另一个数组分配额外的空间，必须通过使用O（1）额外的内存就地修改输入数组来做到这一点。
 * 范例1：
 * 输入： nums = [1,1,2]
 * 输出： 2，nums = [1,2]
 * 说明： 函数应返回length = 2，前两个元素分别为numsbe1和2。剩下的长度与返回的长度无关紧要。
 * @Date 2020/11/12 9:09
 **/
public class RemoveDuplicates {

    public static void main(String[] args) {
        int[] nums = {1,2};
        int i = removeDuplicates(nums);
        System.out.println(i);
    }


    public static int removeDuplicates(int[] nums) {
        if(nums.length<2){
            return nums.length;
        }
        int p1 =2,p2=1;
        while(p1<nums.length+1){
            if(nums[p1]!=nums[p2]){
                nums[p2] = nums[p1];
                nums[p2+1] = nums[p1];
                p2++;
            }
            ++p1;
        }
        return p2;
    }


}
