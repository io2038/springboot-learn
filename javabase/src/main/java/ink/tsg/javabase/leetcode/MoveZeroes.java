package ink.tsg.javabase.leetcode;

import java.util.Arrays;

/**
 * @Author dzsg
 * @Description 给定一个数组nums，编写一个函数将所有0'移到它的末尾，同时保持非零元素的相对位置不变。
 * 例：
 * 输入： [0,1,0,3,12]
 * 输出： [1,3,12,0,0]
 * @Date 2020/11/11 9:13
 * @Param
 * @return
 **/
public class MoveZeroes {

    public static void main(String[] args) {

        int[] nums = {0,1,0,3,12};
        moveZeroes(nums);
    }

    public static void moveZeroes(int[] nums) {
        if (nums.length < 2) {
            return;
        }
        int left = 0, right = 1, boundary = nums.length;
        while (right <= boundary) {
            if (right == boundary) {
                left = 0;
                right = 1;
                boundary--;
            }
            if (nums[left] == 0) {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
            }
            left++;
            right++;
            if(boundary==0){
                break;
            }
        }

    }

}
