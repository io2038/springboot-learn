package ink.tsg.javabase.leetcode;
/**
 * @Author dzsg
 * @Description
 * 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数位于数组的前半部分，所有偶数位于数组的后半部分。
 * 示例：
 * 输入：nums = [1,2,3,4]
 * 输出：[1,3,2,4]
 * 注：[3,1,2,4] 也是正确的答案之一。
 * @Date 2020/11/10 9:55
 **/
public class Exchange {

    public static void main(String[] args) {
        int[] nums = {1,2,3,4};
        int[] exchange = exchange(nums);
        System.out.println(exchange);
    }

    public static int[] exchange(int[] nums) {

        int left = 0,right=nums.length-1;
        while (right>left){
            if(nums[right]%2==0){
                right--;
            }else if (nums[left]%2==1){
                left++;
            }else{
                int temp = nums[right];
                nums[right] = nums[left];
                nums[left] = temp;
            }
        }
        return nums;
    }

}
