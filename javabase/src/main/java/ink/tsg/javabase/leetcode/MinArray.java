package ink.tsg.javabase.leetcode;

/*
 * @Author dzsg
 * @Description
 * 把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。输入一个递增排序的数组的一个旋转，输出旋转数组的最小元素。
 * 例如，数组 [3,4,5,1,2] 为 [1,2,3,4,5] 的一个旋转，该数组的最小值为1。
    示例 1：
    输入：[3,4,5,1,2]
    输出：1
    示例 2：
    输入：[2,2,2,0,1]
    输出：0
 * @Date 2020/11/5 11:56
 **/
public class MinArray {

    public static void main(String[] args) {

        int[] nums = {3, 4, 5, 1, 2,3,4};
        //int i = minArray(nums);
        int i = officeMethod(nums);
        System.out.println(i);
    }

    private static int officeMethod(int[] nums) {
        int left = 0, right = nums.length - 1;
        while (left<right){
            int mid = (left+right)/2;
            if(nums[mid]<nums[right]){
                right = mid;
            }else if (nums[mid]>nums[right]){
                left = mid;
            }else{
                right--;
            }
        }
        return nums[left];
    }


    public static int minArray(int[] numbers) {
        int min = numbers[0];
        for (int i = 0; i < numbers.length - 1; i++) {
            if (numbers[i] < numbers[i + 1]) {
                if (numbers[i] < min) {
                    min = numbers[i];
                } else {
                    continue;
                }
            } else {
                if (numbers[i + 1] < min) {
                    min = numbers[i + 1];
                } else {
                    continue;
                }
            }
        }
        return min;
    }
}
