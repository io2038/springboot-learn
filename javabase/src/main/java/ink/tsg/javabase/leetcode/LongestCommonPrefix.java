package ink.tsg.javabase.leetcode;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName LongestCommonPrefix.java
 * @Description
 * @createTime 2022-03-10
 */
public class LongestCommonPrefix {

    public static void main(String[] args) {
       String[] s = {"aaa","aa","aaa"};
        System.out.println(longestCommonPrefix(s));
    }

    public static String longestCommonPrefix(String[] strs) {
        if(strs.length==1){
            return strs[0];
        }
        String result = strs[0];
        for(int i=1;i<strs.length;i++){
            result = commonPrefix(result,strs[i]);
            if(result.length() == 0){
                break;
            }
        }
        return result;
    }

    public static String commonPrefix(String s1,String s2){
        int rang = Math.min(s1.length(),s2.length());
        int index = 0;
        while (index<rang){
            if(s1.charAt(index)==s2.charAt(index)){
                index++;
            }else{
                break;
            }
        }
        return s1.substring(0,index);
    }
}
