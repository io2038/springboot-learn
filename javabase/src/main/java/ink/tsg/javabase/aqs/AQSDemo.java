package ink.tsg.javabase.aqs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class AQSDemo {

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        new Thread(() -> {
            lock.lock();
            try{
                System.out.println("------A come in");
                try {
                    TimeUnit.MINUTES.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }finally {
                lock.unlock();
            }
        }, "a").start();

        new Thread(() -> {
            lock.lock();
            try{
                System.out.println("------b come in");
            }finally {
                lock.unlock();
            }
        }, "b").start();
    }
}
