package ink.tsg.javabase.guava;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.nio.charset.Charset;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName BloomFilterDemo.java
 * @Description 布隆过滤器
 * 布隆过滤器是可以用于判断一个元素是不是（可能）在一个集合里，并且相比于其它的数据结构，布隆过滤器在空间和时间方面都有巨大的优势。
 * 判断给定数据是否存在
 * 防止缓存穿透（判断请求的数据是否有效避免直接绕过缓存请求数据库）等等、邮箱的垃圾邮件过滤、黑名单功能等等。
 * @createTime 2021-11-04
 */
public class BloomFilterDemo {

    public static void main(String[] args) {

        /**
         * 创建一个插入对象为一亿，误报率为0.01%的布隆过滤器
         * 不存在一定不存在
         * 存在不一定存在
         * ----------------
         *  Funnel 对象：预估的元素个数，误判率
         *  mightContain ：方法判断元素是否存在
         */
        BloomFilter<CharSequence> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.forName("utf-8")), 100000000, 0.0001);
        bloomFilter.put("死");
        bloomFilter.put("磕");
        bloomFilter.put("Redis");
        System.out.println(bloomFilter.mightContain("Redis"));
        System.out.println(bloomFilter.mightContain("Java"));
    }
}
