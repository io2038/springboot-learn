package ink.tsg.javabase.utils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName GuavaUtils.java
 * @Description
 * @createTime 2021-08-19
 */
public class GuavaUtils {


    public static void main(String[] args) {
//        hashMultisetTest();
        multiMapTest();

    }

    /**
     * 添加一个元素时，先在Map中查找该元素对应的List，如果不存在则新建一个List对象。
     */
    private static void multiMapTest() {
        Multimap<String, Integer> multimap = ArrayListMultimap.create();
        multimap.put("a", 1);
        multimap.put("a", 2);
        multimap.put("a", 4);
        multimap.put("b", 3);
        multimap.put("c", 5);

        System.out.println(multimap.keys());//[a x 3, b, c]
        System.out.println(multimap.get("a"));//[1 ,2, 4]
        System.out.println(multimap.get("b"));//[3]
        System.out.println(multimap.get("c"));//[5]
        System.out.println(multimap.get("d"));//[]

        System.out.println(multimap.asMap());//{a=[1, 2, 4], b=[3], c=[5]}


    }

    /**
     * Multiset很像一个ArrayList，因为它允许重复元素，只不过它的元素之间没有顺序；同时它又具备Map<String, Integer>的某一些特性。
     * 但本质上它还是一个真正的集合类型——用来表达数学上“多重集合”的概念，这个例子只是恰好和Map对应上罢了。
     */
    private static void hashMultisetTest() {
        Multiset<String> multiset = HashMultiset.create();

        multiset.add("a");
        multiset.add("a");
        multiset.add("b", 5);//add "b" 5 times

        System.out.println(multiset.elementSet());//[a, b]
        System.out.println(multiset.count("a"));//2
        System.out.println(multiset.count("b"));//5
        System.out.println(multiset.count("c"));//0


    }

}
