package ink.tsg.javabase.utils;

import cn.hutool.system.oshi.OshiUtil;
import oshi.hardware.NetworkIF;

import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SystemUtils.java
 * @Description
 * @createTime 2021-09-10
 */
public class SystemUtils {

    public static void main(String[] args) {
        List<NetworkIF> networkIFs = OshiUtil.getNetworkIFs();
        for (NetworkIF networkIF : networkIFs) {
            networkIF.getIPv4addr();
        }
    }

}
