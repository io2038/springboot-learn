package ink.tsg.javabase.utils;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TestEnum.java
 * @Description
 * @createTime 2022-03-30
 */
public enum TestEnum {

    WENNER_ALPHA("1","Wenner (Alpha)"),
    POLE_POLE("2","Pole-Pole");

    private final String code;
    private final String info;

    TestEnum(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
