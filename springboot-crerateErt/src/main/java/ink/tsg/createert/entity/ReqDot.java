package ink.tsg.createert.entity;

import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ReqDot.java
 * @Description
 * @createTime 2021-12-08
 */
public class ReqDot {
    @NotNull(message = "文件不能为空")
    private MultipartFile calcu;
    @NotNull(message = "文件不能为空")
    private MultipartFile target;
    @DecimalMin(message = "步长必须是一个正数", value = "0.001")
    private Double step;
    @NotNull(message = "输出类型不能为空")
    private String fileType;

    public MultipartFile getCalcu() {
        return calcu;
    }

    public void setCalcu(MultipartFile calcu) {
        this.calcu = calcu;
    }

    public MultipartFile getTarget() {
        return target;
    }

    public void setTarget(MultipartFile target) {
        this.target = target;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
