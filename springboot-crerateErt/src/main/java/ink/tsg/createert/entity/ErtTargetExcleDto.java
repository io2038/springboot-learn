package ink.tsg.createert.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ErtTargetExcleDto.java
 * @Description
 * @createTime 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErtTargetExcleDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(index = 0)
    private Double point;

    @ExcelProperty(index = 1)
    private Double elevation;

    @ExcelProperty(index = 2)
    private Double resistivity;

    @ExcelProperty(index = 3)
    private Double x;

    @ExcelProperty(index = 4)
    private Double y;

    @ExcelProperty(index = 5)
    private Double elv;

    @ExcelProperty(index = 6)
    private Double eleDepth;
}
