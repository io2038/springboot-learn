package ink.tsg.recognition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName RecognitionApplication.java
 * @Description 智能识别
 * @createTime 2021-04-21
 */
@SpringBootApplication
public class RecognitionApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecognitionApplication.class,args);
    }

}
