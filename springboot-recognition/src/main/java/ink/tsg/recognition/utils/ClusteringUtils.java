package ink.tsg.recognition.utils;

import cn.hutool.core.collection.LineIter;
import cn.hutool.core.io.resource.ResourceUtil;
import smile.clustering.HierarchicalClustering;
import smile.clustering.linkage.CompleteLinkage;
import smile.plot.swing.Canvas;
import smile.plot.swing.Dendrogram;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ClusteringUtils.java
 * @Description 聚类算法
 * @createTime 2022-01-04
 */
public class ClusteringUtils {

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        final LineIter lineIter = new LineIter(ResourceUtil.getUtf8Reader("E:\\工作文档\\场地污染\\dat.csv"));
//        double[][] doubles = new double[1902][2];
        List<String> list = new ArrayList<>();
        for (String line : lineIter) {
            list.add(line);
        }
        double[][] doubles = new double[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            String[] split = list.get(i).split(",");
            double one = Double.parseDouble(split[1]);
            double two = Double.parseDouble(split[2]);
            doubles[i][0] = one+two;

            doubles[i][1] = Double.parseDouble(split[0]);
        }
        HierarchicalClustering clusters = HierarchicalClustering.fit(CompleteLinkage.of(doubles));
//        Dendrogram dendrogram = new Dendrogram(clusters.getTree(), clusters.getHeight());
//        Canvas canvas = dendrogram.canvas();
//        canvas.window();
        int[] partition = clusters.partition(6);

    }
}
