package ink.tsg.recognition.utils;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ImagesRecognitionUtils.java
 * @Description
 * @createTime 2021-04-21
 */
public class ImagesRecognitionUtils {


    public static void main(String[] args) throws TesseractException {
        ITesseract instance = new Tesseract();
        //如果未将tessdata放在根目录下需要指定绝对路径
        //设置训练库的位置
        instance.setDatapath("D:\\workspace\\Idea\\springboot-learn\\springboot-recognition\\tessdata");

        //如果需要识别英文之外的语种，需要指定识别语种，并且需要将对应的语言包放进项目中
        // chi_sim ：简体中文， eng    根据需求选择语言库
        instance.setLanguage("chi_sim");
//        instance.setLanguage("eng");

        // 指定识别图片
        File imgDir = new File("E:\\工作文档\\8jd5vfe42g.jpeg");
        long startTime = System.currentTimeMillis();
        String ocrResult = instance.doOCR(imgDir);

        // 输出识别结果
        System.out.println("识别结果: \n" + ocrResult + "\n 耗时：" + (System.currentTimeMillis() - startTime) + "ms");

    }
}
