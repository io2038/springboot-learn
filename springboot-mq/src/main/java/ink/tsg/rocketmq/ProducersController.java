//package ink.tsg.rocketmq;
//
//import com.alibaba.fastjson.JSON;
//import ink.tsg.entity.User;
//import org.apache.rocketmq.spring.core.RocketMQTemplate;
//import org.apache.rocketmq.spring.support.RocketMQHeaders;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.support.MessageBuilder;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.UUID;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName ProducersController.java
// * @Description
// * @createTime 2021-10-09
// */
//@RestController
//public class ProducersController {
//
//    @Autowired
//    private RocketMQTemplate template;
//
//    @RequestMapping("/producer")
//    public String producersMessage() {
//        User user = new User("sharfine", "123456789");
//        template.convertAndSend("demo-topic", user);
//
//        return JSON.toJSONString(user);
//    }
//
//    @RequestMapping("/producerOrder")
//    public String producerOrder() {
//        for (int i = 1; i <= 100; i++) {
//            User user = new User("sharfine", "密码：" + i);
//            template.syncSendOrderly("demo1-topic", user, String.valueOf(i));
//        }
//        return JSON.toJSONString("success");
//    }
//
//    @RequestMapping("/producerTransaction")
//    public String producerTransaction() {
//        /*
//	 	 * 这里的Message不是rocketmq.commen的
//	 	 * 是springframework的接口
//         */
//        User user = new User("sharfine", "密码：");
//
//        /*
//          * myTransactionGroup要和@RocketMQTransactionListener(txProducerGroup = "myTransactionGroup")定义的Group一致
//		  * 消息会通过TransactionGroup找到事务消费者、通过topic普通消费者 只有事务消费者commit 普通消费者的结果才会执行
//         */
//        template.sendMessageInTransaction(
//                "transaction-topic",
//                MessageBuilder.withPayload(user).setHeader(RocketMQHeaders.TRANSACTION_ID, UUID.randomUUID().toString()).build(),
//                user);
//        return JSON.toJSONString("success");
//    }
//
//}
