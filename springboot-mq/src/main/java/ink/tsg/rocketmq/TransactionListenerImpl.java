//package ink.tsg.rocketmq;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.IdWorker;
//import ink.tsg.entity.TRocketmq;
//import ink.tsg.mapper.TRocketmqMapper;
//import org.apache.rocketmq.client.producer.LocalTransactionState;
//import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
//import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
//import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
//import org.apache.rocketmq.spring.support.RocketMQHeaders;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.MessageHeaders;
//
//import javax.annotation.Resource;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName TransactionListenerImpl.java
// * @Description
// * @createTime 2021-10-12
// */
//@RocketMQTransactionListener
//public class TransactionListenerImpl implements RocketMQLocalTransactionListener {
//
//    @Resource
//    private TRocketmqMapper tRocketmqMapper;
//
//
//    /**
//     * 可以定义一个static final的Map 用来保存返回unknown要回查消息的一些属性 那么所有对象都可以获取该消息回滚前的一些信息
//     * private static Map<String, Object> INFO_MAP = new HashMap<>();
//     */
//
//
//    /**
//     * 执行本地事务
//     */
//    @Override
//    public RocketMQLocalTransactionState  executeLocalTransaction(Message message, Object o) {
//        System.out.println("执行本地事务");
//        // arg：sendMessageInTransaction方法的第四个参数，用于处于本地业务
//        try {
//            // 本地业务
//            // 消息头
//            MessageHeaders headers = message.getHeaders();
//            String transactionalId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
//            TRocketmq tRocketmq = new TRocketmq();
//            tRocketmq.setId((int)IdWorker.getId());
//            tRocketmq.setLog("执行本地事务");
//            tRocketmq.setTransactionId(transactionalId);
//            tRocketmqMapper.insert(tRocketmq);
//            return RocketMQLocalTransactionState.COMMIT;
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            return RocketMQLocalTransactionState.ROLLBACK;
//        }
//
//    }
//    /**
//     * 若在本地事务执行过程中缺少二次确认消息或生产者处于等待状态
//     * MQ Server将向同一组中的每个生产者发送检查消息
//     */
//    @Override
//    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
//        System.out.println("检查本地事务");
//        MessageHeaders headers = message.getHeaders();
//        //获取事务ID
//        String transactionId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
//        try {
//            TRocketmq rocketmq = tRocketmqMapper.selectOne(new QueryWrapper<TRocketmq>().eq("transaction_id", transactionId));
//            if(null != rocketmq){
//                return RocketMQLocalTransactionState.COMMIT;
//            }
//            return RocketMQLocalTransactionState.ROLLBACK;
//        } catch (Exception e) {
//            return RocketMQLocalTransactionState.ROLLBACK;
//        }
//    }
//}
