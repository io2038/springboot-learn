package ink.tsg.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 项目表
 * </p>
 *
 * @author GEO_ITEr
 * @since 2021-04-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "t_rocketmq")
public class TRocketmq implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    private String transactionId;

    private String log;

}
