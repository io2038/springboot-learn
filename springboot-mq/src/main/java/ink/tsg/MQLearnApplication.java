package ink.tsg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName MQLearnApplication.java
 * @Description
 * @createTime 2021-10-09
 */
@SpringBootApplication
@EnableJms
public class MQLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(MQLearnApplication.class,args);
    }
}
