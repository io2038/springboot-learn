package ink.tsg.activemq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.jms.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ActiveMQController.java
 * @Description
 * @createTime 2022-01-20
 */
@RestController
@RequestMapping("/active")
public class ActiveMQController {


    @Resource
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Resource
    private Topic topic;

    @GetMapping
    public String sendMsg(){
        this.jmsMessagingTemplate.convertAndSend(this.topic, "你好");
        return "success";
    }

}
