package ink.tsg.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.entity.TRocketmq;
import ink.tsg.mapper.TRocketmqMapper;
import org.springframework.stereotype.Service;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TRocketmqService.java
 * @Description
 * @createTime 2021-10-12
 */
@Service
public class TRocketmqService extends ServiceImpl<TRocketmqMapper, TRocketmq> {

}
