package ink.tsg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.entity.TRocketmq;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TRocketmqMapper.java
 * @Description
 * @createTime 2021-10-12
 */
@Mapper
public interface TRocketmqMapper extends BaseMapper<TRocketmq> {
}
