package ink.tsg.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/20 16:01
 */
@SpringBootApplication
public class KafakaApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafakaApplication.class,args);
    }

}
