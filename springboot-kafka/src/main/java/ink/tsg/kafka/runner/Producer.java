package ink.tsg.kafka.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class Producer implements ApplicationRunner {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    public static ThreadPoolExecutor executor = new ThreadPoolExecutor(20,40,5, TimeUnit.MINUTES,new ArrayBlockingQueue<>(1024), new ThreadPoolExecutor.AbortPolicy());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        while (true){
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    kafkaTemplate.send("test","测试Kafka，测试Kafka，测试Kafka，测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试K" +
                            "afka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测" +
                            "试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafka测试Kafkav");
                }
            });
        }
    }
}
