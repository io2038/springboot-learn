package ink.tsg.springboot.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HelloEntity {

    private String deptName;
    private String transferIn;
    private String transferOut;
    private String retention;
    private String testNum;
    private String defects;
    private String adverse;
    private String tsgCsId;
}
