package ink.tsg.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("ink.tsg.springboot.mapper")
public class SQLServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SQLServerApplication.class, args);
    }


}
