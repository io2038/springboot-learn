package ink.tsg.springboot.mapper;

import ink.tsg.springboot.entities.HelloEntity;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

@MapperScan
public interface HelloMapper {

    List<HelloEntity> selectOne();
}
