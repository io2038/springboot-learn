package ink.tsg.springboot.controller;

import ink.tsg.springboot.entities.HelloEntity;
import ink.tsg.springboot.interceptor.SqlLogInterceptor;
import ink.tsg.springboot.mapper.HelloMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class HelloController {

    @Autowired
    DataSource dataSource;

    @Autowired
    private HelloMapper mapper;

    @Autowired
    SqlLogInterceptor sqlLogInterceptor;

    @GetMapping("/hello")
    public String hello(){
        List<HelloEntity> entities = mapper.selectOne();
        return entities.toString();
    }



}
