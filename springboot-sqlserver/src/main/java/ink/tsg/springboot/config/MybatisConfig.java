package ink.tsg.springboot.config;

import ink.tsg.springboot.interceptor.SqlLogInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class MybatisConfig {

    @Bean
    SqlLogInterceptor sqlLogInterceptor(){
        SqlLogInterceptor sqlLogInterceptor = new SqlLogInterceptor();
        Properties properties = new Properties();
        sqlLogInterceptor.setProperties(properties);
        return sqlLogInterceptor;
    }
}
