package ink.tsg.postgresql.postgresql.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileReader;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import ink.tsg.postgresql.entity.SalEmp;
import ink.tsg.postgresql.service.ISalEmpService;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.Point;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ISalEmpServiceTest.java
 * @Description
 * @createTime 2021-04-15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public  class ISalEmpServiceTest {

    @Autowired
    private ISalEmpService salEmpService;

    @Test
    public void testGetList() throws Exception{
        List<SalEmp> list = salEmpService.list();
        SalEmp salEmp = list.get(0);
        //Integer[] payByQuarter = (Integer[]) salEmp.getPayByQuarter();
        createFile(list.get(0).getFileBinary());
    }

    private void createFile(byte[] fileBinary) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(fileBinary);
//        FileOutputStream fileOutputStream = new FileOutputStream(inputStream);
        BufferedOutputStream out = FileUtil.getOutputStream("E:\\工作文档\\address1111.json");
        IoUtil.copy(inputStream,out);



//        long copySize = IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
//        FileWriter writer = new FileWriter("E:\\工作文档\\address1111.json");

    }

    @Test
    public void testInsert() throws Exception{
        SalEmp salEmp = new SalEmp();
        salEmp.setName("aaa");
        File file = new File("E:\\工作文档\\address.json");
        //默认UTF-8编码，可以在构造中传入第二个参数做为编码
        FileReader fileReader = new FileReader("E:\\工作文档\\address.json");
        byte[] bytes = fileReader.readBytes();
        salEmp.setFileBinary(bytes);
        salEmpService.save(salEmp);
    }

    @Test
    public void testPoint2Line(){
        salEmpService.testPoint2Line();
    }



}