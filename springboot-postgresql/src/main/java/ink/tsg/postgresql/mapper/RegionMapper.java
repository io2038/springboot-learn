package ink.tsg.postgresql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.postgresql.entity.Region;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-12
 */
public interface RegionMapper extends BaseMapper<Region> {

}
