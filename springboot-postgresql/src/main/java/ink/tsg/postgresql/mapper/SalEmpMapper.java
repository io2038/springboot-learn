package ink.tsg.postgresql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.postgresql.entity.SalEmp;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
public interface SalEmpMapper extends BaseMapper<SalEmp> {

    void testPoint2Line(String toString);

    void updateInfoById(SalEmp salEmp);

}
