package ink.tsg.postgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName PostgresqlApplication.java
 * @Description
 * @createTime 2021-04-12
 */
@SpringBootApplication
public class PostgresqlApplication {
    public static void main(String[] args) {
        SpringApplication.run(PostgresqlApplication.class,args);
    }
}
