package ink.tsg.postgresql.utils;

import com.vividsolutions.jts.geom.Coordinate;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;
import net.sf.geographiclib.GeodesicLine;
import net.sf.geographiclib.GnomonicData;
import org.osgeo.proj4j.BasicCoordinateTransform;
import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.ProjCoordinate;

import java.util.Arrays;
import java.util.HashMap;

public class GeoTransform {

    public static void main(String[] args) {
        CoordinateConversion conversion = new CoordinateConversion();
        // 113.9594981,22.5790826
        String s = conversion.latLon2UTM(22.5790826, 113.9594981);
        // System.out.println(s);
        double[] doubles = conversion.utm2LatLon(s);
        //113.95949981141041，22.579025941263964
        System.out.println(Arrays.toString(doubles));

    }
}

