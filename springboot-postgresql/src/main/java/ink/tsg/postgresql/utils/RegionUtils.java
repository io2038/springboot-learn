package ink.tsg.postgresql.utils;

import com.alibaba.fastjson.JSONArray;
import ink.tsg.postgresql.entity.Region;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName RegionUtils.java
 * @Description
 * @createTime 2021-04-14
 */
public class RegionUtils {

    public static List<Region> jx() throws Exception {
        File jsonFile = new File("E:\\工作文档\\address.json");
        Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
        int ch = 0;
        StringBuilder buffer = new StringBuilder();
        while ((ch = reader.read()) != -1) {
            buffer.append((char) ch);
        }
        String json = buffer.toString();
        //String[] split = json.split(",");
        //String s = Arrays.toString(split);
        //System.out.println(Arrays.toString(split));
        List<Map> maps = JSONArray.parseArray(json, Map.class);
        Map<String, Object> map = maps.get(0);
        // System.out.println(map);
        List<Region> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String mapKey = entry.getKey();
            //System.out.println(mapKey);
            Map<String,String> value = (Map) entry.getValue();
            //System.out.println(value);
            for (Map.Entry<String, String> entryV : value.entrySet()) {
                Region region = new Region();
                region.setId(entryV.getKey());
                region.setName(entryV.getValue());
                region.setParentId(mapKey);
                list.add(region);
            }
            //System.out.println("===========================");
        }
        return list;
    }


}
