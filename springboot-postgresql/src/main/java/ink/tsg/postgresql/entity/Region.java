package ink.tsg.postgresql.entity;

import lombok.Data;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Region.java
 * @Description
 * @createTime 2021-04-14
 */
@Data
public class Region {

    private String id;
    private String parentId;
    private String name;

}
