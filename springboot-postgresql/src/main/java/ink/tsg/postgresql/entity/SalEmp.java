package ink.tsg.postgresql.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.OffsetDateTime;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sal_emp", autoResultMap = true)
public class SalEmp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "name", type = IdType.ASSIGN_ID)
    private String name;

    /**
     * pay_by_quarter
     */
    private Integer payByQuarter;

    /**
     * schedule
     */
    private String schedule;

    /**
     * the_geom
     */
    private String theGeom;

    /**
     * create_time
     */
    private OffsetDateTime createTime;

    /**
     * update_time
     */
    private LocalDateTime updateTime;

    /**
     * array_column
     */
    private Integer arrayColumn;

    /**
     * geom
     */
    private String geom;

    /**
     * file_binary
     */
    private byte[] fileBinary;
}
