package ink.tsg.postgresql.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import ink.tsg.postgresql.entity.SalEmp;
import ink.tsg.postgresql.service.ISalEmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
@RestController
@RequestMapping("/salEmp")
public class SalEmpController {

    @Autowired
    private ISalEmpService salEmpService;

    @GetMapping("getOne")
    public SalEmp getOne(){
        return salEmpService.getOne(new QueryWrapper<SalEmp>().eq("name","aaa"));
    }

    @GetMapping
    public String add(){
        SalEmp salEmp = new SalEmp();
        salEmp.setName("aaaa");
        salEmp.setTheGeom("POINT(104.091813444837 30.6609826402035)");
        salEmpService.save(salEmp);
        return "aaa";
    }
    
    /**
     * @description 事务失效的七种场景演示-方法内部调用
     * @author geo_tsg 
     * @Date 2021/11/4 10:22
     */
    @PutMapping("inside")
    public String inside(){
        return salEmpService.inside();
    }

    /**
     * @description 多线程下的事务问题
     * @author geo_tsg
     * @Date 2021/11/4 17:35
     */
    @PutMapping("threads")
    public String threads(){
        return salEmpService.threads();
    }

}
