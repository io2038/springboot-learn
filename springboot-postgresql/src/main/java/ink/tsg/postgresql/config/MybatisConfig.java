package ink.tsg.postgresql.config;

import ink.tsg.postgresql.handler.MybatisInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName MybatisPlusConfig.java
 * @Description
 * @createTime 2021-11-15
 */
@Configuration
@MapperScan("ink.tsg.postgresql.mapper")
public class MybatisConfig {

    /**
     * @Bean ： 该注解用于向容器中注入一个Bean
     * 注入Interceptor[]这个Bean
     * @return
     */
    @Bean
    public Interceptor[] interceptors(){
        //创建ParameterInterceptor这个插件
        MybatisInterceptor parameterInterceptor = new MybatisInterceptor();
        //放入数组返回
        return new Interceptor[]{parameterInterceptor};
    }

}
