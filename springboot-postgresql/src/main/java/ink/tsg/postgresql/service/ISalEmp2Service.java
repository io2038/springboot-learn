package ink.tsg.postgresql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.postgresql.entity.SalEmp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
public interface ISalEmp2Service extends IService<SalEmp> {

    /**
     * @description 多线程下的事务问题
     * @author geo_tsg 
     * @Date 2021/11/4 17:36
     */
    String add();
}
