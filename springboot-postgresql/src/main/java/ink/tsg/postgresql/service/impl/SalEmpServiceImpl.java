package ink.tsg.postgresql.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileReader;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.postgresql.entity.SalEmp;
import ink.tsg.postgresql.mapper.SalEmpMapper;
import ink.tsg.postgresql.service.ISalEmp2Service;
import ink.tsg.postgresql.service.ISalEmpService;
import ink.tsg.postgresql.utils.CoordinateConversion;
import ink.tsg.postgresql.utils.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.opengis.metadata.identification.CharacterSet;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
@Service
@Slf4j
public class SalEmpServiceImpl extends ServiceImpl<SalEmpMapper, SalEmp> implements ISalEmpService {

    @Autowired
    private  SalEmpServiceImpl salEmpService;

    @Autowired
    private ISalEmp2Service salEmpsService;


    // E:\工作文档\1129-raw_001_001.rd7.gps
    @Override
    public void testPoint2Line() {

        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder("LINESTRING(");
        CoordinateConversion conversion = new CoordinateConversion();
        try {
            reader = FileUtil.getReader(new File("E:\\工作文档\\1129-raw_001_001.rd7.gps"),"UTF-8");
            while(true) {
                String line = reader.readLine();
                if (line == null) {
                    log.error("文件读取完毕！");
                    break;
                }
                String[] split = line.split(",");
                double[] doubles = conversion.utm2LatLon("49 Q " + split[0] + " " + split[1]);

                sb.append(doubles[1]);
                sb.append(" ");
                sb.append(doubles[0]);
                sb.append(",");
            }
            sb.replace(sb.lastIndexOf(","),sb.length(),")");
            log.info(sb.toString());
            baseMapper.testPoint2Line(sb.toString());
        } catch (IOException var8) {
            throw new IORuntimeException(var8);
        } finally {
            IoUtil.close(reader);
        }
    }

    @Override
    public String inside() {
        SalEmp aaa = baseMapper.selectById("aaa");
        aaa.setTheGeom(null);
        aaa.setGeom(null);
        aaa.setFileBinary(null);
        aaa.setArrayColumn(null);
        // 方法事务失效
//        updateSal(aaa);
//        salEmpService.updateSal(aaa);
//        ((SalEmpServiceImpl) AopContext.currentProxy()).updateSal(aaa);
        SalEmpServiceImpl bean = SpringUtils.getBean(this.getClass());
        bean.updateSal(aaa);
        return "sucess";
    }

    @Transactional
    public void updateSal(SalEmp salEmp) {
        salEmp.setSchedule("inside");
        baseMapper.updateInfoById(salEmp);
        int i = 1/0;
    }

    @Override
    @Transactional
    public String threads() {
        SalEmp salEmp = baseMapper.selectById("aaa");
        salEmp.setTheGeom(null);
        salEmp.setGeom(null);
        salEmp.setFileBinary(null);
        salEmp.setArrayColumn(null);
        salEmp.setSchedule("inside");
        baseMapper.updateInfoById(salEmp);
        new Thread(() -> {
            salEmpsService.add();
        }).start();
        return "success";
    }
}
