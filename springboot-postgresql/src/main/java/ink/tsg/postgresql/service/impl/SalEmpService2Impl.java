package ink.tsg.postgresql.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.postgresql.entity.SalEmp;
import ink.tsg.postgresql.mapper.SalEmpMapper;
import ink.tsg.postgresql.service.ISalEmp2Service;
import ink.tsg.postgresql.service.ISalEmpService;
import ink.tsg.postgresql.utils.CoordinateConversion;
import ink.tsg.postgresql.utils.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
@Service
@Slf4j
public class SalEmpService2Impl extends ServiceImpl<SalEmpMapper, SalEmp> implements ISalEmp2Service {

    @Autowired
    private SalEmpService2Impl salEmpService;

    @Override
    @Transactional
    public String add() {
        SalEmp salEmp = new SalEmp();
        salEmp.setName("add");
        baseMapper.insert(salEmp);
        int i = 1/0;
        return "success";
    }
}
