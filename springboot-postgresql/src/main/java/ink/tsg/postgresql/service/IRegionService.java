package ink.tsg.postgresql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.postgresql.entity.Region;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-12
 */
public interface IRegionService extends IService<Region> {


    void test() throws Exception;
}
