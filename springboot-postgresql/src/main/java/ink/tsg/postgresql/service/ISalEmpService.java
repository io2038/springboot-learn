package ink.tsg.postgresql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.postgresql.entity.SalEmp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-15
 */
public interface ISalEmpService extends IService<SalEmp> {

    /**
     * @description 测试点转面
     * @author geo_tsg
     * @updateTime 2021/4/16 14:06
     */
    void testPoint2Line();

    String inside();

    /**
     * @description 多线程下的事务问题
     * @author geo_tsg 
     * @Date 2021/11/4 17:36
     */
    String threads();
}
