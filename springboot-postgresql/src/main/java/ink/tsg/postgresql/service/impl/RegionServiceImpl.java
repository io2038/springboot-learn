package ink.tsg.postgresql.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.postgresql.entity.Region;
import ink.tsg.postgresql.mapper.RegionMapper;
import ink.tsg.postgresql.service.IRegionService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ITER_TSG
 * @since 2021-04-12
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements IRegionService {

    @Override
    public void test() throws Exception {
        File jsonFile = new File("E:\\工作文档\\address.json");
        Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
        int ch = 0;
        StringBuilder buffer = new StringBuilder();
        while ((ch = reader.read()) != -1) {
            buffer.append((char) ch);
        }
        String json = buffer.toString();
        //String[] split = json.split(",");
        //String s = Arrays.toString(split);
        //System.out.println(Arrays.toString(split));
        List<Map> maps = JSONArray.parseArray(json, Map.class);
        Map<String, Object> map = maps.get(0);
        // System.out.println(map);
        //List<Region> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String mapKey = entry.getKey();
            //System.out.println(mapKey);
            Map<String,String> value = (Map) entry.getValue();
            //System.out.println(value);
            for (Map.Entry<String, String> entryV : value.entrySet()) {
                Region region = new Region();
                region.setId(entryV.getKey());
                region.setName(entryV.getValue());
                region.setParentId(mapKey);
                //list.add(region);
                baseMapper.insert(region);
            }
            //System.out.println("===========================");
        }
    }
}
