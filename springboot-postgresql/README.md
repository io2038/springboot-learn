# postgresql数据库

> **postgresql的索引体系，参考https://developer.aliyun.com/article/111793**

## @Transactional事务失效的景演示

> 参考：https://juejin.cn/post/7003949263281455112#heading-0
>
> 使用默认的事务处理方式
> spring的事务默认是对**RuntimeException**进行回滚，而不继承RuntimeException的不回滚。因为在java的设计中，它认为不继承RuntimeException的异常是”checkException”或普通异常，如IOException，这些异常在java语法中是要求强制处理的。对于这些普通异常，spring默认它们都已经处理，所以默认不回滚。可以添加rollbackfor=Exception.class来表示所有的Exception都回滚。

### 访问权限问题

对于定义为private的方法，即便添加了@Transactional也会失效，因为**spring不会代理非public的方法**

idea也会编译报错：

![image-20211104105053435](\img\image-20211104105053435.png)

```java
/**
	 * Same signature as {@link #getTransactionAttribute}, but doesn't cache the result.
	 * {@link #getTransactionAttribute} is effectively a caching decorator for this method.
	 * <p>As of 4.1.8, this method can be overridden.
	 * @since 4.1.8
	 * @see #getTransactionAttribute
	 */
	@Nullable
	protected TransactionAttribute computeTransactionAttribute(Method method, @Nullable Class<?> targetClass) {
		// Don't allow no-public methods as required.排除非public
		if (allowPublicMethodsOnly() && !Modifier.isPublic(method.getModifiers())) {
			return null;
		}

		// The method may be on an interface, but we need attributes from the target class.
		// If the target class is null, the method will be unchanged.
		Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);

		// First try is the method in the target class.
		TransactionAttribute txAttr = findTransactionAttribute(specificMethod);
		if (txAttr != null) {
			return txAttr;
		}

		// Second try is the transaction attribute on the target class.
		txAttr = findTransactionAttribute(specificMethod.getDeclaringClass());
		if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
			return txAttr;
		}

		if (specificMethod != method) {
			// Fallback is to look at the original method.
			txAttr = findTransactionAttribute(method);
			if (txAttr != null) {
				return txAttr;
			}
			// Last fallback is the class of the original method.
			txAttr = findTransactionAttribute(method.getDeclaringClass());
			if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
				return txAttr;
			}
		}

		return null;
	}
```

### 方法用final、static修饰

被final、static修饰的方法，由于spring动态代理，重载生成代理类，编译会报错

![image-20211104103712213](\img\image-20211104103712213.png)

### 方法内部调用

示例：

```java
 @Override
    public String inside() {
        SalEmp aaa = baseMapper.selectById("aaa");
        aaa.setTheGeom(null);
        aaa.setGeom(null);
        aaa.setFileBinary(null);
        aaa.setArrayColumn(null);
        updateSal(aaa);
        return "sucess";
    }

    @Transactional      // 回滚失效
    public void updateSal(SalEmp salEmp) {
        salEmp.setSchedule("inside");
        baseMapper.updateInfoById(salEmp);
        int i = 1/0;
    }
```

> updateSal方法拥有事务的能力是因为spring aop生成代理了对象，但是这种方法直接调用了this对象的方法，所以updateSal方法不会生成事务。

#### 解决方案：

1. 新加一个Service方法，把方法调用放到其他service中

2. 在该Service类中注入自己

   由于spring  ioc三级缓存，不会造成循环依赖

   ```java
   @Service
   @Slf4j
   public class SalEmpServiceImpl{
   
       @Autowired
       private  SalEmpServiceImpl salEmpService;
       
       @Override
       public String inside() {
           SalEmp aaa = baseMapper.selectById("aaa");
           aaa.setTheGeom(null);
           aaa.setGeom(null);
           aaa.setFileBinary(null);
           aaa.setArrayColumn(null);
           // 方法事务失效
           // updateSal(aaa);
           salEmpService.updateSal(aaa);
           return "sucess";
       }
   
       @Transactional
       public void updateSal(SalEmp salEmp) {
           salEmp.setSchedule("inside");
           baseMapper.updateInfoById(salEmp);
           int i = 1/0;
       }
   }
   ```

   

3. 通过AopContent类/SpringUtils重新获取当前类的代理类

   SpringUtils：

   ```java
   import org.springframework.beans.BeansException;
   import org.springframework.context.ApplicationContext;
   import org.springframework.context.ApplicationContextAware;
   import org.springframework.core.env.Environment;
   import org.springframework.stereotype.Component;
   @Component
   public class SpringUtils implements ApplicationContextAware {
   
       private static ApplicationContext applicationContext = null;
   
       @Override
       public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
           SpringUtils.applicationContext = applicationContext;
       }
   
       public static <T> T getBean(Class<T> cla) {
           return applicationContext.getBean(cla);
       }
   
       public static <T> T getBean(String name, Class<T> cal) {
           return applicationContext.getBean(name, cal);
       }
   
       public static Object getBean(String name){
           return applicationContext.getBean(name);
       }
   
       public static String getProperty(String key) {
           return applicationContext.getBean(Environment.class).getProperty(key);
       }
   }
   
   ```

   ```java
   	@Override
       public String inside() {
           SalEmp aaa = baseMapper.selectById("aaa");
           aaa.setTheGeom(null);
           aaa.setGeom(null);
           aaa.setFileBinary(null);
           aaa.setArrayColumn(null);
           // 方法事务失效
   //        updateSal(aaa);
   //        salEmpService.updateSal(aaa);
           SalEmpServiceImpl bean = SpringUtils.getBean(this.getClass());
           bean.updateSal(aaa);
           return "sucess";
       }
   
       @Transactional
       public void updateSal(SalEmp salEmp) {
           salEmp.setSchedule("inside");
           baseMapper.updateInfoById(salEmp);
           int i = 1/0;
       }
   ```

   

### 未加入到spring容器中

在我们平时开发过程中，有个细节很容易被忽略。即使用spring事务的前提是：对象要被spring管理，需要创建bean实例。

通常情况下，我们通过@Controller、@Service、@Component、@Repository等注解，可以自动实现bean实例化和依赖注入的功能。

### 多线程调用

两个方法不在同一个线程中，获取到的数据库连接不一样，从而是两个不同的事务。自然回滚是不会发生的。当前线程中保存了一个map，key是数据源，value是数据库连接。参考：https://www.jianshu.com/p/4b5eb29cc6d9

SalEmpServiceImpl：

```java
@Service
@Slf4j
public class SalEmpServiceImpl{
    @Autowired
    private ISalEmp2Service salEmpsService;
  	@Override
    @Transactional
    public String threads() {
        SalEmp salEmp = baseMapper.selectById("aaa");
        salEmp.setTheGeom(null);
        salEmp.setGeom(null);
        salEmp.setFileBinary(null);
        salEmp.setArrayColumn(null);
        salEmp.setSchedule("inside");
        baseMapper.updateInfoById(salEmp);
        new Thread(() -> {
            salEmpsService.add();
        }).start();
        return "success";
    }  
}

```

SalEmpService2Impl：

```
@Service
@Slf4j
public class SalEmpService2Impl{
	@Override
    @Transactional
    public String add() {
        SalEmp salEmp = new SalEmp();
        salEmp.setName("add");
        baseMapper.insert(salEmp);
        int i = 1/0;
        return "success";
    }
}
```

## mybatis插件

参考：https://mybatis.net.cn/configuration.html#plugins

原理：

