package ink.tsg.springboot.service;

import jdk.nashorn.internal.runtime.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DiscardService.java
 * @Description
 * @createTime 2021-10-11
 */
@Service
@Logger
public class DiscardService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DiscardService.class);

    public void discard (String message) {
        LOGGER.info("丢弃消息:{}", message);
    }

}
