package ink.tsg.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DiscardController.java
 * @Description
 * @createTime 2021-10-11
 */
@Controller
public class DiscardController {

    @RequestMapping("/")
    public ModelAndView index(ModelAndView model){
        model.setViewName("index");
        return model;
    }
}
