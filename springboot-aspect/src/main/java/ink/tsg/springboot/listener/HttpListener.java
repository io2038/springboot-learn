package ink.tsg.springboot.listener;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
// @WebListener
@Slf4j
public class HttpListener implements ServletRequestListener {
    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        log.info("监听器：Listener ServletRequest出生…………");
    }
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        log.info("监听器：Listener ServletRequest销毁…………");
    }
}
