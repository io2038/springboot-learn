package ink.tsg.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
//使用@ServletComponentScan注解后，Servlet、Filter、Listener可以直接通过@WebServlet、@WebFilter、@WebListener注解自动注册。
//@ServletComponentScan
public class AspectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AspectApplication.class,args);
    }
}
