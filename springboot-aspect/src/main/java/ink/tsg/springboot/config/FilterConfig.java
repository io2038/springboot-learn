package ink.tsg.springboot.config;

import ink.tsg.springboot.filter.HttpFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.Filter;

@Configuration
public class FilterConfig {

    // 注册第一个Filter
    @Bean // 标注为Spring配置bean组件
    public FilterRegistrationBean<Filter> registerFilter1() {
        //通过FilterRegistrationBean实例设置优先级可以生效
        FilterRegistrationBean<Filter> registrationBean = new  FilterRegistrationBean<>();
        // 注册自定义过滤器
        registrationBean.setFilter(new HttpFilter());

        // 设置过滤器的名字<filter-name>
        registrationBean.setName("httpFilter");
        // 设置过滤器的名字过滤路径<url-partten>
        registrationBean.addUrlPatterns("/*");

        // 设置过滤器优先级：最顶级
        registrationBean.setOrder(1);
        return registrationBean;
    }

//    @Bean //第二个filter
//    public FilterRegistrationBean<Filter> registerFilter2() {
//        FilterRegistrationBean<Filter> registrationBean = new  FilterRegistrationBean<>();
//        // 注册第二个自定义过滤器TestFilter2
//        registrationBean.setFilter(new TestFilter2());
//        registrationBean.setName("filter02");
//        registrationBean.addUrlPatterns("/*");
//        registrationBean.setOrder(5);
//        return registrationBean;
//    }
}
