package ink.tsg.springboot.config;

import ink.tsg.springboot.listener.HttpListener;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EventListener;

@Configuration
public class ListenerConfig {

    @Bean // 标注为Spring配置bean组件
    public ServletListenerRegistrationBean<EventListener> registerListener() {
        ServletListenerRegistrationBean<EventListener> listenerRegistrationBean = new ServletListenerRegistrationBean<>();
        listenerRegistrationBean.setListener(new HttpListener());
        listenerRegistrationBean.setOrder(1);
        return listenerRegistrationBean;
    }
}
