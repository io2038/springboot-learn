package ink.tsg.springboot.config;

import ink.tsg.springboot.interceptor.HttpInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class HandlerInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加一个实现HandlerInterceptor接口的拦截器实例
        registry.addInterceptor(new HttpInterceptor())
                // 用于设置拦截器的过滤路径规则
                .addPathPatterns("/**");
                // 用于设置不需要拦截的过滤规则
                //.excludePathPatterns( "/js/**", "/css/**", "/images/**");
    }
}
