package ink.tsg.springboot.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
// @WebFilter("/*")
public class HttpFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("过滤器：HttpFilter------init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws  ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        log.info("过滤器：HttpFilter--------------------do filter, {}, {}", Thread.currentThread().getId(), request.getServletPath());
        //在ThreadLocal中添加当前线程的id
//        RequestHolder.add(Thread.currentThread().getId());
       filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        log.info("过滤器：HttpFilter------destroy");
    }
}
