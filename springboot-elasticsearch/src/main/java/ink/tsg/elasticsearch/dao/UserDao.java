package ink.tsg.elasticsearch.dao;

import ink.tsg.elasticsearch.entities.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UserDao extends ElasticsearchRepository<User, Integer> {
}
