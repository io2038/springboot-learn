//package ink.tsg.elasticsearch.config;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.elasticsearch.support.VersionInfo;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import org.slf4j.MDC;
//
//import java.time.LocalDateTime;
//import java.util.Random;
//import java.util.UUID;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName LogstashLogbackAggregation.java
// * @Description
// * @createTime 2021-12-16
// */
////@EnableScheduling
//@Component
//public class LogstashLogbackAggregation {
//
//    private static final Logger LOG = LoggerFactory.getLogger(LogstashLogbackAggregation.class);
//
//    @Scheduled(cron = "0/3 * * * * ?")
//    public void logbackMdc() {
//        MDC.put("code", UUID.randomUUID().toString());
//        MDC.put("createTime", LocalDateTime.now().toString());
//        MDC.put("content", "logstash日志采集错误" + LocalDateTime.now());
//        LOG.warn("logstash日志采集，当前时间【{}】", LocalDateTime.now());
//        MDC.clear();
//    }
//}
