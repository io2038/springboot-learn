package ink.tsg.flink.leftJoin;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/17 17:22
 */
public class SensorRecordJoin {

    private SensorRecord first;
    private SensorRecord second;

    public SensorRecordJoin(SensorRecord first, SensorRecord second) {
        this.first = first;
        this.second = second;
    }

    public SensorRecordJoin() {
    }

    public SensorRecord getFirst() {
        return first;
    }

    public void setFirst(SensorRecord first) {
        this.first = first;
    }

    public SensorRecord getSecond() {
        return second;
    }

    public void setSecond(SensorRecord second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "SensorRecordJoin{" +
                "first=" + first.toString() +
                ", second=" + second.toString() +
                '}';
    }
}
