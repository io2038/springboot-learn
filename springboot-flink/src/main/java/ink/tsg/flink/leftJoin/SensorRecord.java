package ink.tsg.flink.leftJoin;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/17 17:09
 */
public class SensorRecord {

    private String id;

    private Double record;

    private LocalDateTime time;

    public Long getTimeEpochMilli() {
        return time.toInstant(ZoneOffset.of("+8")).toEpochMilli()*3000;
    }

    public SensorRecord(String id, Double record, LocalDateTime time) {
        this.id = id;
        this.record = record;
        this.time = time;
    }

    public SensorRecord() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getRecord() {
        return record;
    }

    public void setRecord(Double record) {
        this.record = record;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SensorRecord{" +
                "id='" + id + '\'' +
                ", record=" + record +
                ", time=" + time +
                '}';
    }
}
