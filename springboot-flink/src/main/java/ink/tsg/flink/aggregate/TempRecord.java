package ink.tsg.flink.aggregate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/17 9:30
 */
public class TempRecord {

    private String province;

    private String city;

    private String deviceId;

    private Double temp;

    private LocalDateTime eventTime;

    //将Long类型的时间值
    public Long getTimeEpochMilli() {
        return eventTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    public TempRecord() {
    }

    public TempRecord(String province, String city, String deviceId, Double temp, LocalDateTime eventTime) {
        this.province = province;
        this.city = city;
        this.deviceId = deviceId;
        this.temp = temp;
        this.eventTime = eventTime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }
}
