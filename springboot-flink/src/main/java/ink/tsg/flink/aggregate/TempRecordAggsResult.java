package ink.tsg.flink.aggregate;

import java.time.LocalDateTime;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/18 10:10
 */
public class TempRecordAggsResult {

    private static TempRecordAggsResult in = new TempRecordAggsResult();

    private String key;
    private Double max;
    private Double min;
    private Double sum;
    private Double counts;
    private Double avg;

    private LocalDateTime beginTime;
    private LocalDateTime endTime;


    private TempRecordAggsResult() {
    }

    public static TempRecordAggsResult getInitResult() {
        return in;
    }

    public LocalDateTime getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginTime = beginTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Double getCounts() {
        return counts;
    }

    public void setCounts(Double counts) {
        this.counts = counts;
    }

    public Double getAvg() {
        return avg;
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    @Override
    public String toString() {
        return "TempRecordAggsResult{" +
                "key='" + key + '\'' +
                ", max=" + max +
                ", min=" + min +
                ", sum=" + sum +
                ", counts=" + counts +
                ", avg=" + avg +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                '}';
    }
}
