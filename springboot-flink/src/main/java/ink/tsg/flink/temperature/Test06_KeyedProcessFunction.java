package ink.tsg.flink.temperature;

import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tsg
 * @version 1.0
 * @description: 持续增温警告案例
 * @date 2022/5/17 13:40
 */
public class Test06_KeyedProcessFunction {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //从Flink1.12开始，默认为EventTime了，所以下面一句可以省略
//        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStreamSource<SensorRecord> source = env.fromElements(
                new SensorRecord(1,20.5d,LocalDateTime.parse("2021-01-02 10:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,24.5d,LocalDateTime.parse("2021-01-02 10:00:10", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,26.5d,LocalDateTime.parse("2021-01-02 10:00:20", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,23.5d,LocalDateTime.parse("2021-01-02 10:00:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,21.5d,LocalDateTime.parse("2021-01-02 10:00:40", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,20.5d,LocalDateTime.parse("2021-01-02 10:00:50", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,25.5d,LocalDateTime.parse("2021-01-02 10:01:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))),
                new SensorRecord(1,27.5d,LocalDateTime.parse("2021-01-02 10:01:10", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
        );
//        DataStreamSource<String> source = env.socketTextStream(BaseConstant.URL, BaseConstant.PORT);

        SingleOutputStreamOperator<SensorRecord> dataStream = source
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy.<SensorRecord>forBoundedOutOfOrderness(Duration.ofSeconds(1))
                                .withTimestampAssigner(new SerializableTimestampAssigner<SensorRecord>() {
                                    @Override
                                    public long extractTimestamp(SensorRecord element, long recordTimestamp) {
                                        return element.getTimeEpochMilli();
                                    }
                                })
                );

        dataStream
                .keyBy(SensorRecord::getId)
                .process(new MyKeyedProcessFunction(3)).print();

        env.execute();
    }


}
