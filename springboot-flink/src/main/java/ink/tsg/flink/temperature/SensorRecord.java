package ink.tsg.flink.temperature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/17 16:04
 */

public class SensorRecord {

    private Integer id;

    private Double record;

    private Double lastRecord;

    private LocalDateTime time;

    //将Long类型的时间值
    public Long getTimeEpochMilli() {
        return time.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    public SensorRecord() {
    }

    public SensorRecord(Integer id, Double record, LocalDateTime time) {
        this.id = id;
        this.record = record;
        this.lastRecord = lastRecord;
        this.time = time;
    }

    public Double getLastRecord() {
        return lastRecord;
    }

    public void setLastRecord(Double lastRecord) {
        this.lastRecord = lastRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getRecord() {
        return record;
    }

    public void setRecord(Double record) {
        this.record = record;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SensorRecord{" +
                "id=" + id +
                ", record=" + record +
                ", time=" + time +
                '}';
    }
}
