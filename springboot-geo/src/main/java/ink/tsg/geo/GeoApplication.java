package ink.tsg.geo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName GeoApplication.java
 * @Description
 * @createTime 2021-07-05
 */
@SpringBootApplication
public class GeoApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeoApplication.class,args);
    }
}
