//package ink.tsg.geo.utils;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import wcontour.Contour;
//import wcontour.Interpolate;
//import wcontour.global.Border;
//import wcontour.global.PointD;
//import wcontour.global.PolyLine;
//import wcontour.global.Polygon;
//
//import java.io.*;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName KrigingUtils.java
// * @Description
// * @createTime 2021-07-05
// */
//public class KrigingUtils {
//
//    /**
//     * 通过本地文件访问json并读取
//     *
//     * @param path：json文件路径
//     * @return：json文件的内容
//     */
//    public static String readFile(String path) {
//        StringBuffer laststr = new StringBuffer();
//        File file = new File(path);// 打开文件
//        BufferedReader reader = null;
//        try {
//            FileInputStream in = new FileInputStream(file);
//            reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));// 读取文件
//            String tempString = null;
//            while ((tempString = reader.readLine()) != null) {
//                laststr = laststr.append(tempString);
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException el) {
//                }
//            }
//        }
//        return laststr.toString();
//    }
//
//    private static String getPolygonGeoJson(List<Polygon> cPolygonList) {
//        String geo = null;
//        String geometry = " { \"type\":\"Feature\",\"geometry\":";
//        String properties = ",\"properties\":{ \"hvalue\":";
//
//        String head = "{\"type\": \"FeatureCollection\"," + "\"features\": [";
//        String end = "  ] }";
//        if (cPolygonList == null || cPolygonList.size() == 0) {
//            return null;
//        }
//        try {
//            for (Polygon pPolygon : cPolygonList) {
//
//                List<Object> ptsTotal = new ArrayList<Object>();
//                List<Object> pts = new ArrayList<Object>();
//
//                PolyLine pline = pPolygon.OutLine;
//
//                for (PointD ptD : pline.PointList) {
//                    List<Double> pt = new ArrayList<Double>();
//                    pt.add(ptD.X);
//                    pt.add(ptD.Y);
//                    pts.add(pt);
//                }
//
//                ptsTotal.add(pts);
//
//                if (pPolygon.HasHoles()) {
//                    for (PolyLine cptLine : pPolygon.HoleLines) {
//                        List<Object> cpts = new ArrayList<Object>();
//                        for (PointD ccptD : cptLine.PointList) {
//                            List<Double> pt = new ArrayList<Double>();
//                            pt.add(ccptD.X);
//                            pt.add(ccptD.Y);
//                            cpts.add(pt);
//                        }
//                        if (cpts.size() > 0) {
//                            ptsTotal.add(cpts);
//                        }
//                    }
//                }
//
//                JSONObject js = new JSONObject();
//                js.put("type", "Polygon");
//                js.put("coordinates", ptsTotal);
//                double hv = pPolygon.HighValue;
//                double lv = pPolygon.LowValue;
//
//                if (hv == lv) {
//                    if (pPolygon.IsClockWise) {
//                        if (!pPolygon.IsHighCenter) {
//                            hv = hv - 0.1;
//                            lv = lv - 0.1;
//                        }
//
//                    } else {
//                        if (!pPolygon.IsHighCenter) {
//                            hv = hv - 0.1;
//                            lv = lv - 0.1;
//                        }
//                    }
//                } else {
//                    if (!pPolygon.IsClockWise) {
//                        lv = lv + 0.1;
//                    } else {
//                        if (pPolygon.IsHighCenter) {
//                            hv = hv - 0.1;
//                        }
//                    }
//
//                }
//
//                geo = geometry + js.toString() + properties + hv
//                        + ", \"lvalue\":" + lv + "} }" + "," + geo;
//
//            }
//            if (geo.contains(",")) {
//                geo = geo.substring(0, geo.lastIndexOf(","));
//            }
//
//            geo = head + geo + end;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return geo;
//        }
//        return geo;
//    }
//
//
//
//    public static void main(String[] args) {
//        //雅安样本数据
//        double[][]  trainData = JSON.parseObject(readFile("G:\\t.json"), double[][].class);
//
//        KrigingUtil kriging = new KrigingUtil(KrigingUtil.SPHERICAL_MODEL, 0, 100);
//        kriging.train(trainData[2], trainData[0], trainData[1]);
//        //经度起点
//        double left = 101.91;
//        //经度结束点
//        double right =103.395;
//        //维度起点
//        double bottom = 28.845;
//        //维度结束点
//        double top = 30.935;
//
//        double[] bottomLeft = {left, bottom};
//        double[] topLeft = {left, top};
//        double[] topRight = {right, top};
//        double[] bottomRight = {right, bottom};
//        double[][][] polygons = {{bottomLeft, topLeft, topRight, bottomRight}};
//        //计算经纬度格点距离
//        double xWidth = Math.abs(right - left) / 297;
//        double yWidth = Math.abs(top - bottom) / 418;
//        //克里金插值
//        KrigingUtil.Grid grid = kriging.grid(polygons, xWidth, yWidth);
//
//        //等值线阈值
//        double[] dataInterval = new double[]{10,20,25,30,35};
//        //异常数据样本
//        double _undefData = -9999.0;
//        double[] _X = new double[420];
//        double[] _Y = new double[298];
//
//
//        Interpolate.createGridXY_Num(101.91,28.845,103.395,30.935, _X,  _Y);
//        int[][] S1 = new int[grid.A.length][grid.A[0].length];
//        //绘线
//        List<Border> _borders = Contour.tracingBorders(grid.A, _X, _Y,S1, _undefData);
//        List<PolyLine> cPolylineList = new ArrayList<PolyLine>();
//        cPolylineList = Contour.tracingContourLines(grid.A, _X, _Y, dataInterval.length, dataInterval, _undefData, _borders, S1);// 生成等值线
//        cPolylineList = Contour.smoothLines(cPolylineList);// 平滑
//        List<Polygon> cPolygonList = new ArrayList<Polygon>();
//        cPolygonList = Contour.tracingPolygons(grid.A, cPolylineList,_borders, dataInterval);
//        String geojsonpogylon = getPolygonGeoJson(cPolygonList);
//        CommonMethod cm = new CommonMethod();
//        cm.append2File("G:\\data.json",geojsonpogylon);
//    }
//}
