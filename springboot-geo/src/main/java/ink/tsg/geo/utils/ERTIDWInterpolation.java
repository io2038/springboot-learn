package ink.tsg.geo.utils;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import org.springframework.util.ResourceUtils;
import wcontour.Interpolate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ERTIDWInterpolation.java
 * @Description ERT数据使用IDW插值
 * @createTime 2021-08-16
 */
public class ERTIDWInterpolation {

    public static void main(String[] args) throws Exception {
        String tmpFile = "D:\\workspace\\Idea\\springboot-learn\\springboot-geo\\src\\main\\java\\ink\\tsg\\geo\\data\\inversionData0.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
//        ArrayList<List> list1 = new ArrayList<>(rows.size());
        double[][] doubles = new double[rows.size()][3];
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;
        double maxZ = Double.MIN_VALUE;
        double minZ = Double.MAX_VALUE;

        //遍历行
        for (int i=1;i<rows.size();i++){
            minX = Math.min(Double.parseDouble(rows.get(i).getRawList().get(5)),minX);
            maxX = Math.max(Double.parseDouble(rows.get(i).getRawList().get(5)),maxX);
            minY = Math.min(Double.parseDouble(rows.get(i).getRawList().get(7)),minY);
            maxY = Math.max(Double.parseDouble(rows.get(i).getRawList().get(7)),maxY);
            maxZ = Math.max(Double.parseDouble(rows.get(i).getRawList().get(8)),maxZ);
            minZ = Math.min(Double.parseDouble(rows.get(i).getRawList().get(8)),minZ);
            doubles[i][0] =Double.parseDouble(rows.get(i).getRawList().get(5));
            doubles[i][1] =Double.parseDouble(rows.get(i).getRawList().get(7));
            doubles[i][2] =Double.parseDouble(rows.get(i).getRawList().get(8));

        }
        double[] X = new double[1278];
        double[] Y = new double[173];
        Interpolate.createGridXY_Num(minX, minY, maxX, maxY, X, Y);
        double[][] dataArray = Interpolate.interpolation_IDW_Radius(doubles, X, Y, 200, 500, -9999);
        System.out.println(dataArray);
    }

}
