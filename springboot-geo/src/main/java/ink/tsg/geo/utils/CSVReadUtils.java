package ink.tsg.geo.utils;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName CSVReadUtils.java
 * @Description
 * @createTime 2021-07-23
 */
public class CSVReadUtils {
    public static void main(String[] args) throws Exception {
        String tmpFile = "D:\\workspace\\Idea\\bak\\contour-master\\src\\main\\resources\\contour\\data.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
        ArrayList<List> list1 = new ArrayList<>(rows.size());
        //遍历行
        for (int i=1;i<rows.size();i+=10){
            ArrayList<Double> list = new ArrayList<>(3);
//            list.add((int)((Double.parseDouble(rows.get(i).getRawList().get(1))%113.25209)*1000000));
//            list.add((int) Math.round((Double.parseDouble(rows.get(i).getRawList().get(2))%23.08404)*1000000));
//            list.add((int) Double.parseDouble(rows.get(i).getRawList().get(0)));
            list.add(Double.parseDouble(rows.get(i).getRawList().get(1)));
            list.add(Double.parseDouble(rows.get(i).getRawList().get(2)));
            list.add(Double.parseDouble(rows.get(i).getRawList().get(0)));
            list1.add(list);
        }
    }
}
