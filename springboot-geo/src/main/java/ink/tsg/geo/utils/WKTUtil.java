package ink.tsg.geo.utils;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName WKTUtil.java
 * @Description
 * @createTime 2021-07-29
 */
public class WKTUtil {

    public static String geomToWkt(Geometry geometry) {
        String wkt = null;
        WKTWriter writer = new WKTWriter();
        wkt = writer.write(geometry);
        return wkt;
    }

    public static Geometry wktToGeom(String wkt) throws ParseException {
        Geometry geometry = null;
        WKTReader reader = new WKTReader();
        geometry = reader.read(wkt);
        //geometry.setSRID(4326);
        return geometry;
    }

}
