package ink.tsg.geo.utils;

import ink.tsg.geo.domain.DrawImage;
import wcontour.Interpolate;

import java.awt.image.BufferedImage;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName WContourUtils.java
 * @Description
 * @createTime 2021-07-23
 */
public class WContourUtils {

    // 使用插值法插值后的网格数据
    double[][] _gridData = null;

    double[] _X = null;
    double[] _Y = null;

    double _undefData = -9999.0;

    // 离散 0 是经度， 1 是 纬度， 2 是 值
    // 根据输入的 n ,创建离散 [n][3]
    private double[][] _discreteData = {
            { 3 ,6 ,15 },
            { 8 ,8 ,25 },
            { 13 ,1 ,5 },
            { 5 ,15 ,10 }
    };
    public static void main(String[] args) {
        WContourUtils t = new WContourUtils();

        // 插值生成网格
        int raws = 14,cols=10;
        t.interpolateData(raws,cols);

        BufferedImage image = new BufferedImage(raws,cols,BufferedImage.TYPE_INT_RGB);
        DrawImage d = new DrawImage(image);


        // 将 double 数组转化为 int 数组，像素无法直接用 double[][] 绘制
        int[][] data = new int[t._gridData.length][t._gridData[0].length];
        for (int i = 0; i < t._gridData.length; i++) {
            for (int j = 0; j < t._gridData[i].length; j++) {
                data[i][j] = (int) t._gridData[i][j];
            }
        }
        // 画图 生成 image
        d.draw(data);

        // 将 image 输出到文件
        d.write("d:\\test.png");
    }
    /**
     * 插值数据
     *
     * @param rows 行
     * @param cols 列
     *             <p>
     *             根据行列计算 _X[] 和 _Y[] 坐标数组
     *             以及插值后的网格数据 _gridData
     *             直接赋值给 成员变量 _X,_Y,_gridData
     */
    public void interpolateData(int rows, int cols) {
        double[][] dataArray = null;
        //double XDelt = 0;
        //double YDelt = 0;

        //---- Generate Grid Coordinate
        //double minX = bounds[0];
        //double minY = bounds[1];
        //double maxX = bounds[2];
        //;
        //double maxY = bounds[3];

        double minX = 3;
        double minY = 1;
        double maxX = 13;
        ;
        double maxY = 15;


        ;

        //maxX = this.getWidth();
        //maxY = this.getHeight();
        //XDelt = this.drawingPanel1.getWidth() / cols;
        //YDelt = this.drawingPanel1.getHeight() / rows;

        _X = new double[cols];
        _Y = new double[rows];
        Interpolate.createGridXY_Num(minX, minY, maxX, maxY, _X, _Y);

        dataArray = new double[rows][cols];
        //dataArray = Interpolate.Interpolation_IDW_Neighbor(_discreteData, _X, _Y, 8, _undefData);
        dataArray = Interpolate.interpolation_IDW_Radius(_discreteData, _X, _Y, 2, 10, _undefData);

        _gridData = dataArray;
        System.out.println(_gridData);
    }
}
