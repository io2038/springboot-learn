package ink.tsg.geo.utils;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import ink.tsg.geo.domain.DrawImage;
import org.springframework.util.ResourceUtils;
import wcontour.Interpolate;

import javax.lang.model.type.ErrorType;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName RenderERTData.java
 * @Description
 * @createTime 2021-07-23
 */
public class RenderERTData {


    public static void main(String[] args) throws Exception {
//        ertRender();
        emRender();
    }

    private static void emRender() throws Exception {
        String tmpFile = "D:\\workspace\\Idea\\bak\\contour-master\\src\\main\\resources\\contour\\data.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
        double[][] doubles = new double[rows.size()][3];
        //遍历行
        double xmin = Double.MAX_VALUE;
        double xmax = Double.MIN_VALUE;
        double ymin = Double.MAX_VALUE;
        double ymax = Double.MIN_VALUE;
        for (int i=1;i<rows.size();i++){
            double[] doubles1 = new double[3];
            double xnum = Double.parseDouble(rows.get(i).getRawList().get(1));
            doubles1[0] = xnum;
            xmin = Math.min(xmin,xnum);
            xmax = Math.max(xmax,xnum);
            double depth = Double.parseDouble(rows.get(i).getRawList().get(2));
            doubles1[1] = depth;
            ymin = Math.min(ymin,depth);
            ymax = Math.max(ymax,depth);
            doubles1[2] = Double.parseDouble(rows.get(i).getRawList().get(0));
            doubles[i] = doubles1;
        }
        double[] _X = new double[(int) ((xmax-xmin)*10000000)];
        double[] _Y = new double[(int) ((ymax-ymin)*10000000)];
        Interpolate.createGridXY_Num(xmin, ymin, xmax, ymax, _X, _Y);
        double[][] _gridData  = Interpolate.interpolation_IDW_Radius(doubles, _X, _Y, 100,500,-9999);
        // y：列----->是宽度，x：行----->是高度
        BufferedImage image = new BufferedImage(_Y.length,_X.length,BufferedImage.TYPE_INT_RGB);
        DrawImage d = new DrawImage(image);
        // 将 double 数组转化为 int 数组，像素无法直接用 double[][] 绘制
        int[][] rdata = new int[_gridData.length][_gridData[0].length];
        for (int i = 0; i < _gridData.length; i++) {
            for (int j = 0; j < _gridData[i].length; j++) {
                rdata[i][j] = (int) _gridData[i][j];
            }
        }
        // 画图 生成 image
        d.draw(rdata);
        // 将 image 输出到文件
        d.write("d:\\test3.png");
    }

    private static void ertRender() {
        String result2 = HttpRequest.get("localhost:9090/ert/dat/listPage?ertId=8337b172e4a94485b5a48713569a3954&pageNum=1&pageSize=99999")
                .header("GeomativeAuthorization", "Geomative eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6ImU1ZjQxNmEyLTQ3NDAtNDhjNi1hODllLWZhM2Q4ZDRhMTdjZSJ9.NTRV5FItSFsRS5o2biV5ZDoOyH1wvgnmR05bbGzI3pbB6G9it5sePp6XcpI0-mLH37dGYWGgbDJB-EfpKKyRlg")//头信息，多个头信息多次调用此方法即可
//                .form(paramMap)//表单内容
                .execute().body();
        Map map = JSONObject.parseObject(result2, Map.class);
        Map data = (Map) map.get("data");
        List<Map> records = (List) data.get("records");
        double[][] doubles = new double[records.size()][3];

        double xmin = Double.MAX_VALUE;
        double xmax = Double.MIN_VALUE;
        double ymin = Double.MAX_VALUE;
        double ymax = Double.MIN_VALUE;

        for (int i = 0; i < records.size(); i++) {
            double[] doubles1 = new double[3];
            double xnum = Double.parseDouble(String.valueOf((BigDecimal) records.get(i).get("depth")));
            doubles1[0] = xnum;
            xmin = Math.min(xmin,xnum);
            xmax = Math.max(xmax,xnum);
            double depth = Double.parseDouble(String.valueOf((BigDecimal) records.get(i).get("xnum")));
            doubles1[1] = depth;
            ymin = Math.min(ymin,depth);
            ymax = Math.max(ymax,depth);
            doubles1[2] = Double.parseDouble(String.valueOf(records.get(i).get("ro")));
            doubles[i] = doubles1;
        }
        double[] _X = new double[(int) (xmax-xmin)];
        double[] _Y = new double[(int) (ymax-ymin)];
        Interpolate.createGridXY_Num(xmin, ymin, xmax, ymax, _X, _Y);
        double[][] _gridData  = Interpolate.interpolation_IDW_Radius(doubles, _X, _Y, 2, 4, -9999.0);
//        double[][] _gridData = Interpolate.interpolation_IDW_Neighbor(doubles,_X,_Y,1);
        // y：列----->是宽度，x：行----->是高度
        BufferedImage image = new BufferedImage(_Y.length,_X.length,BufferedImage.TYPE_INT_RGB);
        DrawImage d = new DrawImage(image);


        // 将 double 数组转化为 int 数组，像素无法直接用 double[][] 绘制
        int[][] rdata = new int[_gridData.length][_gridData[0].length];
        for (int i = 0; i < _gridData.length; i++) {
            for (int j = 0; j < _gridData[i].length; j++) {
                rdata[i][j] = (int) _gridData[i][j];
            }
        }
        // 画图 生成 image
        d.draw(rdata);

        // 将 image 输出到文件
        d.write("d:\\test1.png");

    }
}
