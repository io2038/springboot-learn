package ink.tsg.geo.domain;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *  Java zo
 * 输入值：
 *     x y value
 *     3 6 15
 *     8 8 25
 *     13 1 5
 *     5 15 10
 *     11 3 20
 *
 * 要求输出：
 *     20*20像素的png图片
 *
 * 生成 png 的颜色设置：
 *     value   rgb
 *     0-10   （0,0,0）
 *     10-20  （155,0,0）
 *     >20    （255,0,0）

 * 差值成20*20网格   根据差值结果写入png
 *
 * */
public class DrawImage {

    // 参数宽高，图片类型
    private final BufferedImage image;
    //获取画笔
    private final Graphics g;


    public DrawImage(BufferedImage image) {
        this.image = image;
        g = image.getGraphics();
    }


    public static void main(String[] args) {
        BufferedImage image = new BufferedImage(20,20,BufferedImage.TYPE_INT_RGB);
        DrawImage d = new DrawImage(image);
        int[][] _discreteData = {
                { 3 ,6 ,15 },
                { 8 ,8 ,25 },
                { 13 ,1 ,5 },
                { 5 ,15 ,10 },
                { 11 ,3 ,20 },
        };

        d.draw(_discreteData);
        d.write("d:\\test.png");
    }


    /**
     * 画出网格数据
     * */
    public   void draw(int[][] arr) {

        Color colorLevel1 = Color.white;
        Color colorLevel2 = Color.magenta;
        Color colorLevel3 = Color.red;

        int len = 1;
        // x,y 表示要绘制的坐标
        int x,y ;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                int value = arr[i][j];
                if (value<50){
                    g.setColor(colorLevel1);
                }else if (50<value &&value<60){
                    g.setColor(colorLevel2);
                }else if (60<value &&value<70){
                    g.setColor(Color.orange);
                }else if (70<value &&value<80){
                    g.setColor(Color.CYAN);
                }else if (80<value &&value<100){
                    g.setColor(Color.GREEN);
                }else {
                    g.setColor(colorLevel3);
                }
                x = i;
                y= j;
                g.fillRect(x,y,len,len);
            }
        }
    }
    /**
     * 画出离散点
     * */
    public   void drawDiscreteData(int[][] arr) {

        Color colorLevel1 = new Color(55, 0, 0);
        Color colorLevel2 = new Color(155, 0, 0);
        Color colorLevel3 = new Color(255, 0, 0);
        Color colorLevel0 = Color.red;

        int len = 5;
        for (int[] raws : arr) {

            int value = raws[2];
            System.out.println("value = " + value);
            if (0<value && value<10){
                g.setColor(colorLevel1);
            }else if (value<20){
                g.setColor(colorLevel2);
            }else {
                g.setColor(colorLevel3);
            }
            g.fillRect(raws[0],raws[1],len,len);
        }
    }

    /**
     * 根据文件图片将 image 写入文件
     * @param filePath
     */
    public void write(String filePath){
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(filePath);
            //参数一：图片对象
            //参数二：图片的格式，如PNG,JPG,GIF
            //参数三：outputStream
            ImageIO.write(image,"PNG",fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(null != fos){
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
