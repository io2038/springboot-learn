package ink.tsg.geo.domain;

import lombok.Data;

import java.util.Date;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ShapeModel.java
 * @Description
 * @createTime 2021-07-29
 */
@Data
public class ShapeModel {
    private String layer;

    private String projection;

    private String region;

    private String regionCode;

    private Integer level;

    private String time;

    private Date createTime;

    private String clazz;

    private String geoStr;
}
