package ink.tsg.geo.controller;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import ink.tsg.geo.domain.DrawImage;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wcontour.Interpolate;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DemoController.java
 * @Description
 * @createTime 2021-08-16
 */
@RestController
@RequestMapping
public class DemoController {

    @GetMapping("/hello")
    public double[][] hello() throws FileNotFoundException {
        long start = System.currentTimeMillis();
        String tmpFile = "D:\\workspace\\Idea\\springboot-learn\\springboot-geo\\src\\main\\java\\ink\\tsg\\geo\\data\\inversionData0.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
//        ArrayList<List> list1 = new ArrayList<>(rows.size());
        double[][] doubles = new double[rows.size()][3];
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;
        double maxZ = Double.MIN_VALUE;
        double minZ = Double.MAX_VALUE;

        //遍历行
        for (int i = 1; i < rows.size(); i++) {
            minX = Math.min(Double.parseDouble(rows.get(i).getRawList().get(5)), minX);
            maxX = Math.max(Double.parseDouble(rows.get(i).getRawList().get(5)), maxX);
            minY = Math.min(Double.parseDouble(rows.get(i).getRawList().get(7)), minY);
            maxY = Math.max(Double.parseDouble(rows.get(i).getRawList().get(7)), maxY);
            maxZ = Math.max(Double.parseDouble(rows.get(i).getRawList().get(8)), maxZ);
            minZ = Math.min(Double.parseDouble(rows.get(i).getRawList().get(8)), minZ);
            doubles[i][0] = Double.parseDouble(rows.get(i).getRawList().get(5));
            doubles[i][1] = Double.parseDouble(rows.get(i).getRawList().get(7));
            doubles[i][2] = Double.parseDouble(rows.get(i).getRawList().get(8));
        }
        double[] X = new double[1278];
        double[] Y = new double[173];
        Interpolate.createGridXY_Num(minX, minY, maxX, maxY, X, Y);
        double[][] dataArray = Interpolate.interpolation_IDW_Radius(doubles, X, Y, 5, 50, -9999);
//        BufferedImage image = new BufferedImage(173, 1278, BufferedImage.TYPE_INT_RGB);
//        DrawImage d = new DrawImage(image);
        // 将 double 数组转化为 int 数组，像素无法直接用 double[][] 绘制
//        int[][] datas = new int[dataArray.length][dataArray[0].length];
        double minV = Double.MAX_VALUE;
        double maxV = Double.MIN_VALUE;
        for (int i = 0; i < dataArray.length; i++) {
            for (int j = 0; j < dataArray[i].length; j++) {
//                datas[i][j] = (int) dataArray[i][j];
                minV = Math.min(dataArray[i][j], minV);
                maxV = Math.max(dataArray[i][j], maxV);
            }
        }
        // 画图 生成 image
//        d.draw(datas);
        // 将 image 输出到文件
//        d.write("d:\\ert.png");
//        System.out.println("over");
        long end = System.currentTimeMillis();
        System.out.println("插值用时：" + (end - start));
        return dataArray;
    }
}
