apache的空间包：https://commons.apache.org/proper/commons-geometry/

经纬度等值面：http://meteothink.org/docs/wcontour/index.html

geotools工具：http://docs.geotools.org/

postgis求点到直线的距离：

参考：https://www.cnblogs.com/htoooth/p/7073904.html

```sql
-- 查询雨点最近的距离的线
SELECT
collect_id,local_name,drainage_type,material,ST_AsText(the_geom),
	ST_distance ( the_geom :: geography, ( SELECT the_geom FROM pzb_abnormal WHERE abnormal_id = '1433006871273586690' ) :: geography ) AS distance 
FROM
	pz_collect_two 
WHERE
	project_id = '1432243196359884801' 
ORDER BY
	distance 
	LIMIT 10
```
构造平行线，参考：https://postgis.net/docs/manual-3.0/ST_OffsetCurve.html


```sql
SELECT ST_AsText(ST_OffsetCurve((SELECT the_geom from pz_collect_two where collect_id = 'ysg00136067'),0.00001, 'quad_segs=0 join=round'));
```

