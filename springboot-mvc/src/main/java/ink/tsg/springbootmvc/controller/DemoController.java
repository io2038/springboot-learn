package ink.tsg.springbootmvc.controller;

import ink.tsg.springbootmvc.domian.User;
import ink.tsg.springbootmvc.service.DemoService;
import ink.tsg.springbootmvc.service.First;
import ink.tsg.springbootmvc.utils.IpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DemoController.java
 * @Description
 * @createTime 2021-04-23
 */
@RestController
public class DemoController {

    @Autowired
    private DemoService demoService;

//    @Resource
//    private CustomUtils demoUtil;

    @PostMapping("demo")
    public void demo(@RequestBody Map map){
        System.out.println(map.toString());
    }


    @GetMapping("hello")
    public String hello(HttpServletRequest request){
//        System.out.println(demoUtil.getSomeInfo());
        String ipAddr = IpUtils.getIpAddr(request);
        return ipAddr;
    }

    /**
     *  嵌套对象的校验可以加@Valid，如果对象为空则不会校验，对象不为空，会对嵌套的对象内的字段进行校验
     * @param user
     * @return
     */
    @PostMapping("validation")
    public String validation(@RequestBody User user){
        demoService.validation(user);
        return "success";
    }

    /**
     * first可以为空,其他的都不可以为空
     * @param user
     * @return
     */
    @PostMapping("validationGroup")
    public String validationGroup(@RequestBody @Validated User user){
        System.out.println(user);
        return "success";
    }

    /**
     * first不可以为空，其他的都可以为空
     * @param user
     * @return
     */
    @PostMapping("validationGroupFirst")
    public String validationGroupFirst(@RequestBody @Validated({First.class}) User user){
        System.out.println(user);
        return "success";
    }
}
