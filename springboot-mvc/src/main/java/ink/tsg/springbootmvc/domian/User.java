package ink.tsg.springbootmvc.domian;



import ink.tsg.springbootmvc.service.First;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class User {

    @NotNull(message = "name不能为空")
    private String name;

    @NotNull(message = "first不能为空",groups = {First.class})
    private String first;

    @Valid
    private UserInfo userInfoList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserInfo getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(UserInfo userInfoList) {
        this.userInfoList = userInfoList;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", first='" + first + '\'' +
                ", userInfoList=" + userInfoList +
                '}';
    }
}
