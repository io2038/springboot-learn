package ink.tsg.springbootmvc.domian;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserInfo {


    @NotBlank(message = "cloth不能为空")
    private String cloth;

    private String color;

    public String getCloth() {
        return cloth;
    }

    public void setCloth(String cloth) {
        this.cloth = cloth;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "cloth='" + cloth + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
