package ink.tsg.springbootmvc.service;

import ink.tsg.springbootmvc.domian.User;
import org.springframework.validation.annotation.Validated;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/10 10:05
 */
public interface DemoService {


    void validation(User user);

}
