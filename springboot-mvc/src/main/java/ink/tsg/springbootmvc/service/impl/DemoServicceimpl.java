package ink.tsg.springbootmvc.service.impl;

import ink.tsg.springbootmvc.domian.User;
import ink.tsg.springbootmvc.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/10 10:07
 */
@Service
@Slf4j
public class DemoServicceimpl implements DemoService {

    /**
     * 这里的Validated不会起作用
     * @Author tsg
     * @Date: 2022/5/10 10:31
    */
    @Override
    public void validation(@Validated User user) {
        log.info("数据：{}",user.toString());
    }
}
