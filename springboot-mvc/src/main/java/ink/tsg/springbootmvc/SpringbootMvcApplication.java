package ink.tsg.springbootmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SpringbootMvcApplication.java
 * @Description
 * @createTime 2021-04-23
 */
@SpringBootApplication
@ServletComponentScan("ink.tsg.springbootmvc.filter")
public class SpringbootMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMvcApplication.class,args);
    }

}
