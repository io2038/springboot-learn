package ink.tsg.springbootmvc.filter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 * 
 * @author geomative
 */
@RestControllerAdvice
public class GlobalExceptionHandler
{
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);



    @ExceptionHandler(Exception.class)
    public String handleException(Exception e)
    {
        log.error(e.getMessage(), e);
        return e.getMessage();
    }

    /**
     * 演示模式异常
     */
//    @ExceptionHandler(DemoModeException.class)
//    public AjaxResult demoModeException(DemoModeException e)
//    {
//        return AjaxResult.error("演示模式，不允许操作");
//    }
}
