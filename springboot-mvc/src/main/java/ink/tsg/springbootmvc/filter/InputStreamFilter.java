package ink.tsg.springbootmvc.filter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonParser;
import ink.tsg.springbootmvc.wapper.RepeatedlyRequestWrapper;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName InputStreamFilter.java
 * @Description
 * @createTime 2021-04-23
 */
@Slf4j
@WebFilter(filterName = "inputStreamFilter", urlPatterns = "/demo")
public class InputStreamFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        ServletRequest requestWrapper = new RepeatedlyRequestWrapper(httpServletRequest);

        // 获取输入流处理
        try {
            // 获取备份输入流做其他处理，入打印日志、分析校验前参数等
            log.info("获取到的输入流：{}", requestWrapper.getInputStream());
        } catch (Exception e) {
            log.error("异常：", e);
        }
        filterChain.doFilter(requestWrapper, servletResponse);
    }


}
