package ink.tsg.config;

import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SaTokenConfigure.java
 * @Description
 * @createTime 2021-08-09
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册路由拦截器，自定义验证规则
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler)->{
            // 根据路由划分模块，不同模块不同鉴权
            SaRouter.match("/user/**", () -> StpUtil.checkPermission("user"));
            SaRouter.match("/admin/**", () -> StpUtil.checkPermission("admin"));
            SaRouter.match("/goods/**", () -> StpUtil.checkPermission("goods"));
            SaRouter.match("/orders/**", () -> StpUtil.checkPermission("orders"));
            SaRouter.match("/notice/**", () -> StpUtil.checkPermission("notice"));
            SaRouter.match("/comment/**", () -> StpUtil.checkPermission("comment"));
        })).addPathPatterns("/**").excludePathPatterns("/login");
    }
}
