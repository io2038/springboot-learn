package ink.tsg.controller;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName LoginController.java
 * @Description
 * @createTime 2021-08-09
 */
@RestController
public class LoginController {

    @Autowired
    private JavaMailSender mailSender;


    @PostMapping("login")
    public String login(@RequestBody Map<String,Object> map){
        StpUtil.login(10001);
        System.out.println(StpUtil.getTokenValue());
        return "success";
    }
    // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
    @RequestMapping("isLogin")
    public String isLogin() {
        return "当前会话是否登录：" + StpUtil.isLogin();
    }


    // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
    @RequestMapping("sendMail")
    public String sendMail() {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, false);
            helper.setFrom("shenggeng.tang@geomative.com");
            helper.setTo("iter.tsg@qq.com");
            helper.setSubject("测试");
            helper.setText("测试");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        mailSender.send(message);
        return "success";
    }
}
