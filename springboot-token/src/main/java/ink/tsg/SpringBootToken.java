package ink.tsg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SpringBootToken.java
 * @Description
 * @createTime 2021-08-09
 */
@SpringBootApplication
public class SpringBootToken {


    public static void main(String[] args) {
        SpringApplication.run(SpringBootToken.class,args);
    }
}
