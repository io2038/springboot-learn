package ink.tsg.protocol.controller;

import ink.tsg.ftp.FtpClientTemplate;
import ink.tsg.ftp.FtpProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName FTPController.java
 * @Description
 * @createTime 2021-10-30
 */
@RestController
public class FTPController {

    @Autowired
    private FtpClientTemplate ftpClientTemplate;

    @GetMapping
    public String ftp() throws Exception {
//        FTPClient ftp = ftpProperties.getFtp();
//        System.out.println(ftp.isConnected());
//        InputStream inputStream = ftpClientTemplate.downloadFile("1435054684120293390/abd.kml");
//        FtpProperties helloProperties = ftpClientTemplate.getHelloProperties();
//        InputStream inputStream = ftpClientTemplate.downloadFile("/share/1435054684120293390/abd.kml");
//        System.out.println(ftpClientTemplate.connected());
        ftpClientTemplate.create().downloadFile("/share/1435054684120293390","abd.kml","d:/");
        return "sss";
    }
}
