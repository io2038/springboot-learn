package ink.tsg.protocol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ProtocolApplication.java
 * @Description
 * @createTime 2021-10-30
 */
@SpringBootApplication
public class ProtocolApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProtocolApplication.class,args);
    }
}
