//package ink.tsg.protocol.properties;
//
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;
//import org.apache.commons.net.ftp.FTPReply;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName FtpProperties.java
// * @Description
// * @createTime 2021-10-30
// */
//@PropertySource("classpath:ftp.properties")
//@ConfigurationProperties(prefix  = "ftp")
//public class FtpProperties {
//
//    //ftp服务器地址
////    @Value("${.host}")
//    private String host;
//
//    //ftp服务器端口
////    @Value("${ftp.port}")
//    private int port = 21;
//
//    //ftp登录账号
////    @Value("${ftp.username}")
//    private String username;
//
//    //ftp登录密码
////    @Value("${ftp.password}")
//    private String password;
//
//    //ftp保存目录
////    @Value("${ftp.basePath}")
//    private String basePath;
//
//    public String getHost() {
//        return host;
//    }
//
//    public void setHost(String host) {
//        this.host = host;
//    }
//
//    public int getPort() {
//        return port;
//    }
//
//    public void setPort(int port) {
//        this.port = port;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getBasePath() {
//        return basePath;
//    }
//
//    public void setBasePath(String basePath) {
//        this.basePath = basePath;
//    }
//
//
//}
