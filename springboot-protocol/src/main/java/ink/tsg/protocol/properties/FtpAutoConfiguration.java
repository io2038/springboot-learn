//package ink.tsg.protocol.properties;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.io.IOException;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName FtpAutoConfiguration.java
// * @Description
// * @createTime 2021-11-02
// */
//@Configuration
//@ConditionalOnWebApplication //web应用才生效
//@EnableConfigurationProperties(FtpProperties.class)
//public class FtpAutoConfiguration {
//
//    @Autowired
//    FtpProperties ftpProperties;
//
//    @Bean
//    public FtpClientTemplate ftpClientTemplate() throws IOException {
//        FtpClientTemplate template = new FtpClientTemplate();
//        template.setHelloProperties(ftpProperties);
//        return template;
//    }
//}
