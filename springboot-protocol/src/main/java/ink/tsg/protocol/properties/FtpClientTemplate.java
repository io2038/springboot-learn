//package ink.tsg.protocol.properties;
//
//
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;
//import org.apache.commons.net.ftp.FTPReply;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.util.StringUtils;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//
///**
// * @author geo_tsg
// * @version 1.0.0
// * @ClassName FtpClientConfig.java
// * @Description
// * @createTime 2021-10-30
// */
//public class FtpClientTemplate {
//
//    private FtpProperties ftpProperties;
//
//    private FTPClient ftp;
//
//    public FtpProperties getHelloProperties() {
//        return ftpProperties;
//    }
//    public void setHelloProperties(FtpProperties ftpProperties) throws IOException {
//        this.ftpProperties = ftpProperties;
//        ftp = new FTPClient();
//        ftp.setControlEncoding("UTF-8");
//        ftp.connect(ftpProperties.getHost(),ftpProperties.getPort());
//        ftp.login(ftpProperties.getUsername(), ftpProperties.getPassword()); // 登录ftp服务器
//        ftp.setDataTimeout(6000);
//        int replyCode = ftp.getReplyCode(); // 是否成功登录服务器
////            if (FTPReply.isPositiveCompletion(ftp.sendCommand(
////                    "OPTS UTF8", "ON"))) {      // 开启服务器对UTF-8的支持，如果服务器支持就用UTF-8编码，否则就使用本地编码（GBK）.
////                LOCAL_CHARSET = "UTF-8";
////            }
//        ftp.enterLocalPassiveMode();
//        if (!FTPReply.isPositiveCompletion(replyCode)) {
//            System.out.println("connect failed...ftp服务器:" + ftpProperties.getHost() + ":" + ftpProperties.getPort());
//        }
//        ftp.setFileType(FTP.BINARY_FILE_TYPE);
//        System.out.println(ftp);
//    }
//
//    /**
//     * 断开ftp链接
//     */
//    public void disconnect() {
//        try {
//            if (ftp.isConnected()) {
//                ftp.quit();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 读取ftp文件流
//     *
//     * @param filePath ftp文件路径
//     * @return s
//     * @throws Exception
//     */
//    public InputStream downloadFile(String filePath) {
//        try {
//            return ftp.retrieveFileStream(filePath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//}
