package ink.tsg.pool.mqtt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tsg
 * @version 1.0
 * @description: MqttConfig
 * @date 2022/4/28 11:57
 */
@Configuration
@EnableConfigurationProperties(MqttClientProperties.class)
public class MqttConfig {

    private MqttClientProperties mqttClientProperties;

    @Autowired
    public void setClientProperties(MqttClientProperties mqttClientProperties) {
        this.mqttClientProperties = mqttClientProperties;
    }

    @Bean
    public MqttClientFactory getClientFactory() {
        return new MqttClientFactory(mqttClientProperties);
    }

    @Bean
    public MqttTemplate getMqttTemplate() {
        return new MqttTemplate(getClientFactory(),mqttClientProperties);
    }
}
