package ink.tsg.pool.controller;

import ink.tsg.pool.mqtt.MqttTemplate;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.buffer.Buffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping
@RestController
public class PoolController {

    @Autowired
    private MqttTemplate mqttTemplate;


    @GetMapping
    public boolean pool(){
        boolean publish = mqttTemplate.publish("20220428testtopic", Buffer.buffer("aaa"), MqttQoS.AT_MOST_ONCE, false, false);
        return publish;
    }


}
