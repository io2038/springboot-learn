package ink.tsg.pool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/4/28 10:21
 */
@SpringBootApplication
@EnableConfigurationProperties
public class SpringbootPoolApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootPoolApplication.class,args);
    }
}
