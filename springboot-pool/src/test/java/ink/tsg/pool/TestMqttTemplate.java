package ink.tsg.pool;

import ink.tsg.pool.mqtt.MqttTemplate;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.buffer.Buffer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/4/28 12:53
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootPoolApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestMqttTemplate {
    @Autowired
    private MqttTemplate mqttTemplate;

    @Test
    public void uploadFileTest() {
        boolean uploadResult = mqttTemplate.publish("20220428testtopic", Buffer.buffer("aaa"), MqttQoS.AT_MOST_ONCE,false,false);
    }


}
