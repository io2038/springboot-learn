package ink.tsg.easyexcle;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import ink.tsg.export.ExportApplication;
import ink.tsg.export.entity.EasyExcleData;
import ink.tsg.export.listener.EasyExcleDataListener;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName EasyExcleDataTest.java
 * @Description
 * @createTime 2021-06-29
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExportApplication.class)
class EasyExcleDataTest {



    /**
     * 读多个或者全部sheet,这里注意一个sheet不能读取多次，多次读取需要重新读取文件
     * <p>
     * 1. 创建excel对应的实体对象 参照{@link EasyExcleData}
     * <p>
     * 2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link EasyExcleDataListener}
     * <p>
     * 3. 直接读即可
     */
    @Test
    public void repeatedRead() throws FileNotFoundException {
        // 读取部分sheet
        String fileName = "classpath:static\\广州胶化厂RTK.xlsx";
        // 读取全部sheet
        // 这里需要注意 EasyExcleDataListener的doAfterAllAnalysed 会在每个sheet读取完毕后调用一次。然后所有sheet都会往同一个EasyExcleDataListener里面写
        EasyExcel.read(ResourceUtils.getFile(fileName), EasyExcleData.class, new EasyExcleDataListener()).doReadAll();

    }

}