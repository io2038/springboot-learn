package ink.tsg.easyexcle;

import com.alibaba.excel.EasyExcel;
import ink.tsg.export.ExportApplication;
import ink.tsg.export.entity.EasyExcleData;
import ink.tsg.export.entity.Thermod;
import ink.tsg.export.listener.EasyExcleDataListener;
import ink.tsg.export.listener.ThermodDataListener;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ThermodTest.java
 * @Description
 * @createTime 2021-07-01
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExportApplication.class)
class ThermodTest {

    @Test
    public void repeatedRead() throws FileNotFoundException {
        // 读取部分sheet
        String fileName = "classpath:static\\demo.xlsx";
        EasyExcel.read(ResourceUtils.getFile(fileName), Thermod.class, new ThermodDataListener()).doReadAll();
    }
}