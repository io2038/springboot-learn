package ink.tsg.export;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.PictureType;
import ink.tsg.export.build.WordBuild;
import ink.tsg.export.entity.Template;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName WordBuildTest.java
 * @Description
 * @createTime 2021-05-17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class WordBuildTest {

    @Test
    void testExportWord2() throws Exception {
        String tmpFile = "D:\\workspace\\Idea\\springboot-learn\\springboot-export-file\\src\\main\\resources\\static\\template.docx";
        String expFile = "D:/result.doc";
        Map<String, String> datas = new HashMap<String, String>();
        datas.put("title", "标题部份");
        datas.put("content", "这里是内容，测试使用POI导出到Word的内容！");
        datas.put("author", "知识林");
        datas.put("url", "http://www.zslin.com");
        //WordBuild.build(ResourceUtils.getFile(tmpFile), datas, expFile);
    }

    @Test
    void testExportWord() throws Exception {
        String tmpFile = "classpath:static/template.docx";
        String expFile = "D:/result.doc";
        Map<String, String> datas = new HashMap<String, String>();
        datas.put("title", "标题部份");
        datas.put("content", "这里是内容，测试使用POI导出到Word的内容！");
        datas.put("author", "知识林");
        datas.put("url", "http://www.zslin.com");
        //WordBuild.build(ResourceUtils.getFile(tmpFile), datas, expFile);
    }

    @Test
    void testExportWord03() throws Exception {
        String tmpFile = "classpath:static\\template.docx";
        String expFile = "D:\\result.docx";
//        Map<String, Object> datas = new HashMap<>();
//        datas.put("title", "标题部份");
//        datas.put("content", "这里是内容，测试使用POI导出到Word的内容！");
//        datas.put("author", "知识林");
//        datas.put("url", "http://www.zslin.com");
//
//        datas.put("img", new PictureRenderData(100,100,PictureType.PNG,new FileInputStream("E:\\工作文档\\wukong.jpg")));

        Template template = new Template();
        template.setTitle("标题部份");
        template.setAuthor("知识林");
        template.setContent("这里是内容");
        template.setUrl("http://www.zslin.com");
        template.setImg(new PictureRenderData(100,100,PictureType.PNG,new FileInputStream("E:\\工作文档\\wukong.jpg")));
        //The core API uses a minimalist design, only one line of code is required
        XWPFTemplate.compile(ResourceUtils.getFile(tmpFile)).render(template).writeToFile("out_template.docx");

    }

    @Test
    void testExportWord04() throws Exception {
        String tmpFile = "classpath:static\\template01.docx";
        Map<String, Object> datas = new HashMap<>();
        datas.put("title", "标题部份");
        datas.put("content", "这里是内容，测试使用POI导出到Word的内容！");
        datas.put("author", "知识林");
        datas.put("url", "http://www.zslin.com");

        datas.put("img", new PictureRenderData(100,100,PictureType.PNG,new FileInputStream("E:\\工作文档\\wukong.jpg")));
        //The core API uses a minimalist design, only one line of code is required
        XWPFTemplate.compile(ResourceUtils.getFile(tmpFile)).render(datas).writeToFile("out_template01.docx");

    }

}