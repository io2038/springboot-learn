package ink.tsg.export;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExportApplication.java
 * @Description
 * @createTime 2021-05-17
 */
@SpringBootApplication
public class ExportApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExportApplication.class,args);
    }
}
