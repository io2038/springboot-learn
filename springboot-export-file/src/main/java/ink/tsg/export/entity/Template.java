package ink.tsg.export.entity;

import com.deepoove.poi.data.PictureRenderData;
import lombok.Data;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Template.java
 * @Description
 * @createTime 2021-05-18
 */
@Data
public class Template {

    private String title;

    private String content;
    private String author;
    private String url;
    private PictureRenderData img;


}
