package ink.tsg.export.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ErtExcleDto.java
 * @Description
 * @createTime 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErtExcleDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("点名")
    private Double point;

    @ExcelProperty("北坐标")
    private Double north;

    @ExcelProperty("东坐标")
    private Double east;

    @ExcelProperty("高程")
    private Double elv;

}
