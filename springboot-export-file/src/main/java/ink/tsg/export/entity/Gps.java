package ink.tsg.export.entity;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Gps.java
 * @Description
 * @createTime 2021-07-28
 */
public class Gps {

    private double wgLat;
    private double wgLon;

    public Gps(double wgLon, double wgLat) {
        setWgLat(wgLat);
        setWgLon(wgLon);
    }

    public double getWgLat() {
        return wgLat;
    }

    public void setWgLat(double wgLat) {
        this.wgLat = wgLat;
    }

    public double getWgLon() {
        return wgLon;
    }

    public void setWgLon(double wgLon) {
        this.wgLon = wgLon;
    }

    @Override
    public String toString() {
        return wgLon + "," + wgLat;
    }
}
