package ink.tsg.export.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ErtTargetExcleDto.java
 * @Description
 * @createTime 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErtTargetExcleDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("X-location")
    private Double point;

    @ExcelProperty("Elevation")
    private Double elevation;

    @ExcelProperty("Resistivity")
    private Double resistivity;

    @ExcelProperty("X")
    private Double x;

    @ExcelProperty("Y")
    private Double y;

    @ExcelProperty("Ele")
    private Double ele;

    @ExcelProperty("ELE+depth")
    private Double eLEdepth;
}
