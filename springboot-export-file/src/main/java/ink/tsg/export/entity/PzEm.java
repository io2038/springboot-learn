package ink.tsg.export.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName PzEm.java
 * @Description
 * @createTime 2021-07-14
 */
@Data
public class PzEm {
    private String longitude;
    private String latitude;
    private JSONObject attribute;
}
