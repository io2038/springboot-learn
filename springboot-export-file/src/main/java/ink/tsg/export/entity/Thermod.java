package ink.tsg.export.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName Thermod.java
 * @Description
 * @createTime 2021-07-01
 */
@Data
public class Thermod {

    @ExcelProperty("经度")
    private float longitude;

    @ExcelProperty("纬度")
    private float latitude;

    @ExcelProperty("高程")
    private float altitude;
}
