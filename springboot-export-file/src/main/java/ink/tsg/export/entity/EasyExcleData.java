package ink.tsg.export.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName EasyExcleData.java
 * @Description
 * @createTime 2021-06-29
 */
@Data
public class EasyExcleData {

    @ExcelProperty("序号")
    private String ertNo;

    @ExcelProperty("北坐标")
    private String northCoor;

    @ExcelProperty("东坐标")
    private String eastCoor;

    @ExcelProperty("纬度")
    private String latitude;

    @ExcelProperty("经度")
    private String longitude;

    @ExcelProperty("高程")
    private float altitude;

}
