package ink.tsg.export.controller;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.PictureType;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExportController.java
 * @Description
 * @createTime 2021-05-17
 */
@RestController
@RequestMapping
@CrossOrigin
public class ExportController {

    @GetMapping("/exportWord")
    public void exportWord(HttpServletResponse response) throws IOException {
        String tmpFile = "classpath:static\\template.docx";
        Map<String, Object> datas = new HashMap<>();
        datas.put("title", "标题部份");
        datas.put("content", "这里是内容，测试使用POI导出到Word的内容！");
        datas.put("author", "知识林");
        datas.put("url", "http://www.zslin.com");

        datas.put("img", new PictureRenderData(100,100, PictureType.PNG,new FileInputStream("E:\\工作文档\\wukong.jpg")));
        //The core API uses a minimalist design, only one line of code is required
        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        StringBuilder contentDispositionValue = new StringBuilder();
        contentDispositionValue.append("attachment; filename=")
                .append("out_template.docx")
                .append(";")
                .append("filename*=")
                .append("utf-8''")
                .append("out_template.docx");

        response.setHeader("Content-disposition", contentDispositionValue.toString());
        XWPFTemplate.compile(ResourceUtils.getFile(tmpFile)).render(datas).writeAndClose(outputStream);

    }

    @GetMapping("get3Ddata")
    public HashMap<Object, Object> get3Ddata(){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("code",20000);
        map.put("data",requData(true));
        return map;
    }

    @GetMapping("getdata")
    public HashMap<Object, Object> getdata(){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("code",20000);
        map.put("data",requData(false));
        return map;
    }

    public ArrayList<Object> requData(boolean flag){
        String srcPath = "C:\\Users\\Administrator\\Desktop\\fifth_json.json";
        JSON json = JSONUtil.readJSONArray(new File(srcPath), CharsetUtil.charset("UTF-8"));
        List list = json.toBean(List.class);
        double minx = Double.MAX_VALUE;
        double miny = Double.MAX_VALUE;
        double mincolor = Double.MAX_VALUE;

        for (Object o : list) {
            List item = (List) o;
            BigDecimal lonBig = (BigDecimal) item.get(0);
            minx = Math.min(lonBig.doubleValue(),minx);
            BigDecimal latBig = (BigDecimal) item.get(1);
            miny = Math.min(latBig.doubleValue(),miny);
            String colorBig = (String) item.get(3);
            mincolor = Math.min(Double.parseDouble(colorBig),miny);
        }
        System.out.println(minx);
        System.out.println(miny);
        ArrayList<Object> objects = new ArrayList<>();
        for (Object o : list) {
            ArrayList<Object> item3d = new ArrayList<>();
            List item = (List) o;
            if(flag){
                BigDecimal lonBig = (BigDecimal) item.get(0);
                item3d.add( (int)((lonBig.doubleValue()%minx)*10000));
            }
            BigDecimal latBig = (BigDecimal) item.get(1);
            item3d.add((int)((latBig.doubleValue()%miny)*10000));
            item3d.add(Double.parseDouble((String) item.get(2)));
            item3d.add((int)(Double.parseDouble((String) item.get(3))%mincolor));
            objects.add(item3d);
        }
        return objects;
    }


}
