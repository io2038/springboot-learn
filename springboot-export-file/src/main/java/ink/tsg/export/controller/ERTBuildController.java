package ink.tsg.export.controller;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import ink.tsg.export.entity.ErtExcleDto;
import ink.tsg.export.entity.ErtTargetExcleDto;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ERTBuildController.java
 * @Description
 * @createTime 2021-09-23
 */
@RestController
@RequestMapping
@CrossOrigin
public class ERTBuildController {

    //MultipartFile calcu, MultipartFile target, HttpServletResponse response
    @PostMapping("ertBuild")
    public void ertBuild(MultipartFile calcu, MultipartFile target, HttpServletResponse response) {

        ExcelWriter excelWriter = null ;
        try {
            List<ErtExcleDto> ertExcleDtos = EasyExcel.read(calcu.getInputStream()).head(ErtExcleDto.class).sheet().doReadSync();
            int newSize = (int) Math.ceil((ertExcleDtos.get(ertExcleDtos.size() - 1).getPoint() - ertExcleDtos.get(0).getPoint()) / 0.5);
            List<ErtExcleDto> newErtExcleDtos = new ArrayList<>(newSize);
            for (int i = 0; i < ertExcleDtos.size()-1; i++) {
                int j = i;
                ErtExcleDto ertExcleDtoA = ertExcleDtos.get(j);
                ErtExcleDto ertExcleDtoB = ertExcleDtos.get(++j);
                newErtExcleDtos.add(ertExcleDtoA);
                double[] point = new double[]{ertExcleDtoA.getPoint(), ertExcleDtoB.getPoint()};
                double[] north = new double[]{ertExcleDtoA.getNorth(), ertExcleDtoB.getNorth()};
                double[] east = new double[]{ertExcleDtoA.getEast(), ertExcleDtoB.getEast()};
                double[] elv = new double[]{ertExcleDtoA.getElv(), ertExcleDtoB.getElv()};
                for (double k = ertExcleDtoA.getPoint() + 0.5; k < ertExcleDtoB.getPoint(); k += 0.5) {
                    ErtExcleDto ertExcleDto = new ErtExcleDto();
                    LinearInterpolator line = new LinearInterpolator(); // 创建线性插值的对象
                    ertExcleDto.setNorth(Double.valueOf(NumberUtil.roundStr(line.interpolate(point, north).value(k),3)));
                    ertExcleDto.setEast(Double.valueOf(NumberUtil.roundStr(line.interpolate(point, east).value(k),3)));
                    ertExcleDto.setElv(Double.valueOf(NumberUtil.roundStr(line.interpolate(point, elv).value(k),3)));
                    ertExcleDto.setPoint(k);
                    newErtExcleDtos.add(ertExcleDto);
                }
//                newErtExcleDtos.add(ertExcleDtoB);
            }
            newErtExcleDtos.add(ertExcleDtos.get(ertExcleDtos.size()-1));
            List<ErtTargetExcleDto> targets = EasyExcel.read(target.getInputStream()).head(ErtTargetExcleDto.class).sheet().doReadSync();
            for (ErtExcleDto newErtExcleDto : newErtExcleDtos) {
                for (ErtTargetExcleDto ertTargetExcleDto : targets) {
                    if(ertTargetExcleDto.getPoint().equals(newErtExcleDto.getPoint())){
                        ertTargetExcleDto.setX(newErtExcleDto.getEast());
                        ertTargetExcleDto.setY(newErtExcleDto.getNorth());
                        ertTargetExcleDto.setEle(newErtExcleDto.getElv());
                    }
                }
            }
            // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("整合", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            excelWriter = EasyExcel.write(response.getOutputStream()).build();
            WriteSheet allSheet = EasyExcel.writerSheet("all" ).head(ErtExcleDto.class).build();
            WriteSheet writeSheet = EasyExcel.writerSheet("ert111_topres" ).head(ErtTargetExcleDto.class).build();
            excelWriter.write(newErtExcleDtos,allSheet);
            excelWriter.write(targets,writeSheet);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 千万别忘记finish 会帮忙关闭流
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
//        LinearInterpolator line = new LinearInterpolator(); // 创建线性插值的对象
//        double[] x = new double[2]; // 创建x轴两个点的数组
//        double[] y = new double[2]; // 创建y轴两个点的数组
//        // 将两个点的x,y添加到数组中
//        x[0] = 0;
//        x[1] = 2;
//        y[0] = 3532629.302;
//        y[1] = 3532631.249;
//        double value = line.interpolate(x, y).value(1);// 调用线性插值函数，并预测x2点的y值
//        System.out.println(value);
    }
}
