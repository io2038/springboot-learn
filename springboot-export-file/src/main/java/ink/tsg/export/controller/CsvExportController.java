package ink.tsg.export.controller;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import ink.tsg.export.entity.Gps;
import ink.tsg.export.utils.PositionUtil;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName CsvExportController.java
 * @Description csv处理
 * @createTime 2021-07-12
 */
@RestController
@RequestMapping
@CrossOrigin
public class CsvExportController {

    @GetMapping("csv")
    public HashMap<String, Object> csv() throws FileNotFoundException {
        String tmpFile = "E:\\工作文档\\管理规范\\课题\\广州胶化厂数据\\广州胶化厂EM\\第一区域\\第一区域综合.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
        ArrayList<List> list1 = new ArrayList<>(rows.size());
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;
        double maxZ = Double.MIN_VALUE;
        double minZ = Double.MAX_VALUE;

        //遍历行
        for (int i=1;i<rows.size();i+=10){
            ArrayList<Integer> list = new ArrayList<>(3);
            minX = Math.min(Double.parseDouble(rows.get(i).getRawList().get(0)),minX);
            maxX = Math.max(Double.parseDouble(rows.get(i).getRawList().get(0)),maxX);
            minY = Math.min(Double.parseDouble(rows.get(i).getRawList().get(1)),minY);
            maxY = Math.max(Double.parseDouble(rows.get(i).getRawList().get(1)),maxY);
            maxZ = Math.max(Double.parseDouble(rows.get(i).getRawList().get(2)),maxZ);
            minZ = Math.min(Double.parseDouble(rows.get(i).getRawList().get(2)),minZ);
            list.add((int)((Double.parseDouble(rows.get(i).getRawList().get(0))%113.2521201)*1000000));
            list.add( (int)((Double.parseDouble(rows.get(i).getRawList().get(1))%23.08408667)*1000000));
            list.add((int)Double.parseDouble(rows.get(i).getRawList().get(2)));
//            Gps gps = PositionUtil.gps84_To_Bd09(Double.parseDouble(rows.get(i).getRawList().get(0)), Double.parseDouble(rows.get(i).getRawList().get(1)));
//            list.add(gps.getWgLon());
//            list.add(gps.getWgLat());
//            list.add(Double.parseDouble(rows.get(i).getRawList().get(2)));
            list1.add(list);
        }
//        String s = JSON.toJSONString(list1.subList(0,5000));
//        String result2 = HttpRequest.post("http://localhost:5000/em/add").body(s).execute().body();
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",200);
        map.put("data",list1);
        System.out.println(map);
        return map;
    }

    @GetMapping("getErtDat")
    public Map getErtDat(){
        String result2 = HttpRequest.get("localhost:9090/ert/dat/listPage?ertId=6616beea341f44ca8f4299d53e771d53&pageNum=1&pageSize=99999")
                .header("GeomativeAuthorization", "Geomative eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6ImJmOTNlYmZkLTFhM2QtNDg4MC04N2ZkLTc4ZTkzYzkwMTAyMCJ9.SsP2gw2OHNbQDtlbUtK2JqyM2Xc79_-Uc4R1xyop57bSzuLrMxbBzRwwHYRizZPO6gnq_B4usYYVLoBFhpN6ag")//头信息，多个头信息多次调用此方法即可
//                .form(paramMap)//表单内容
                .execute().body();
        System.out.println(result2);
        Map map = JSONObject.parseObject(result2, Map.class);
        return map;
    }
    @GetMapping("getDrillDB")
    public Map getDrillDB(){
        String result2 = HttpRequest.get("localhost:9090/em/point/getEMData/4d0886e801144ff6bce283754012588c")
                .header("GeomativeAuthorization", "Geomative eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6ImYwMzNkNjg2LWMxYjUtNGE0MC1iZDIyLTI1YjVkZTk2ZWVmNiJ9.TUM7AGn0KqVrrUhn1OozUc_gK5TUkZ7Wd3pO2ceXkIDDr2yXVL7p7DiuiaJUBoxspjPTLpUWjcV4atp6-HH-Pw")//头信息，多个头信息多次调用此方法即可
//                .form(paramMap)//表单内容
                .execute().body();
        Map map = JSONObject.parseObject(result2, Map.class);

        return map;
    }
}
