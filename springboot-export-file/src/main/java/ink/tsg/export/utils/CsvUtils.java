package ink.tsg.export.utils;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import ink.tsg.export.entity.PzEm;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName CsvUtils.java
 * @Description
 * @createTime 2021-07-12
 */
public class CsvUtils {
    public static void main(String[] args) throws FileNotFoundException {
        String tmpFile = "classpath:static\\第一区域综合.csv";
        File file = ResourceUtils.getFile(tmpFile);
        CsvReader reader = CsvUtil.getReader();
        //从文件中读取CSV数据
        CsvData data = reader.read(file);
        List<CsvRow> rows = data.getRows();
        //遍历行
        for (int i=1;i<rows.size();i++){
            PzEm pzEm = new PzEm();
            pzEm.setLongitude(rows.get(i).get(0));
            pzEm.setLatitude(rows.get(i).get(1));
            HashMap<String, Object> map = new HashMap<>();
            for (int j = 2;j<rows.get(0).size();j++){
                map.put(rows.get(0).get(j),new BigDecimal(rows.get(i).get(j)).toPlainString());
            }
            JSONObject jsonObject = new JSONObject(map);
            pzEm.setAttribute(jsonObject);
            System.out.println(pzEm);
        }


    }
}
