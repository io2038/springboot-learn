package ink.tsg.export.utils;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName XMLUtils.java
 * @Description
 * @createTime 2021-11-03
 */
public class XMLUtils {

    public static void main(String[] args) throws ParserConfigurationException {
        // 创建解析器工厂
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = factory.newDocumentBuilder();
        Document document = db.newDocument();
        document.createElement("<LineString><coordinates>104.09251857627,30.668787457843901 104.092794660118,30.668610158154902</coordinates></LineString>");
        System.out.println(document);
    }
}
