package ink.tsg.export.utils;

import cn.hutool.core.io.file.FileReader;

import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TextUtil.java
 * @Description
 * @createTime 2022-04-07
 */
public class TextUtil {

    public static void main(String[] args) {
        //默认UTF-8编码，可以在构造中传入第二个参数做为编码
        FileReader fileReader = new FileReader("E:\\工作文档\\场地污染\\ert\\res2dinv相关文件数据格式说明\\附件6：4.xyz");
        List<String> list = fileReader.readLines();
        String s = list.get(1);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i)+"--------------------"+i);
        }
    }

}
