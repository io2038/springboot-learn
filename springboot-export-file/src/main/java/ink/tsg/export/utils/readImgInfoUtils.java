package ink.tsg.export.utils;

import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.File;
import java.io.IOException;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.tiff.TiffMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.sun.media.jai.codec.*;
import org.geotools.coverage.grid.GridCoverage2D;
import  org.geotools.gce.geotiff.*;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.GeneralEnvelope;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import javax.media.jai.JAI;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName readImgInfoUtils.java
 * @Description
 * @createTime 2021-07-16
 */
public class readImgInfoUtils {

    public static void main(String[] args) throws Exception {
        File imageFile = new File("E:\\工作文档\\管理规范\\测试数据\\济南裕兴化工原厂区多期遥感影像提交版\\20030703.tif");
//        File ImageFile=new File(ImageUrl);
//        GeoTiffReader reader=new GeoTiffReader(imageFile);
//        GeneralEnvelope originalEnvelope = reader.getOriginalEnvelope();
//        GridCoverage2D coverage = reader.read(null);
//        CoordinateReferenceSystem crs = coverage.getCoordinateReferenceSystem2D();
//        Envelope2D coverageEnvelope = coverage.getEnvelope2D();
//        double coverageMinX = coverageEnvelope.getBounds().getMinX();
//        double coverageMaxX = coverageEnvelope.getBounds().getMaxX();
//        double coverageMinY = coverageEnvelope.getBounds().getMinY();
//        double coverageMaxY = coverageEnvelope.getBounds().getMaxY();
//        System.out.println(coverageMinX);
//        System.out.println(coverageMaxX);
//        System.out.println(coverageMinY);
//        System.out.println(coverageMaxY);
        tifToPng(imageFile);
    }

    public static void tifToPng(File tif) throws IOException {
        if (null != tif && tif.exists()) {
            //获取tif文件不带后缀的文件名
            String fileNameIgnore = tif.getName().replaceAll("[.][^.]+$", "");
            TIFFDecodeParam param0 = null;
            TIFFEncodeParam param = new TIFFEncodeParam();
            JPEGEncodeParam param1 = new JPEGEncodeParam();
            ImageDecoder dec = ImageCodec.createImageDecoder("tiff", tif, param0);
            int count = dec.getNumPages();
            param.setCompression(TIFFEncodeParam.COMPRESSION_GROUP4);
            param.setLittleEndian(false);
            System.out.println(tif.getName() + "文件含有" + count + "张图片");
//            for (int i = 0; i < count; ) {
                RenderedImage page = dec.decodeAsRenderedImage(0);
                //转换后的png图片存储路径,这里设置为tif同级目录。可根据需要修改
                StringBuffer pngPath = new StringBuffer(tif.getParent()).append(File.separator).append(fileNameIgnore);
//                if (count > 1) {
//                    pngPath.append("(").append(++i).append(")");        //大于一张，用(1),(2)...区分
//                }
                pngPath.append(".png");                                 //png格式
                File pngFile = new File(pngPath.toString());
                System.out.println("转换后png图片路径:" + pngFile.getCanonicalPath());
                ParameterBlock pb = new ParameterBlock();
                pb.addSource(page);
                pb.add(pngFile.toString());
                pb.add("JPEG");
                pb.add(param1);
                JAI.create("filestore", pb);
//            }
        }
    }
}
