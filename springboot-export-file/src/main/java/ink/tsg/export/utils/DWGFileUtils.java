package ink.tsg.export.utils;

import com.aspose.cad.fileformats.cad.CadDrawTypeMode;
import com.aspose.cad.imageoptions.PngOptions;
import com.aspose.cad.imageoptions.SvgOptions;
import com.iver.cit.jdwglib.dwg.DwgFile;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.dwg.DWGParser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.ToXMLContentHandler;
import org.kabeja.batik.tools.SAXPNGSerializer;
import org.kabeja.dxf.DXFDocument;
import org.kabeja.parser.Parser;
import org.kabeja.parser.ParserBuilder;
import org.kabeja.svg.SVGGenerator;
import org.kabeja.xml.SAXGenerator;
import org.kabeja.xml.SAXSerializer;
import org.xml.sax.ContentHandler;
import com.aspose.cad.Image;
import com.aspose.cad.imageoptions.CadRasterizationOptions;
import com.aspose.cad.imageoptions.PdfOptions;
import java.io.*;
import java.util.HashMap;
import java.util.Vector;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DWGFileUtils.java
 * @Description 读取cad文件的工具类
 * @createTime 2021-08-18
 */
public class DWGFileUtils {

    public static void main(String[] args) throws Exception {
//        readDwgByjdwglib();
//        readDwgByTika();
//        readDxf();
        readDwgByAspose();
    }

    private static void readDwgByAspose() {

        // The path to the resource directory.
        //ExStart:RenderDWGDocument
        String srcFile = "E:\\工作文档\\管理规范\\测试数据\\cad\\CADExport20210818170258.dwg";

        Image image = Image.load(srcFile);
        // Create an instance of CadRasterizationOptions and set its various properties
        CadRasterizationOptions rasterizationOptions = new CadRasterizationOptions();
        rasterizationOptions.setPageWidth(image.getWidth()+2000);
        rasterizationOptions.setPageHeight(image.getHeight()+2000);
        rasterizationOptions.setDrawType(CadDrawTypeMode.UseObjectColor);
        // Specify desired layout name
        rasterizationOptions.setLayouts(new String[] {"Layout1"});
        // Create an instance of PdfOptions
        PdfOptions options = new PdfOptions();
//        PngOptions options = new PngOptions();
        // Set the VectorRasterizationOptions property
        options.setVectorRasterizationOptions(rasterizationOptions);

        // Export the DWG to PDF
        image.save( "E:\\工作文档\\管理规范\\测试数据\\cad\\CADExport20210818170258.pdf", options);

    }

    private static void readDxf() throws Exception {
        Parser dxfParser = ParserBuilder.createDefaultParser();
        dxfParser.parse(new FileInputStream("E:\\工作文档\\管理规范\\测试数据\\cad\\555.00-536.00.dxf"), "UTF-8");
        DXFDocument doc = dxfParser.getDocument();
        SAXGenerator generator = new SVGGenerator();
        OutputStream fileo = new FileOutputStream("E:\\工作文档\\管理规范\\测试数据\\cad\\555.00-536.00.png");//转换所得的文件
        // 输出png
        SAXSerializer out =new SAXPNGSerializer();
        // out.setOutputStream(response.getOutputStream()) //write direct to
        // ServletResponse
        out.setOutput(fileo);
        // generate
        generator.generate(doc, out, new HashMap());
    }

    /**
     * @description 使用apache的tika
     * @author geo_tsg 
     * @Date 2021/8/18 11:01
     */
    private static void readDwgByTika() throws Exception {
        DWGParser parser = new DWGParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        ParseContext parseContext = new ParseContext();
//        Reader parse = tika.parse(new File("E:\\工作文档\\管理规范\\测试数据\\555.00-536.00.dwg"));
        parser.parse(new FileInputStream("E:\\工作文档\\管理规范\\测试数据\\555.00-536.00.dwg"), handler, metadata);
        System.out.println(handler.toString());


    }

    /**
     * @description 使用jdwglib
     * @author geo_tsg 
     * @Date 2021/8/18 11:01
     */
    //读取不出来
    @Deprecated
    private static void readDwgByjdwglib() throws IOException {
        DwgFile dwgFile = new DwgFile("E:\\工作文档\\管理规范\\测试数据\\555.00-536.00.dwg");
        Vector dwgObjects = dwgFile.getDwgObjects();
        System.out.println(dwgObjects);


    }

}
