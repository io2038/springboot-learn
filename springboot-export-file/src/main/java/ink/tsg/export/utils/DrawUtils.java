package ink.tsg.export.utils;

import ink.tsg.export.entity.Thermod;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DrawUtils.java
 * @Description
 * @createTime 2021-07-01
 */
public class DrawUtils {

    private List<BufferedImage> list = new ArrayList<>();

    public DrawUtils() {
        BufferedImage left = new BufferedImage(10000, 140500,
                BufferedImage.TYPE_INT_RGB);
        this.list.add(left);
        BufferedImage right = new BufferedImage(10000, 140500,
                BufferedImage.TYPE_INT_RGB);
        this.list.add(right);
    }

    public void draw(List<Thermod> list) throws IOException {
        for (Thermod t : list) {
            if (t.getLongitude() <= 113.2530000) {
                Graphics graphics = this.list.get(0).getGraphics();
                // 添加到左边
                if (0 < t.getAltitude() && t.getAltitude() <= 5800) {
                    graphics.setColor(new Color(0, 0, 205));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 5800 && t.getAltitude() <= 8000) {
                    graphics.setColor(new Color(2, 2, 250));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 8000 && t.getAltitude() <= 20000) {
                    graphics.setColor(new Color(250, 238, 2));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 20000 && t.getAltitude() <= 60000) {
                    graphics.setColor(new Color(250, 138, 2));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else {
                    graphics.setColor(new Color(250, 2, 23));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                }
            } else {
                // 添加到右边
                Graphics graphics = this.list.get(1).getGraphics();
                if (0 < t.getAltitude() && t.getAltitude() <= 5800) {
                    graphics.setColor(new Color(0, 0, 205));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 5800 && t.getAltitude() <= 8000) {
                    graphics.setColor(new Color(2, 2, 250));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 8000 && t.getAltitude() <= 20000) {
                    graphics.setColor(new Color(250, 238, 2));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else if (t.getAltitude() > 20000 && t.getAltitude() <= 60000) {
                    graphics.setColor(new Color(250, 138, 2));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                } else {
                    graphics.setColor(new Color(250, 2, 23));//设置颜色
                    graphics.fillRect((int) (t.getLongitude() % 113.2520000), (int) (t.getLatitude() % 23.08408000), 1, 1);
                }
            }
        }
        ImageIO.write(this.list.get(0), "PNG", new FileOutputStream("D:/test/test01.png"));//把图片输出到指定路径
        ImageIO.write(this.list.get(1), "PNG", new FileOutputStream("D:/test/test02.png"));//把图片输出到指定路径
    }

}
