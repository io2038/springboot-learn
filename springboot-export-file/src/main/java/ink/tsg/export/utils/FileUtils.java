package ink.tsg.export.utils;

import cn.hutool.core.util.ZipUtil;

import java.io.File;
import java.nio.charset.Charset;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName FileUtils.java
 * @Description
 * @createTime 2021-08-12
 */
public class FileUtils {
    public static void main(String[] args) {
        unzip();

    }

    private static void unzip() {
        File unzip = ZipUtil.unzip("E:\\工作文档\\aa.zip", "E:\\工作文档\\aa", Charset.forName("gbk"));
        File[] files = unzip.listFiles();
        for (File file : files) {
            if(file.isDirectory()){
                File[] listFiles = file.listFiles();
                for (File listFile : listFiles) {
                    System.out.println(listFile.getAbsolutePath());
                }
            }else{
                System.out.println(file.getAbsoluteFile());
            }
        }

    }
}
