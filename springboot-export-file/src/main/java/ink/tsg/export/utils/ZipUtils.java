package ink.tsg.export.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.ZipUtil;
import jcifs.smb.SmbFile;
import org.opengis.metadata.identification.CharacterSet;
import org.zeroturnaround.zip.ZipEntryCallback;
import org.zeroturnaround.zip.ZipInfoCallback;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ZipUtils.java
 * @Description
 * @createTime 2021-10-27
 */
public class ZipUtils {

    public static void main(String[] args) throws Exception {
//        boolean exists = ZipUtil.containsEntry(new File("E:\\工作文档\\四川\\管线数据\\Downloads.zip"), "Downloads");
//        System.out.println(exists);
//        ZipUtil.unpack(new File("E:\\工作文档\\四川\\管线数据\\Downloads.zip"), new File("E:\\工作文档\\四川\\管线数据\\demo"));

//        ZipUtil.explode(new File("E:\\工作文档\\四川\\管线数据\\Downloads.zip"));
//        ZipUtil.iterate(new File("/tmp/demo.zip"), zipEntry -> {
//            if (zipEntry.getName().endsWith(".class"))
//                System.out.println("Found " + zipEntry.getName());
//        });

//        ZipUtil.iterate(new File("E:\\工作文档\\四川\\管线数据\\Downloads\\Downloads.zip"), (inputStream,zipEntry)->{
//            @Override
//            public void process(InputStream inputStream, ZipEntry zipEntry) throws IOException {
//                if (zipEntry.getName().endsWith(".xls")){
//                    System.out.println("Found " + zipEntry.getName());
//                }
//            }
//        },Charset.forName("GBK"));
//        URI uri1 = new URI("http://192.168.11.139:9527/share/模板.cfg");
//        URI uri2 = new URI("http://192.168.11.139:9527/share/anaconda-ks.cfg");

//        URL url1 = new URL("http://192.168.11.139:9527/share//1435054684120293390/5d58d29950294b6ea75361d69d4d7755/ZLWL001.srd");
//        URL url2 = new URL("http://192.168.11.139:9527/share/anaconda-ks.cfg");
//        int startIndex = url1.toString().lastIndexOf('/');
//        String fileName = url1.toString().substring(startIndex + 1);
//        System.out.println(fileName);
//        ZipUtil.pack(new File(uri), new File("E:\\工作文档\\四川\\管线数据\\aa\\aaa.zip"));
//        ZipUtil.zip(FileUtil.file("E:\\工作文档\\四川\\管线数据\\aa\\aaa.zip"), false,
//                FileUtil.file(url1)
//                FileUtil.file(url2)
//        );
        smb();

    }

    private static void smb() throws Exception {

        SmbFile smbFile = new SmbFile("smb://admin:登录密码@192.168.1.69/文件夹名称/");
        if(smbFile.isDirectory()){
            SmbFile[] listFiles = smbFile.listFiles();//获取所有文件
            System.out.println(listFiles.length);//文件数量
            for (SmbFile smbFile2 : listFiles) {
                System.out.println(smbFile2.getName());//文件名
                InputStream inputStream = smbFile2.getInputStream();
                System.out.println(smbFile2.getPath());//smb协议下的服务器文件路径
            }
        }
    }

}
