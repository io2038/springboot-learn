# 各类文件操作功能

## word文档导出使用的是

[http://deepoove.com/poi-tl/]: poi-tl

## excle使用的是阿里巴巴的

[https://www.yuque.com/easyexcel]: Easy-excle

> 注：暂不支持图片数据

## 操作CAD文件的解决方案

直接读取CAD文件目前没有免费的开源库支持（[jdwglib](https://sourceforge.net/projects/jdwglib/)好像没有维护了，读取失败）https://repo.fenixedu.org/webapp/browserepo.html?2

好用的收费解决方案[aspose-cad](https://www.aspose.com/)

替代解决方案参考：

1. 读取dxf文件：[kabeja](https://blog.csdn.net/weixin_43747076/article/details/105534212)
2. 使用apache的[TIKA](https://tika.apache.org/)

## 解压缩文件

使用hutool的解压缩也行

使用：https://github.com/zeroturnaround/zt-zip

> 注意编码问题

## poi-tl

> Word模板引擎，使用Word模板和数据创建很棒的Word文档。[文档](http://deepoove.com/poi-tl/)	[github](https://github.com/Sayi/poi-tl)

| 方案           | 移植性                       | 功能性                                                       | 易用性                                                       |
| :------------- | :--------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| **Poi-tl**     | Java跨平台                   | Word模板引擎                                                 | 基于Apache POI，更友好的API                                  |
| Apache POI     | Java跨平台                   | Apache项目，不仅封装了易用的文档API(文本、图片、表格、页眉、页脚、图表等)，也可以在底层直接操作XML结构 | 文档不全，这里有一个教程：[Apache POI Word快速入门](http://deepoove.com/poi-tl/apache-poi-guide.html) |
| Freemarker     | XML跨平台                    | 仅支持文本，很大的局限性                                     | 不推荐，需要维护XML结构，代码后期不可维护                    |
| OpenOffice     | 部署OpenOffice，移植性较差   | -                                                            | 需要了解OpenOffice的API                                      |
| HTML浏览器导出 | 依赖浏览器的实现，移植性较差 | HTML不能很好的兼容Word的格式                                 | -                                                            |
| Jacob、winlib  | Windows平台                  | -                                                            | 复杂，完全不推荐使用                                         |

### 关于使用spring EL表达式

标签无法被计算的场景有几种，比如模板中引用了一个不存在的变量，或者级联的前置结果不是一个哈希，如 `{{author.name}}` 中author的值为null，此时就无法计算name的值。

poi-tl可以在发生这种错误时对计算结果进行配置，默认会认为标签值为`null`。当我们需要严格校验模板是否有人为失误时，可以抛出异常：

```java
builder.useDefaultEL(true);
```

注意的是，如果使用SpringEL表达式，可以通过参数来配置是否抛出异常：

```java
builder.useSpringEL(true);
```

在模板上可以通过spring EL表达式

代码：

```java
if (StringUtils.isNotEmpty(risk1)) {
    datas.put("risk1", CollUtil.join(risk1, "、"));
}
```

```tex
{{risk1}}{{?risk1}}风险等级评价均为Ⅰ；{{/}}
```



