package ink.tsg.service.impl;

import ink.tsg.service.IHelloService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Service(version = "1")
@Component
public class IHelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String str) {
        return "provider Hello, " + str + " (from Dubbo with Spring Boot)";
    }
}
