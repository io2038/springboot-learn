package ink.tsg.utils;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SendPhoneMsg.java
 * @Description 阿里云发送短信工具类
 * @createTime 2021-06-09
 */
@Component
// @ConfigurationProperties(prefix = "aliyun")
public class SendMsgUtils {

    @Value("${aliyun.accessKeyId}")
    private String accessKeyId;

    @Value("${aliyun.secret}")
    private String secret;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * 向手机发送验证码
     *
     * @param param
     * @param phone
     */
    public boolean send2Phone(final Map<String, Object> param, final String phone) {
        try {
            DefaultProfile profile =
                    DefaultProfile.getProfile("default", accessKeyId, secret);
            IAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.setSysMethod(MethodType.POST);
            request.setSysDomain("dysmsapi.aliyuncs.com");
            request.setSysVersion("2017-05-25");
            request.setSysAction("SendSms");
            //request.putQueryParameter("RegionId", "cn-hangzhou");
            // 手机号
            request.putQueryParameter("PhoneNumbers", phone);
            // 签名
            request.putQueryParameter("SignName", "蛮吉成长记");
            // 模板code
            request.putQueryParameter("TemplateCode", "SMS_205611635");
            request.putQueryParameter("TemplateParam", JSONUtil.toJsonStr(param));
            CommonResponse response = client.getCommonResponse(request);
            return response.getHttpResponse().isSuccess();
        } catch (ClientException e) {
            e.printStackTrace();
            // throw new IterException(ResultCode.ERROR, "手机验证码发送出现异常");
        }
        return false;
    }

    public boolean send2Email(String to, String subject) {

//        Context context = new Context();
//        context.setVariable("project", "demo");
//        context.setVariable("author", "yimcarson");
//        context.setVariable("code", text);
//        String emailContent = templateEngine.process("mail", context);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText("aaa", false);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        mailSender.send(message);
        return false;
    }

}
