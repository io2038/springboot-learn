package ink.tsg.controller;

import cn.hutool.core.util.RandomUtil;
import ink.tsg.utils.SendMsgUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ApiController.java
 * @Description
 * @createTime 2021-06-09
 */
@RestController
public class ApiController {

    @Autowired
    private SendMsgUtils sendMsgUtils;

    @GetMapping("sendPhoneMsg")
    public boolean sendPhoneMsg(){
        String code = RandomUtil.randomString(4);
        Map<String,Object> param = new HashMap<>();
        param.put("code",code);
        return sendMsgUtils.send2Phone(param,"13007220660");
    }

    @GetMapping("sendEmailMsg")
    public boolean sendEmailMsg(){
        return sendMsgUtils.send2Email("1pu_8gk5ufht2z@dingtalk.com","测试主题");
    }
}
