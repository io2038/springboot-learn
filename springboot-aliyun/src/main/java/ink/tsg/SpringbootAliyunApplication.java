package ink.tsg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName SpringbootAliyunApplication.java
 * @Description
 * @createTime 2021-06-09
 */
@SpringBootApplication
public class SpringbootAliyunApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAliyunApplication.class,args);
    }
}
