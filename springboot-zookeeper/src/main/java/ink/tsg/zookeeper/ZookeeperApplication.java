package ink.tsg.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ZookeeperApplication.java
 * @Description
 * @createTime 2021-05-13
 */
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class ZookeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZookeeperApplication.class,args);
    }

}
