package ink.tsg.zookeeper.lock;

import lombok.Getter;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TestLock.java
 * @Description
 * @createTime 2022-03-28
 */
public abstract  class TestLock<String> extends AbstractZookeeperLock<String> {

    private static final java.lang.String LOCK_PATH = "test_";

    @Getter
    private String lockId;

    public TestLock(String lockId) {
        this.lockId = lockId;
    }

    @Override
    public java.lang.String getLockPath() {
        return LOCK_PATH + this.lockId;
    }
}
