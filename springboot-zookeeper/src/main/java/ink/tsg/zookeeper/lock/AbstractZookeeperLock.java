package ink.tsg.zookeeper.lock;

import java.util.concurrent.TimeUnit;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName AbstractZookeeperLock.java
 * @Description
 * @createTime 2022-03-28
 */
public abstract  class AbstractZookeeperLock <T> {
    private static final int TIME_OUT = 5;

    public abstract String getLockPath();

    public abstract T execute();

    public int getTimeout(){
        return TIME_OUT;
    }

    public TimeUnit getTimeUnit(){
        return TimeUnit.SECONDS;
    }
}