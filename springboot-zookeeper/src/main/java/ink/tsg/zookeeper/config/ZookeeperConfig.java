package ink.tsg.zookeeper.config;

import ink.tsg.zookeeper.client.ZookeeperClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ZookeeperConfig.java
 * @Description
 * @createTime 2022-03-28
 */
@Configuration
public class ZookeeperConfig {

    @Value("zookeeper.server")
    private String zookeeperServer;

    @Value("zookeeper.lockPath")
    private String zookeeperLockPath;

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public ZookeeperClient zookeeperClient(){

        return new ZookeeperClient(zookeeperServer, zookeeperLockPath);
    }
}
