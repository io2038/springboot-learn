package ink.tsg.zookeeper.utils;

import org.apache.zookeeper.KeeperException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ZkUtilTest.java
 * @Description
 * @createTime 2021-05-13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ZkUtilTest {

    @Autowired
    private ZkUtil zkUtil;
    
    /**
     * @description 创建持久化节点
     * @author geo_tsg 
     * @Date 2021/5/13 16:08
     */
    @Test
    void createPerNode() {
        zkUtil.createPerNode("/test", "auskat");
    }

    /**
     * @description
     * @author geo_tsg
     * @Date 2021/5/13 16:08
     */
    @Test
    void createTmpNode() {
        zkUtil.createTmpNode("/temp","temp");
    }

    @Test
    void updateNode() {
        zkUtil.updateNode("/demo/aa","临时");
    }

    @Test
    void deleteNode() {
        zkUtil.deleteNode("/demo");
    }

    @Test
    void exists() {
        zkUtil.exists("/demo", new CustomWatcher());
    }

    @Test
    void testExists() {

    }

    @Test
    void getChildren() throws KeeperException, InterruptedException {
        System.out.println(zkUtil.getChildren("/demo"));
    }

    @Test
    void getData() {
        System.out.println(zkUtil.getData("/demo",new CustomWatcher()));
    }
}