# ZooKeeper分布式锁

参考：https://juejin.cn/post/6854573210756972557

zookeeper可以使用apache封装的工具类来调用使用zookeeper的api：http://curator.apache.org/curator-examples/index.html

controller：

```java
@GetMapping("zookeeper")
    public String zookeeper() {
        //设置重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", retryPolicy);
        // 启动客户端
        client.start();
        InterProcessMutex mutex = new InterProcessMutex(client, "/locks");
        try {
            //加锁
            if (mutex.acquire(3, TimeUnit.SECONDS)) {
                // 业务调用
                return tbCommodityInfoService.zookeeper() ? "success" : "fail";
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        } finally {
            //解锁
            try {
                mutex.release();
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "error";
    }
```

service

```java
@Override
@Transactional
public Boolean zookeeper() {
    boolean bool = false;
    //1.先查询数据库中商品的数量
    TbCommodityInfo commodityInfo = baseMapper.selectById("1");
    //2.判断商品数量是否大于0，或者购买的数量大于库存
    Integer count = commodityInfo.getNumber();
    if (count <= 0) {
        //商品数量小于或者等于0，或者购买的数量大于库存，则返回false
        return false;
    }
    //3.如果库存数量大于0，并且购买的数量小于或者等于库存。则更新商品数量
    count--;
    commodityInfo.setNumber(count);
    bool = baseMapper.updateById(commodityInfo) == 1;
    if (bool) {
        //如果更新成功，则打印购买商品成功
        System.out.println("购买商品[ " + commodityInfo.getCommodityName() + " ]成功");
    }
    return bool;
}
```

JMeter调用:

![image-20211118161834338](\img\image-20211118161834338.png)

![image-20211118161926019](\img\image-20211118161926019.png)

