package ink.tsg.manydatasource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.manydatasource.entity.User;
import org.springframework.stereotype.Service;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/5 10:58
 */
@Service
public interface UserService extends IService<User> {

}
