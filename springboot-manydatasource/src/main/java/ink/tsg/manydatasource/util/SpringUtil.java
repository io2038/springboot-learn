package ink.tsg.manydatasource.util;

import lombok.Getter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/5 9:41
 */
@Component
public class SpringUtil implements ApplicationContextAware {

    @Getter
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringUtil.applicationContext == null) {
            SpringUtil.applicationContext = applicationContext;
        }
    }

    public static <T> T getBean(Class<T> clazz) {
        return SpringUtil.applicationContext.getBean(clazz);
    }

    public static Object getBean(String name) {
        return SpringUtil.applicationContext.getBean(name);
    }

    public static String getProperty(String key) {
        return SpringUtil.applicationContext.getEnvironment().getProperty(key);
    }
}
