package ink.tsg.manydatasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.manydatasource.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/5 10:59
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
