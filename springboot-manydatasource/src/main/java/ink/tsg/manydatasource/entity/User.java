package ink.tsg.manydatasource.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/5 10:57
 */
@Data
public class User {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * username
     */
    private String username;
}
