package ink.tsg.manydatasource.entity;

import lombok.Data;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/5 10:48
 */
@Data
public class UserDb {
    private String jdbcUrl;
    private String username;
    private String password;
    private String driverClass;



}
