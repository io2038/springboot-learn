package ink.tsg.scheduled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/19 11:54
 */
@SpringBootApplication
@EnableScheduling
public class TimerTaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(TimerTaskApplication.class);
    }
}

