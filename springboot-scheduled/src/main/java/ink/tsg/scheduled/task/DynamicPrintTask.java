package ink.tsg.scheduled.task;

import ink.tsg.scheduled.repository.SpringScheduledCronRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/19 13:36
 */
@Component
public class DynamicPrintTask implements Runnable {

    public static Boolean isRun = false;

    @Autowired
    SpringScheduledCronRepository springScheduledCronRepository;

    @Override
    public void run() {
        if (isRun) {
            return;
        }
        isRun = true;
        //通过类名获得表达式信息  判断状态是否可用 1:正常   2：停用  如果不可用直接返回
        //this.getClass().getName()  获得当前类名   com.lsh.task.DynamicPrintTask3
        Integer status = springScheduledCronRepository.findSpringScheduledCronByCronKey(this.getClass().getName()).getStatus();
        if (2 == status){
            return;
        }
        System.out.println("-----=====定时任务："+ LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        isRun = false;
    }
}
