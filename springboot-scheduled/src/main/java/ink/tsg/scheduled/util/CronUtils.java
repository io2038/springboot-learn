package ink.tsg.scheduled.util;

import org.springframework.scheduling.support.CronExpression;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author tsg
 * @version 1.0
 * @description: Cron表达式
 * @date 2022/5/19 15:12
 */
public class CronUtils {

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String cron = "0 */5 * * % ?"; //每个五分钟执行一次

        CronExpression parse = CronExpression.parse(cron);
        System.out.println(parse.next(LocalDateTime.now()));
//        System.out.println(CronSequenceGenerator.isValidExpression(cron));
//        if(CronSequenceGenerator.isValidExpression(cron)) {
//            System.out.println("表达式正确");
//            CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(cron);
//
//            Date currentTime = new Date();
//            System.out.println("currentTime: " + sdf.format(currentTime));
//
//            Date nextTimePoint = cronSequenceGenerator.next(currentTime); // currentTime为计算下次时间点的开始时间
//            System.out.println("nextTimePoint: " + sdf.format(nextTimePoint));
//
//            Date nextNextTimePoint = cronSequenceGenerator.next(nextTimePoint);
//            System.out.println("nextNextTimePoint: " + sdf.format(nextNextTimePoint));
//
//
//        }else {
//            System.out.println("表达式错误");
//        }

    }

}
