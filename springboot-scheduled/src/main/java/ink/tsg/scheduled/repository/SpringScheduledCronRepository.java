package ink.tsg.scheduled.repository;

import ink.tsg.scheduled.domain.SpringScheduledCron;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tsg
 * @version 1.0
 * @description: TODO
 * @date 2022/5/19 11:56
 */
public interface  SpringScheduledCronRepository extends JpaRepository<SpringScheduledCron, Integer> {
    //根据CronKey查找表达式

    SpringScheduledCron findSpringScheduledCronByCronKey(String CronKey);
}
