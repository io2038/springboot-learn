package ink.tsg.scheduled.domain;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author tsg
 * @version 1.0
 * @description: sal
 * CREATE TABLE `spring_scheduled_cron` (
 *   `cron_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
 *   `cron_key` varchar(128) NOT NULL COMMENT '定时任务完整类名',
 *   `cron_expression` varchar(20) NOT NULL COMMENT 'cron表达式',
 *   `task_explain` varchar(50) NOT NULL DEFAULT '' COMMENT '任务描述',
 *   `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态,1:正常;2:停用',
 *   PRIMARY KEY (`cron_id`),
 *   UNIQUE KEY `cron_key` (`cron_key`),
 *   UNIQUE KEY `cron_key_unique_idx` (`cron_key`)
 * )
 *
 * @date 2022/5/19 11:52
 */
@Entity(name = "spring_scheduled_cron")
public class SpringScheduledCron {
    @Id
    @Column(name = "cron_id")
    private Integer cronId;
    @Column(name = "cron_key", unique = true)
    private String cronKey;
    @Column(name = "cron_expression")
    private String cronExpression;
    @Column(name = "task_explain")
    private String taskExplain;
    private Integer status;


    public Integer getCronId() {
        return cronId;
    }

    public void setCronId(Integer cronId) {
        this.cronId = cronId;
    }

    public String getCronKey() {
        return cronKey;
    }

    public void setCronKey(String cronKey) {
        this.cronKey = cronKey;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getTaskExplain() {
        return taskExplain;
    }

    public void setTaskExplain(String taskExplain) {
        this.taskExplain = taskExplain;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
