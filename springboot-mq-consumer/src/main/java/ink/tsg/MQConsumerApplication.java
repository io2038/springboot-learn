package ink.tsg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName MQConsumerApplication.java
 * @Description
 * @createTime 2021-10-12
 */
@EnableJms
@SpringBootApplication
public class MQConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MQConsumerApplication.class,args);
    }
}
