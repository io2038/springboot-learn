package ink.tsg.rocketmq;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.converter.SimpleMessageConverter;
import org.springframework.stereotype.Component;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ActiveMqConsumer.java
 * @Description
 * @createTime 2022-01-20
 */
@Component
public class ActiveMqConsumer {

    @JmsListener(destination = "sms.queue")
    public void recive(String content) {
        System.out.println("收到消息："+content);
    }

    @JmsListener(destination = "activemq")
    public void mqtt(BytesMessage message, Session session){
        try {
            String msgid = message.getJMSMessageID();
            SimpleMessageConverter simpleMessageConverter = new SimpleMessageConverter();
            byte[] bytes = (byte[]) simpleMessageConverter.fromMessage(message);
            String str = StrUtil.str(bytes, CharsetUtil.UTF_8);
            System.out.println(str);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
