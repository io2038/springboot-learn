package ink.tsg.rocketmq;

//import ink.tsg.entity.User;
//import org.apache.rocketmq.spring.annotation.ConsumeMode;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.springframework.stereotype.Service;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName DemoConsumers.java
 * @Description 模拟消费
 * @createTime 2021-10-09
 */
//@Service
//@RocketMQMessageListener(topic = "transaction-topic", consumerGroup = "demo_consumer")
//public class TransactionConsumer implements RocketMQListener<User> {
//    @Override
//    public void onMessage(User user) {
//        System.out.println("Consumers1接收事务消息:" + user.toString());
//    }
//}
