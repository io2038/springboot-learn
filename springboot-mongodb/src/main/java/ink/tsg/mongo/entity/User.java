package ink.tsg.mongo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
public class User {


    @Id
    private String id;
    private String name;
    private int age;
    private Date createTime;
    private Date updateTime;


}
