package ink.tsg.mongo.service.controller;

import ink.tsg.mongo.entity.User;
import ink.tsg.mongo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/add")
    public void addUser(@RequestBody User user){
        userService.addUser(user);
    }

    @GetMapping("{id}")
    public User getById(@PathVariable String id){
        return userService.getById(id);
    }

    @DeleteMapping("{id}")
    public void delById(@PathVariable String id){
        userService.delById(id);
    }

    @DeleteMapping("{name}")
    public void delByName(@PathVariable String name){
        userService.delByName(name);
    }

    @GetMapping("/searchBy/{keyword}")
    public List<User> getByKeyword(@PathVariable String keyword){
        return userService.getByKeyword(keyword);
    }

    @PutMapping("updateUser")
    public void updateUser(@RequestBody User user){
        userService.updateUser(user);
    }

    @PostMapping("saveSmallFile")
    public void saveSmallFile(@RequestBody MultipartFile file) throws IOException {
        userService.saveSmallFile(file);
    }

    @GetMapping("download/{id}")
    public void download(@PathVariable String id, HttpServletResponse response) throws IOException {
        userService.download(id,response);
    }

    @DeleteMapping("delFileById/{id}")
    public void delFileById(@PathVariable String id){
        userService.delFileById(id);
    }

}
