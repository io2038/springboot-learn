package ink.tsg.mongo.service;

import ink.tsg.mongo.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IUserService {
    /**
     * 添加的id相同时会修改
     * @param user
     */
    void addUser(User user);

    /**
     * 根据id获取
     * @param id
     * @return
     */
    User getById(String id);

    /**
     * 根据id删除
     * @param id
     */
    void delById(String id);

    /**
     * 根据名称删除
     * @param name
     */
    void delByName(String name);

    /**
     * 模糊查询
     * @param keyword
     * @return
     */
    List<User> getByKeyword(String keyword);

    /**
     * 更新用户
     * @param user
     */
    void updateUser(User user);

    /**
     * 存储小文件。mongodb中小于 16M的直接二进制存储
     * @param user
     */
    void saveSmallFile(MultipartFile user) throws IOException;

    /**
     * 根据id下载文件
     * @param id
     * @param response
     */
    void download(String id, HttpServletResponse response) throws IOException;

    /**
     * 根据id删除文件，传入id会把fs.chunk和fs.file内的数据都清空
     * @param id
     */
    void delFileById(String id);
}
