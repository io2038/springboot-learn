package ink.tsg.springexecutor.controller;

import ink.tsg.springexecutor.service.ExecutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExecutorController.java
 * @Description
 * @createTime 2021-06-28
 */
@RestController
@Validated
public class ExecutorController {

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @GetMapping("/testGetPara/{id}")
    public String testGetPara(@Pattern(regexp = "[0-2]?") @PathVariable("id") String id) {
        return "success";
    }

    @GetMapping("testExecutor")
    public String testExecutor() {
        threadPoolTaskExecutor.execute(() -> {
            try {
                System.out.println(Thread.currentThread().getThreadGroup().getName()+Thread.currentThread().getName());
                Thread.sleep(31000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        return "success";
    }


}
