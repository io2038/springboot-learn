package ink.tsg.springexecutor.service;

import java.util.concurrent.CountDownLatch;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExecutorService.java
 * @Description
 * @createTime 2021-04-22
 */
public interface ExecutorService {

    void writeTxt(CountDownLatch countDownLatch,String fileName);

    void testDIYReject(String a) throws InterruptedException;
}
