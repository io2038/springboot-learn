package ink.tsg.springexecutor.service.impl;

import cn.hutool.core.io.FileUtil;
import ink.tsg.springexecutor.service.ExecutorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExecutorServiceImpl.java
 * @Description
 * @createTime 2021-04-22
 */
@Service
@Slf4j
public class ExecutorServiceImpl implements ExecutorService {


    @Override
    @Async("threadPoolTaskExecutor")
    public void testDIYReject(String a) throws InterruptedException {
        Thread.sleep(30000);
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public void writeTxt(CountDownLatch countDownLatch,String fileName){
//        log.info("线程-" + Thread.currentThread().getId() + "在执行写入");
        try {
            // File file = new File(fileName);

            List<String> strings = FileUtil.readLines(fileName, "UTF-8");
//            log.info("文件内容{}",strings);
            System.out.println(strings);
//            File copyFile = new File(fileName + "_copy.txt");
//            lines.stream().forEach(string->{
//                try {
//                    FileUtils.writeStringToFile(copyFile,string,"utf8",true);
//                    FileUtils.writeStringToFile(copyFile,"\r\n","utf8",true);
//                } catch (IOException e) {
//                    log.info(e.getMessage());
//                }
//            });
        }catch (Exception e) {
            log.info(e.getMessage());
        }
        finally {
            countDownLatch.countDown();
        }

    }

}
