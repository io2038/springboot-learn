package ink.tsg.springexecutor.service.impl;

import ink.tsg.springexecutor.service.ExecutorService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;


/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ExecutorServiceImplTest.java
 * @Description
 * @createTime 2021-04-22
 */
@RunWith(value = SpringRunner.class)
@SpringBootTest
class ExecutorServiceImplTest {


    @Autowired
    ExecutorService executorService;


    private CountDownLatch countDownLatch;


    @Test
    public void testWriteCode() {
        try {
            File file = new File("E:\\工作文档\\gps");
            File[] files = file.listFiles();
            //计数器数量就等于文件数量,因为每个文件会开一个线程
            countDownLatch = new CountDownLatch(files.length);
            long start = System.currentTimeMillis();
            Arrays.stream(files).forEach(file1 -> {
                File child = new File(file1.getAbsolutePath());
                String fileName = child.getAbsolutePath();
                executorService.writeTxt(countDownLatch, fileName);
            });
            long end = System.currentTimeMillis();
            System.out.println("历时：" + (end - start));
            countDownLatch.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testReject() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            executorService.testDIYReject("aaaaa");
        }
    }



}