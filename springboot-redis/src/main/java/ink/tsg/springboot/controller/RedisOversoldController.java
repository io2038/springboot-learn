package ink.tsg.springboot.controller;

import ink.tsg.springboot.entities.TbStock;
import ink.tsg.springboot.service.IRedisOversoldService;
import ink.tsg.springboot.service.ITbOrderService;
import ink.tsg.springboot.service.ITbStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName RedisOversoldController.java
 * @Description
 * @createTime 2021-10-21
 */
@RestController
public class RedisOversoldController {


    @Autowired
    private ITbOrderService iTbOrderService;

    @Autowired
    private ITbStockService stockService;

    @Autowired
    private IRedisOversoldService redisOversoldService;

    @GetMapping
    public String get(){
        TbStock byId = stockService.getById(1);
        return byId.toString();
    }

    @GetMapping("{id}")
    public String buy(@PathVariable Integer id){
        return redisOversoldService.buy(id);
    }
}
