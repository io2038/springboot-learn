package ink.tsg.springboot.controller;

import com.taobao.api.ApiException;
import ink.tsg.springboot.entities.UserEntity;
import ink.tsg.springboot.service.impl.DingWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HelloController {


    @Resource
    private RedisTemplate<Object, Object> redisTemplate;


    @Autowired
    private DingWorkService dingWorkService;


    @GetMapping("increasing")
    public String increasing(){
        String key = "redisAtomicLong:getAndIncrement:add";
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, redisTemplate.getConnectionFactory());
        Long increment = entityIdCounter.getAndIncrement();
        System.out.println(increment);
        return null;
    }

    @GetMapping("/hello")
    public String hello(){
        // redisTemplate.delete("strKey");
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setUserName("aaaaa");
        userEntity.setUserSex("m");
        int i = userEntity.hashCode();
        redisTemplate.opsForValue().set(String.valueOf(i),userEntity);
        UserEntity key = (UserEntity) redisTemplate.opsForValue().get("key");
        System.out.println(key);
        return "success";
    }

    @GetMapping("getNo")
    public String getNo(){
        Long aaa = redisTemplate.opsForValue().increment("aaa");
        String format = String.format("%03d", aaa);
        return String.valueOf(format);
    }

    @GetMapping("send/{phone}")
    public String send(@PathVariable("phone")String phone) throws ApiException {
        dingWorkService.sendOA(phone);
        return "success";
    }

    @GetMapping("getCode")
    public void getUserInfo(@RequestParam("code")String code,@RequestParam("state")String state)  {
        System.out.println(code);
    }
}
