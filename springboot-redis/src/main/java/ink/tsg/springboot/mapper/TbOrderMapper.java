package ink.tsg.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.springboot.entities.TbOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TbOrderMapper.java
 * @Description
 * @createTime 2021-10-21
 */
@Mapper
public interface TbOrderMapper extends BaseMapper<TbOrder> {

}
