package ink.tsg.springboot.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TbStock.java
 * @Description
 * @createTime 2021-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tb_order", autoResultMap = true)
public class TbOrder {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer amount;
}
