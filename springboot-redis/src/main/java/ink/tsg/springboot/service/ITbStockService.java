package ink.tsg.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.springboot.entities.TbStock;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ITbOrderService.java
 * @Description
 * @createTime 2021-10-21
 */
public interface ITbStockService extends IService<TbStock> {
}
