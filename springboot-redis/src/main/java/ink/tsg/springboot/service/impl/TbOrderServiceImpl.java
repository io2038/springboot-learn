package ink.tsg.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.springboot.entities.TbOrder;
import ink.tsg.springboot.mapper.TbOrderMapper;
import ink.tsg.springboot.service.ITbOrderService;
import org.springframework.stereotype.Service;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TbOrderServiceImpl.java
 * @Description
 * @createTime 2021-10-21
 */
@Service
public class TbOrderServiceImpl extends ServiceImpl<TbOrderMapper, TbOrder> implements ITbOrderService {
}
