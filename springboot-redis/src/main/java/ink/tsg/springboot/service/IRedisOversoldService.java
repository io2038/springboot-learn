package ink.tsg.springboot.service;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName IRedisOversoldService.java
 * @Description
 * @createTime 2021-10-21
 */
public interface IRedisOversoldService {
    String buy(Integer id);
}
