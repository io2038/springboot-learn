package ink.tsg.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.springboot.entities.TbStock;
import ink.tsg.springboot.mapper.TbStockMapper;
import ink.tsg.springboot.service.ITbStockService;
import org.springframework.stereotype.Service;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName TbOrderServiceImpl.java
 * @Description
 * @createTime 2021-10-21
 */
@Service
public class TbStockServiceImpl extends ServiceImpl<TbStockMapper, TbStock> implements ITbStockService {
}
