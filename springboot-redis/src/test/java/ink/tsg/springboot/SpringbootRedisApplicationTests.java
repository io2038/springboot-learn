package ink.tsg.springboot;

import ink.tsg.springboot.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.Serializable;


@SpringBootTest
class SpringbootRedisApplicationTests {

    @Resource
    private RedisTemplate<Object, Object> redisTemplate;


    @Test
    public void testString() {
        redisTemplate.opsForValue().set("strKey", "zwqh");
        System.out.println(redisTemplate.opsForValue().get("strKey"));
    }



}
