package ink.tsg.utilslearn.springboot;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author geo_tsg
 * @version 1.0.0
 * @ClassName ObjectUtilsLearn.java
 * @Description
 * @createTime 2021-10-15
 */
public class ObjectUtilsLearn {

    public static void main(String[] args) {



        // 数组转集合
//        arrayToList();

        // 检查给定数组是否包含给定元素
//        containsElement();

        // 检查给定的异常（运行时的）是否与throws子句中声明的指定异常类型兼容。
        // compatibleWithThrowsClause();
    }

    private static void arrayToList() {
//        List list = CollectionUtils.arrayToList(new String[]{"aaa", "bbb"});
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        CollectionUtils.mergeArrayIntoCollection(new String[]{"ccc", "ddd"},list);
        System.out.println(list);
    }

    private static void containsElement() {
        System.out.println(ObjectUtils.containsElement(new String[]{"主管井","过渡井","预留井"},"ccc"));

    }

    private static void compatibleWithThrowsClause() {
        try {
            int i = 2/0;
        }catch (Exception e){
            System.out.println(ObjectUtils.isCompatibleWithThrowsClause(e, ArithmeticException.class));
            e.printStackTrace();
        }

    }
}
