# 各个知名框架内的utils学习

## Springboot内的工具类学习

![image-20211015170543629](\img\image-20211015170543629.png)

### ObjectUtils

#### isCompatibleWithThrowsClause

检查给定的异常（运行时的）是否与throws子句中声明的指定异常类型兼容。

```java
/**
	 * Return whether the given throwable is a checked exception:
	 * that is, neither a RuntimeException nor an Error.
	 * @param ex the throwable to check
	 * @return whether the throwable is a checked exception
	 * @see java.lang.Exception
	 * @see java.lang.RuntimeException
	 * @see java.lang.Error
	 */
	public static boolean isCheckedException(Throwable ex) {
		return !(ex instanceof RuntimeException || ex instanceof Error);
	}
/**
 * Check whether the given exception is compatible with the specified
 * exception types, as declared in a throws clause.
 * @param ex the exception to check
 * @param declaredExceptions the exception types declared in the throws clause
 * @return whether the given exception is compatible
 */
public static boolean isCompatibleWithThrowsClause(Throwable ex, @Nullable Class<?>... declaredExceptions) {
   if (!isCheckedException(ex)) {
      return true;
   }
   if (declaredExceptions != null) {
      for (Class<?> declaredException : declaredExceptions) {
         if (declaredException.isInstance(ex)) {
            return true;
         }
      }
   }
   return false;
}
```

#### containsElement

检查给定数组是否包含给定元素

```java
/**
 * Check whether the given array contains the given element.
 * @param array the array to check (may be {@code null},
 * in which case the return value will always be {@code false})
 * @param element the element to check for
 * @return whether the element has been found in the given array
 */
public static boolean containsElement(@Nullable Object[] array, Object element) {
   if (array == null) {
      return false;
   }
   for (Object arrayEle : array) {
      if (nullSafeEquals(arrayEle, element)) {
         return true;
      }
   }
   return false;
}
```

