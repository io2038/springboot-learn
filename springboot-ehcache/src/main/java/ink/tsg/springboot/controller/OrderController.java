package ink.tsg.springboot.controller;

import ink.tsg.springboot.entities.Order;
import ink.tsg.springboot.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("createNormal")
    public String createNormal(@RequestBody Order order) {
        orderService.createNormal(order);
        return "success";
    }

    @PostMapping("createFault")
    public String createFault(Order order) {
        orderService.createFault(order);
        return "success";
    }

    @GetMapping("/getEhcacheById/{id}")
    public String getEhcacheById(@PathVariable String id){
        return orderService.getEhcacheById(id);
    }
}
