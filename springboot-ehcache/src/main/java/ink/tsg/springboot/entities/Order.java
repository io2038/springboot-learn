package ink.tsg.springboot.entities;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_order")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Order {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    // 用户id
    private Long userId;

    // 产品id
    private Long productId;

    // 数量
    private Integer count;

    // 金额
    private BigDecimal money;

    // 订单状态：0：创建中；1：已完结
    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    // 插入与更新都写此字段。若使用FieldFill.UPDATE，则只更新时写此字段。
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
