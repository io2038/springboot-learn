package ink.tsg.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ink.tsg.springboot.entities.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends BaseMapper<Order> {
    @Update("UPDATE `t_order` SET status = #{status} WHERE id = #{id}")
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);

    @Select("select * from t_order where id = #{id}")
    Order getEhcacheById(String id);
}
