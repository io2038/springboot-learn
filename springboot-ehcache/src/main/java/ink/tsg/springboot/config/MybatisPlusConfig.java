package ink.tsg.springboot.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class MybatisPlusConfig { //自动填充插件
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new MybatisPlusAutoFillConfig();
    }

    public class MybatisPlusAutoFillConfig implements MetaObjectHandler {
        // 新增时填充
        @Override
        public void insertFill(MetaObject metaObject) {
            setFieldValByName("createTime", LocalDateTime.now(), metaObject);
            setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        }

        // 修改时填充
        @Override
        public void updateFill(MetaObject metaObject) {
            setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        }
    }
}
