package ink.tsg.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ink.tsg.springboot.entities.Order;
import ink.tsg.springboot.mapper.OrderMapper;
import ink.tsg.springboot.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@CacheConfig(cacheNames = {"orderCache"})
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {



    @Autowired
    OrderMapper orderMapper;


    @Override
    public void createNormal(Order order) {
        log.info("创建订单总业务：开始");

        save(order);

        //orderMapper.updateStatus(order.getId(), 1);

        //orderTccAction.tryCreateOrderNormal(order.getId(), order.getProductId(), order.getCount(), order.getUserId(), order.getMoney());

        // ResultHolder.setResult(OrderTccAction.class, UUID.randomUUID().toString(), "p");

        log.info("创建订单总业务：结束");
    }

    @Override
    public void createFault(Order order) {
        log.info("创建订单总业务：开始");
        save(order);

        orderMapper.updateStatus(order.getId(), 1);

        // orderTccAction.tryCreateOrderFault(order.getId(), order.getProductId(), order.getCount(), order.getUserId(), order.getMoney());

        log.info("创建订单总业务：结束");
    }

    @Override
    @Cacheable(key = "#p0")
    public String getEhcacheById(String id) {
        Order order = orderMapper.getEhcacheById(id);
        return order.toString();
    }

}
