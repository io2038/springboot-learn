package ink.tsg.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ink.tsg.springboot.entities.Order;

public interface OrderService extends IService<Order> {

    void createNormal(Order order);

    void createFault(Order order);

    String getEhcacheById(String id);
}
